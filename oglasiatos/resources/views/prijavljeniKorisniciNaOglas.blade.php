<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
       <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>StudOglas | Prijavljeni korisnici </title>
        <meta name="description" content="company is a real-estate template">
        <meta name="author" content="Kimarotec">
        <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/fontello.css">
        <link href="assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="assets/css/bootstrap-select.min.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/icheck.min_all.css">
        <link rel="stylesheet" href="assets/css/price-range.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">  
        <link rel="stylesheet" href="assets/css/owl.theme.css">
        <link rel="stylesheet" href="assets/css/owl.transitions.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
    </head>
    <body>
        <input class='idprijavljenih' type='hidden' value='{{$id_oglasa}}'>
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Body content -->

        <div class="header-connect">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-8  col-xs-12">
                        <div class="header-half header-call">
                            <p>
                                <span><i class="pe-7s-call"></i> +1 234 567 7890</span>
                                <span><i class="pe-7s-mail"></i> oglasi@atos.com</span>
                            </p>
                        </div>
                    </div>
                       <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12">
                        <div class="header-half header-social">
                            <ul class="list-inline">
                                <li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://vine.co"><i class="fa fa-vine"></i></a></li>
                                <li><a href="https://rs.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="https://dribbble.com"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>             
        <!--End top header -->

        <nav class="navbar navbar-default ">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index"><img src="assets/img/logo.png" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse yamm" id="navigation">
                    <div class="button navbar-right prijavaRegistracija">
                        <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('prijavljivanje.html')" data-wow-delay="0.4s">Prijavljivanje</button>
                        <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.open('registrovanje.html')" data-wow-delay="0.5s">Registrovanje</button>
                    </div>
                    <ul class="main-nav nav navbar-nav navbar-right">
                         <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="index">
                            Početna stranica
                        </a></li>

                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="stanovanje.html">
                            Stanovanje
                        </a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="posao.html">Posao</a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="literatura.html">Literatura</a></li>
                        <li class="dropdown ymm-sw " data-wow-delay="0.1s">
                            <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Profil <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                <li>
                                    <a href="poruke.html">Poruke</a>
                                </li>
                                <li>
                                    <a href="profil.html">Pregled profila</a>
                                </li>
                                
                                <li>
                                    <a href="brisanjeProfila.html">Brisanje profila</a>
                                </li>
                                
                               <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle active" data-toggle="dropdown" >Moji oglasi <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="mojiOglasiStan.html">Stan</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiLiteratura.html">Literatura</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiPosao.html">Posao</a>
                                </li>
                                

                            </ul>
                        </li>
    
                            </ul>
                        </li>
                       

                        
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"> Korisnici prijavljeni na Vaš oglas </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area recent-property padding-top-40" style="background-color: #FFF;">
            <div class="container">  
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 "> 
                        <div id="contact1">                        
                            
                            <!-- /.row -->
                         
                            
                           
                            <h2>Prijavljeni korisnici</h2>
                            <hr>
                            <br>
                            <form>
                                <div class="row " style="overflow-y:scroll;height: 200px; border-right: solid lightgray 1px;" >
                                    
                                    <div class="col-sm-12 prijavljeni">
                                        <div class="form-group">
                                            <label for="firstname"><strong>Korisnicko ime</strong></label>
                                            <div class="col-sm-3 text-center pull-right">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i>  Slanje </button>
                                    </div>
                                            <hr>
                                        </div>

                                    </div>
                                  
                                </div>

                                <!-- /.row -->
                            </form>
                           
                        </div>
                    </div> 

                  
                </div>

               
               
            </div>
        </div>
   <!--     <div id="map" style="height: 400px;"></div> -->
  <!-- Footer area-->
     <div class="footer-area">

            <div class=" footer">
                <div class="container">
                    <div class="row">

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>O nama </h4>
                                <div class="footer-title-line"></div>

                                <img src="assets/img/logo.png" alt="" class="wow pulse" data-wow-delay="1s">
                                <p>Dobrodošli na StudOglas! Ovde možete naći oglase za stan,literutu, posao ili praksu.</p>
                                <ul class="footer-adress">
                                    <li><i class="pe-7s-map-marker strong"> </i> Niš </li>
                                    <li><i class="pe-7s-mail strong"> </i> oglasi@atos.com</li>
                                    <li><i class="pe-7s-call strong"> </i> +1 234 567 7890</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Linkovi </h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-menu">
                                    <li><a href="index">Početna stranica</a>  </li> 
                                    <li><a href="stanovanje.html">Stanovanje</a>  </li> 
                                    <li><a href="literatura.html">Literatura </a></li> 
                                    <li><a href="posao.html">Posao </a></li> 
                                    <li><a href="profil.html">Profil</a>  </li> 
                                    <li><a href="registrovanje.html">Registracija</a>  </li> 
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Predloženi oglasi</h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-blog predlozi">
                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 

                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 
                                     

                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 


                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer news-letter">
                                <h4>Ostanite povezani</h4>
                                <div class="footer-title-line"></div>
                              

                                <div class="social "> 
                                    <ul>
                                        <li><a class="wow fadeInUp animated" href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://www.facebook.com" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://plus.google.com" data-wow-delay="0.3s"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://instagram.com" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                                        
                                    </ul> 
                                </div>
                            </div>
                        </div> 


                    </div>
                </div>
            </div>

            <div class="footer-copy text-center">
                <div class="container">
                        <div class="row">
                        <div class="pull-left">
                            <span> &#169 ATOSGroup, All rights reserved 2019  </span> 
                        </div> 
                        <div class="bottom-menu pull-right"> 
                              <ul> 
                                <li><a class="wow fadeInUp animated" href="index" data-wow-delay="0.2s">Početna stranica</a></li>
                                <li><a class="wow fadeInUp animated" href="stanovanje.html" data-wow-delay="0.3s">Stanovanje</a></li>
                                <li><a class="wow fadeInUp animated" href="literatura.html" data-wow-delay="0.4s">Literatura</a></li>
                                <li><a class="wow fadeInUp animated" href="posao.html" data-wow-delay="0.6s">Posao</a></li>
                            </ul> 
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script src="assets/js/modernizr-2.6.2.min.js"></script>

        <script src="assets/js/jquery-1.10.2.min.js"></script> 
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-select.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.js"></script>

        <script src="assets/js/easypiechart.min.js"></script>
        <script src="assets/js/jquery.easypiechart.min.js"></script>

        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/wow.js"></script>

        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/price-range.js"></script>
        
        <!--script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
        <script src="assets/js/gmaps.js"></script>        
        <script src="assets/js/gmaps.init.js"></script-->


        <script src="assets/js/main.js"></script>
         <script type="text/javascript">


          var Settings = {
            base_url: "{{ url('/citanjePrijava') }}",
            predloziOglasa_url: "{{ url('/predloziOglasa')}}" ,
            predlozi_url: "{{ url('/oglasStan') }}",
            poruka_url:"{{ url('/novaPoruka') }}",
            proveriPrijavu_url: "{{ url('/proveriPrijavu') }}",
            pocetnaStranica_url: "{{ url('/index') }}",
            odjaviSe_url: "{{ url('session/remove') }}"
          }

      
        </script>
        <script src="assets/js/predloziOglasaZaStan.js"></script>
        <script src="assets/js/prijavljeniNaOglas.js"></script>
        <script src="assets/js/promenaPrijaveUOdjavu.js"></script>


    </body>
</html>