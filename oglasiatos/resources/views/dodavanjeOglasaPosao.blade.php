<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>StudOglas | Dodavanje oglasa za posao</title>
        <meta name="description" content="GARO is a real-estate template">
        <meta name="author" content="Kimarotec">
        <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/fontello.css">
        <link href="assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="assets/css/bootstrap-select.min.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/icheck.min_all.css">
        <link rel="stylesheet" href="assets/css/price-range.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">  
        <link rel="stylesheet" href="assets/css/owl.theme.css">
        <link rel="stylesheet" href="assets/css/owl.transitions.css"> 
        <link rel="stylesheet" href="assets/css/wizard.css"> 
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
        <link rel="stylesheet" href="assets/css/dropzone.css">
    </head>
    <body>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Body content -->

        <div class="header-connect">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-8  col-xs-12">
                        <div class="header-half header-call">
                            <p>
                                <span><i class="pe-7s-call"></i> +1 234 567 7890</span>
                                <span><i class="pe-7s-mail"></i> oglasi@atosi.com</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12">
                        <div class="header-half header-social">
                            <ul class="list-inline">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-vine"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>              
        <!--End top header -->

      

                <!-- Collect the nav links, forms, and other content for toggling -->
                <nav class="navbar navbar-default ">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index"><img src="assets/img/logo.png" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse yamm" id="navigation">
                    <div class="button navbar-right prijavaRegistracija">
                        <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('prijavljivanje.html')" data-wow-delay="0.4s">Prijavljivanje</button>
                        <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.open('registrovanje.html')" data-wow-delay="0.5s">Registrovanje</button>
                    </div>
                    <ul class="main-nav nav navbar-nav navbar-right">
                         <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="index">
                            Početna stranica
                        </a></li>

                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="active" href="stanovanje.html">
                            Stanovanje
                        </a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="posao.html">Posao</a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="literatura.html">Literatura</a></li>
                        <li class="dropdown ymm-sw " data-wow-delay="0.1s">
                            <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Profil <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                <li>
                                    <a href="poruke.html">Poruke</a>
                                </li>
                                <li>
                                    <a href="profil.html">Pregled profila</a>
                                </li>
                                
                                <li>
                                    <a href="brisanjeProfila.html">Brisanje profila</a>
                                </li>
                                
                               <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle active" data-toggle="dropdown" >Moji oglasi <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="mojiOglasiStan.html">Stan</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiLiteratura.html">Literatura</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiPosao.html">Posao</a>
                                </li>
                                

                            </ul>
                        </li>
    
                            </ul>
                        </li>
                       

                        
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title">Dodavanje novog oglasa za posao</h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area submit-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">
                <div class="clearfix" > 
                    <div class="wizard-container"> 

                        <div class="wizard-card ct-wizard-orange" id="wizardProperty">
                            <form>                        
                                <div class="wizard-header">
                                    <h3>
                                        <b>Dodajte </b> Vaš oglas za posao<br>
                                        <small>Ovde možete dodati svoj oglas za posao.</small>
                                    </h3>
                                </div>

                                <ul>
                                    <li><a href="#step1" data-toggle="tab">Korak 1 </a></li>
                                    <li><a href="#step2" data-toggle="tab">Korak 2 </a></li>
                                    <li><a href="#step3" data-toggle="tab">Korak 3 </a></li>
                                    <li><a href="#step4" data-toggle="tab">Završni deo </a></li>
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane" id="step1">
                                        <div class="row p-b-15  ">
                                            <h4 class="info-text"> Počnimo od osnovih informacija <small>(sa validacijom podataka)</small></h4>
                                        <div class="col-sm-2 col-sm-offset-1">
                                            
                                            </div> 
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Naslov oglasa: </label>
                                                    <input name="naslovOglasa" type="text" class="form-control kontrole">
                                                </div>

                                                <div class="form-group">
                                                    <label>Plata: </label>
                                                    <input name="cena" type="text" class="form-control kontrole">
                                                </div> 
                                         
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <!--  End step 1 -->

                                    <div class="tab-pane" id="step2">
                                        <h4 class="info-text"> Dodavanje dodatnih karakteristika oglasa </h4>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="col-sm-12"> 
                                                    <div class="form-group">
                                                        <label>Opis posla:</label>
                                                        <textarea name="opis" class="form-control kontrole" ></textarea>
                                                    </div> 
                                                </div> 
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                    <label>Naziv posla:</label>
                                                    <input name="naziv" type="text" class="form-control kontrole">
                                                </div> 
                                                </div>
                                                 <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Grad:</label>
                                                        <select id="lunchBegins" name = "grad" class="selectpicker kontrole odabirGrada" data-live-search="true" data-live-search-style="begins" title="Odaberite grad">
                                                            <option>Nis</option>
                                                            <option>Novi Sad</option>
                                                            <option>Beograd</option>
                                                            <option>Kragujevac</option>
                                                            <option>Kosovska Mitrovica</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Tip posla:</label>
                                                        <select id="lunchBegins" name="tipPosla" class="selectpicker kontrole" data-live-search="true" data-live-search-style="begins" title="Odaberite tip posla">
                                                            <option>Privremeni</option>
                                                            <option>Sezonski</option>
                                                            <option>Stalni</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                    <label>Lokacija radnog mesta: </label>
                                                    <input name="lokacija" type="text" class="form-control kontrole">
                                                </div> 
                                                </div>
                                               
                                            </div>
                                            <div class="col-sm-12 padding-top-15">   
                                                
                                                
                                                   
                                              
                                                <div class="col-sm-3 col-md-offset-3">

                                                    <div class="form-group">
                                                    <label>Radno vreme: </label>
                                                    <input name="radnoVreme" type="text" class="form-control kontrole">
                                                </div> 
                                                </div>
                                               <div class="col-sm-3 ">

                                                <label><h5>Potrebno radno iskustvo:</h5></label><br>
                                                
                                                <input type="radio" name="radno-iskustvo" value="da" class="form-control kontrole"> Da <br>
                                                <input type="radio" name="radno-iskustvo" value="ne" class="form-control kontrole"> Ne
                                            </div>
                                            </div>
                                            <br>
                                        
                                            

                                        </div>
                                    </div>
                                    <!-- End step 2 -->

                                    <div class="tab-pane" id="step3">                                        
                                        <h4 class="info-text">Dodajte slike radnog mesta</h4>
                                        <div class="row">  
                                            <div class="col-sm-6 col-md-offset-3">
                                                <div class="form-group">
                                                    <label for="property-images">Odaberite slike:</label>
                                                   <div class="clsbox-1" runat="server"  >
                                                     <div class="dropzone clsbox" id="mydropzone">

                                                     </div>
                                                  </div>
                                                    <p class="help-block">Odaberite više slika za Vaš oglas.</p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!--  End step 3 -->


                                    <div class="tab-pane" id="step4">                                        
                                        <h4 class="info-text"> Dodavanje </h4>
                                          
                                            <div class="col-sm-6 col-md-offset-4">
                                                
                                                  
                                                        <label><h5><strong>Da li ste sigurni da želite da dodate ovaj oglas? </strong></h5></label>
                                                        

                                                
                                            </div>
                                        
                                    </div>
                                    <!--  End step 4 -->

                                </div>

                                <div class="wizard-footer">
                                    <input type="hiden" class="btn-next">
                                    <div class="pull-right">
                                        <input type='button' class='btn btn-primary' id="sledeci" name='next' value='Sledeći korak' />
                                        <input type='button' class='btn btn-finish btn-primary ' id="dodaj" name='finish' value='Dodaj' />
                                    </div>

                                    <div class="pull-left">
                                        <input type='button' class='btn btn-previous btn-default' id="prethodni" name='previous' value='Prethodni korak' />
                                    </div>
                                    <div class="clearfix"></div>                                            
                                </div>	
                            </form>
                        </div>
                        <!-- End submit form -->
                    </div> 
                </div>
            </div>
        </div>

          <!-- Footer area-->
      <div class="footer-area">

            <div class=" footer">
                <div class="container">
                    <div class="row">

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>O nama </h4>
                                <div class="footer-title-line"></div>

                                <img src="assets/img/logo.png" alt="" class="wow pulse" data-wow-delay="1s">
                                <p>Dobrodošli na StudOglas! Ovde možete naći oglase za stan,literutu, posao ili praksu.</p>
                                <ul class="footer-adress">
                                    <li><i class="pe-7s-map-marker strong"> </i> Niš </li>
                                    <li><i class="pe-7s-mail strong"> </i> oglasi@atos.com</li>
                                    <li><i class="pe-7s-call strong"> </i> +1 234 567 7890</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Linkovi </h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-menu">
                                    <li><a href="index">Početna stranica</a>  </li> 
                                    <li><a href="stanovanje.html">Stanovanje</a>  </li> 
                                    <li><a href="literatura.html">Literatura </a></li> 
                                    <li><a href="posao.html">Posao </a></li> 
                                    <li><a href="profil.html">Profil</a>  </li> 
                                    <li><a href="registrovanje.html">Registracija</a>  </li> 
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Predloženi oglasi</h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-blog predloziPosao">
                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 

                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 
                                     

                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 


                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer news-letter">
                                <h4>Ostanite povezani</h4>
                                <div class="footer-title-line"></div>
                              

                                <div class="social "> 
                                    <ul>
                                        <li><a class="wow fadeInUp animated" href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://www.facebook.com" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://plus.google.com" data-wow-delay="0.3s"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://instagram.com" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                                        
                                    </ul> 
                                </div>
                            </div>
                        </div> 


                    </div>
                </div>
            </div>

            <div class="footer-copy text-center">
                <div class="container">
                        <div class="row">
                        <div class="pull-left">
                            <span> &#169 ATOSGroup, All rights reserved 2019  </span> 
                        </div> 
                        <div class="bottom-menu pull-right"> 
                              <ul> 
                                <li><a class="wow fadeInUp animated" href="index" data-wow-delay="0.2s">Početna stranica</a></li>
                                <li><a class="wow fadeInUp animated" href="stanovanje.html" data-wow-delay="0.3s">Stanovanje</a></li>
                                <li><a class="wow fadeInUp animated" href="literatura.html" data-wow-delay="0.4s">Literatura</a></li>
                                <li><a class="wow fadeInUp animated" href="posao.html" data-wow-delay="0.6s">Posao</a></li>
                            </ul> 
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script src="assets/js/modernizr-2.6.2.min.js"></script>
        <script src="assets/js//jquery-1.10.2.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-select.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.js"></script>
        <script src="assets/js/easypiechart.min.js"></script>
        <script src="assets/js/jquery.easypiechart.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/wow.js"></script>
        <script src="assets/js/icheck.min.js"></script>

        <script src="assets/js/price-range.js"></script> 
        <script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
        <script src="assets/js/jquery.validate.min.js"></script>
        <script src="assets/js/wizard.js"></script>

        <script src="assets/js/main.js"></script>
        <script src="assets/js/dropzone.min.js"></script>
        <script type="text/javascript">
                
            var i=0;

            Dropzone.autoDiscover = false;

            Dropzone.options.myAwesomeDropzone = { 
                addRemoveLinks: true
            }
            $(document).ready(function(){

                var baseUrl = "{{ url('/slike') }}";
                var token = "{{ Session::token() }}";

                $("div#mydropzone").dropzone({
                    paramName: 'file',
                    name:""+i,
                    url: baseUrl,
                    params: {
                        _token: token
                    },
                    dictDefaultMessage: "Drop or click to upload images",
                    clickable: true,
                    maxFilesize: 2,
                    addRemoveLinks: false,
                    renameFilename: function (filename) {
                        name  = "{{ Session::get('oglasiatos_korisnik')}}"+i+"."+filename.split('.').pop();;
                        return name;
                    },
                    queuecomplete: function(d) {
                        //console.log("ucitan");
                        i=i+1;
                        // @TODO : Ajax call to load your uploaded files right away if required
                    }
                });
            });

        </script>


        <script type="text/javascript">
          var Settings = {
            base_url: "{{ url('/dodajOglasPosao') }}",
            mojoglas_url:"{{ url('/mojiOglasiPosao.html')}}",
            prijava_url:"{{ url('/prijavljivanje.html')}}",
            selectZaGrad_url: "{{ url('/selectZaGrad') }}",
            predloziOglasaPosao_url: "{{ url('/predloziOglasaPosao')}}" ,
            predlozi_url: "{{ url('/oglasPosao') }}",
            proveriPrijavu_url: "{{ url('/proveriPrijavu') }}",
            pocetnaStranica_url: "{{ url('/index') }}",
            odjaviSe_url: "{{ url('session/remove') }}"
          }
      
        </script>
        <script src="assets/js/predloziOglasaZaPosao.js"></script>
        <script src="assets/js/dodajOglasPosao.js"></script>
        <script src="assets/js/promenaPrijaveUOdjavu.js"></script>
    </body>
</html>