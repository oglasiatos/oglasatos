<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>StudOglas | Stanovanje </title>
        <meta name="description" content="GARO is a real-estate template">
        <meta name="author" content="Kimarotec">
        <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/fontello.css">
        <link href="assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="assets/css/bootstrap-select.min.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/icheck.min_all.css">
        <link rel="stylesheet" href="assets/css/price-range.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">  
        <link rel="stylesheet" href="assets/css/owl.theme.css">
        <link rel="stylesheet" href="assets/css/owl.transitions.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
    </head>
    <body>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Body content -->
    
       
        <div class="header-connect">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-8  col-xs-12">
                        <div class="header-half header-call">
                            <p>
                                <span><i class="pe-7s-call"></i> +1 234 567 7890</span>
                                <span><i class="pe-7s-mail"></i> oglasi@atos.com</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12">
                        <div class="header-half header-social">
                            <ul class="list-inline">
                                <li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://vine.co"><i class="fa fa-vine"></i></a></li>
                                <li><a href="https://rs.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="https://dribbble.com"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
        <!--End top header -->

        <nav class="navbar navbar-default ">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index"><img src="assets/img/logo.png" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse yamm" id="navigation">
                    <div class="button navbar-right prijavaRegistracija">
                        <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('prijavljivanje.html')" data-wow-delay="0.4s">Prijavljivanje</button>
                        <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.open('registrovanje.html')" data-wow-delay="0.5s">Registrovanje</button>
                    </div>
                    <ul class="main-nav nav navbar-nav navbar-right">
                         <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="index">
                            Početna stranica
                        </a></li>

                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="active" href="stanovanje.html">
                            Stanovanje
                        </a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="posao.html">Posao</a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="literatura.html">Literatura</a></li>
                        <li class="dropdown ymm-sw " data-wow-delay="0.1s">
                            <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Profil <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                <li>
                                    <a href="poruke.html">Poruke</a>
                                </li>
                                <li>
                                    <a href="profil.html">Pregled profila</a>
                                </li>
                               
                                <li>
                                    <a href="brisanjeProfila.html">Brisanje profila</a>
                                </li>
                                
                               <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle active" data-toggle="dropdown" >Moji oglasi <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="mojiOglasiStan.html">Stan</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiLiteratura.html">Literatura</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiPosao.html">Posao</a>
                                </li>
                                

                            </ul>
                        </li>
    
                            </ul>
                        </li>
                       

                        
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title">Pronađite svoj topli dom!</h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="properties-area recent-property" style="background-color: #FFF;">
            <div class="container">  
                <div class="row">
                     
                <div class="col-md-3 p0 padding-top-40">
                    <div class="blog-asside-right pr0">
                        <div class="panel panel-default sidebar-menu wow fadeInRight animated" >
                            <div class="panel-heading">
                                <h3 class="panel-title">Pretraga</h3>
                            </div>
                            <div class="panel-body search-widget">
                                <form action="" class=" form-inline"> 

                                    <fieldset>
                                        <div class="row">
                                            <div class="col-xs-6">

                                                <select id="lunchBegins" name = "grad" class="selectpicker pretraga odabirGrada" data-live-search="true" data-live-search-style="begins" title="Grad">

                                                    <option>Nis</option>
                                                    <option>Pirot</option>
                                                    <option>Beograd</option>
                                                    <option>Novi Sad</option>
                                                    <option>Kragujevac</option>
                                                    
                                                </select>
                                            </div>
                                            <div class="col-xs-6">

                                                <select id="basic" name = "grejanje" class="selectpicker show-tick form-control pretraga" title="Tip grejanja">
                                                    <option> Centralno grejanje </option>
                                                    <option> TA </option>
                                                    <option> Ostalo </option>  

                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset class="padding-5">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="price-range"><h5>Cena:</h5></label>
                                        
                                                <input type="text" class="span2 sliderCena slider1" value="" data-slider-min="0" 
                                                       data-slider-max="100000" data-slider-step="1" 
                                                       data-slider-value="[0,450]" data-slider-selection="after" id="price-range" ><br />
                                                <b class="pull-left color">0€</b> 
                                                <b class="pull-right color">100000€</b>                                                
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="property-geo"><h5>Površina:</h5></label>
                                                <input type="text" class="span2 sliderPovrsina slider2" value="" data-slider-min="30" 
                                                       data-slider-max="12000" data-slider-step="1" 
                                                       data-slider-value="[50,450]" data-slider-selection="after" id="property-geo" ><br />
                                                <b class="pull-left color">30m2</b> 
                                                <b class="pull-right color">12000m2</b>                                                
                                            </div>                                            
                                        </div>
                                    </fieldset> 
                                    <fieldset>
                                         <div class="row">
                                            <div class="col-xs-6">
                                                <label for="price-range">Tip stana:</label><br>
                                                <input type="radio" name="kucaStan" value="Kuca" class="form-control pretraga" checked> Kuća
                                            </div>
                                            <div class="col-xs-6">
                                                <br>
                                                <input type="radio" name="kucaStan" value="Stan" class="form-control pretraga"> Stan
                                            </div>
                                        </div>

                                    </fieldset>                               

                                    <fieldset class="padding-5">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="price-range"><h5>Sprat:</h5></label>
                                                <input type="text" class="span2 sliderSprat slider3" value="" data-slider-min="1" 
                                                       data-slider-max="20" data-slider-step="1" 
                                                       data-slider-value="[6,13]" data-slider-selection="after" id="min-baths" ><br />
                                                <b class="pull-left color">1</b> 
                                                <b class="pull-right color">20</b>                                                
                                            </div>

                                            <div class="col-xs-6">
                                                <label for="property-geo"><h5>Broj soba:</h5></label>
                                                <input type="text" class="span2 sliderBrojSoba slider4" value="" data-slider-min="1" 
                                                       data-slider-max="10" data-slider-step="1" 
                                                       data-slider-value="[2,4]" data-slider-selection="after" id="min-bed" ><br />
                                                <b class="pull-left color">1</b> 
                                                <b class="pull-right color">10</b>

                                            </div>
                                        </div>
                                    </fieldset>
                                               <fieldset>
                                         <div class="row">
                                            <div class="col-xs-6">
                                                <label for="price-range"><h5>Tip stanara:</h5></label><br>
                                                <input type="radio" name="cimer_stanar" value="Stanar" class="form-control pretraga" checked> Stanar
                                            </div>
                                            <div class="col-xs-6">
                                                <br>
                                                <input type="radio" name="cimer_stanar" value="Cimer" class="form-control pretraga"> Cimer
                                            </div>
                                        </div>

                                    </fieldset>
                                                
                                    <fieldset class="padding-5">
                                         <label for="price-range"><h5>Dodatne karakteristike:</h5></label><br>
                                        <div class="row">
                                            <div class="col-xs-6">

                                                <div class="checkbox">
                                                    <label> <input name="terasa" class = "pretraga" type="checkbox" checked> Terasa </label>
                                                </div> 
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="checkbox">
                                                    <label> <input name= "lift" class = "pretraga" type="checkbox"> Lift </label>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </fieldset>

                                    <fieldset class="padding-5">
                                        <div class="row">
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label> <input name="parking" class = "pretraga" type="checkbox" checked> Parking </label>
                                                </div>
                                            </div>  
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label> <input name="garaza" class = "pretraga" type="checkbox" checked> Garaža </label>
                                                </div>
                                            </div>  
                                        </div>
                                    </fieldset>

                                    <fieldset class="padding-5">
                                        <div class="row">
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label><input name="podrum" class = "pretraga" type="checkbox"> Podrum </label>
                                                </div>
                                            </div>  
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label> <input name = "telefon" class = "pretraga" type="checkbox"> Telefon </label>
                                                </div>
                                            </div>  
                                        </div>
                                    </fieldset>

                                    <fieldset class="padding-5">
                                        <div class="row">
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label>  <input name = "internet" class = "pretraga" type="checkbox" checked> Internet </label>
                                                </div>
                                            </div>  
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label>  <input name = "kablovska" class = "pretraga" type="checkbox"> Kablovska </label>
                                                </div>
                                            </div>  
                                        </div>
                                    </fieldset>

                                    <fieldset class="padding-5">
                                        <div class="row">
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label>  <input name="novogradnja" class = "pretraga" type="checkbox"> Novogradnja </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6"> 
                                                <div class="checkbox">
                                                    <label>  <input name = "opremljenost" class = "pretraga" type="checkbox"> Opremljenost </label>
                                                </div>
                                            </div> 
                                        </div>
                                    </fieldset>

                                    <fieldset >
                                        <div class="row">
                                            <div class="col-xs-12">  
                                                <input class="button btn largesearch-btn pretrazi_oglase" value="Pretraži" type="button">
                                            </div>  
                                        </div>
                                    </fieldset>                                     
                                </form>
                            </div>
                        </div>

                       
                    </div>
                </div>

                <div class="col-md-9  pr0 padding-top-40 properties-page">
                    <div class="col-md-12 clear"> 
                        <div class="col-xs-10 page-subheader sorting pl0">
                        
                        </div>

                        <div class="col-xs-2 layout-switcher">
                            <a class="layout-list" href="javascript:void(0);"> <i class="fa fa-th-list"></i>  </a>
                            <a class="layout-grid active" href="javascript:void(0);"> <i class="fa fa-th"></i> </a>                          
                        </div><!--/ .layout-switcher-->
                    </div>

                    <div class="col-md-12 clear"> 
                        <div id="list-type" class="proerty-th oglasi">
                            <div class="col-sm-6 col-md-4 p0">
                                    <div class="box-two proerty-item">
                                        <div class="item-thumb">
                                            <a href="property-1.html" ><img src="assets/img/demo/property-3.jpg"></a>
                                        </div>

                                        <div class="item-entry overflow">
                                            <h5><a href="property-1.html">Naziv oglasa</a></h5>
                                            <div class="dot-hr"></div>
                                            <span class="pull-left"><b>Površina:</b> 120m </span>
                                            <span class="proerty-price pull-right"> $ 300,000</span>
                                            <p style="display: none;">Suspendisse ultricies Suspendisse ultricies Nulla quis dapibus nisl. Suspendisse ultricies commodo arcu nec pretium ...</p>
                                            <div class="property-icon">
                                                <img src="assets/img/icon/bed.png">(5)|
                                                <img src="assets/img/icon/shawer.png">(2)|
                                                <img src="assets/img/icon/cars.png">(1)  
                                            </div>
                                        </div>


                                    </div>
                                </div> 

                        </div>
                    </div>
                    
                    <div class="col-md-12"> 
                        <div class="pull-right">
                            <div class="pagination">
                                <ul  class="stranice">
                                    <li><a href="#">Prev</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>                
                    </div>
                </div>  
                </div>              
            </div>
        </div>

          <!-- Footer area-->
               <div class="footer-area">

            <div class=" footer">
                <div class="container">
                    <div class="row">

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>O nama </h4>
                                <div class="footer-title-line"></div>

                                <img src="assets/img/logo.png" alt="" class="wow pulse" data-wow-delay="1s">
                                <p>Dobrodošli na StudOglas! Ovde možete naći oglase za stan,literutu, posao ili praksu.</p>
                                <ul class="footer-adress">
                                    <li><i class="pe-7s-map-marker strong"> </i> Niš </li>
                                    <li><i class="pe-7s-mail strong"> </i> oglasi@atos.com</li>
                                    <li><i class="pe-7s-call strong"> </i> +1 234 567 7890</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Linkovi </h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-menu">
                                    <li><a href="index">Početna stranica</a>  </li> 
                                    <li><a href="stanovanje.html">Stanovanje</a>  </li> 
                                    <li><a href="literatura.html">Literatura </a></li> 
                                    <li><a href="posao.html">Posao </a></li> 
                                    <li><a href="profil.html">Profil</a>  </li> 
                                    <li><a href="registrovanje.html">Registracija</a>  </li> 
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Predloženi oglasi</h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-blog predlozi">
                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 


                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer news-letter">
                                <h4>Ostanite povezani</h4>
                                <div class="footer-title-line"></div>
                              

                                <div class="social "> 
                                    <ul>
                                        <li><a class="wow fadeInUp animated" href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://www.facebook.com" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://plus.google.com" data-wow-delay="0.3s"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://instagram.com" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                                        
                                    </ul> 
                                </div>
                            </div>
                        </div> 


                    </div>
                </div>
            </div>

            <div class="footer-copy text-center">
                <div class="container">
                        <div class="row">
                        <div class="pull-left">
                            <span> &#169 ATOSGroup, All rights reserved 2019  </span> 
                        </div> 
                        <div class="bottom-menu pull-right"> 
                              <ul> 
                                <li><a class="wow fadeInUp animated" href="index" data-wow-delay="0.2s">Početna stranica</a></li>
                                <li><a class="wow fadeInUp animated" href="stanovanje.html" data-wow-delay="0.3s">Stanovanje</a></li>
                                <li><a class="wow fadeInUp animated" href="literatura.html" data-wow-delay="0.4s">Literatura</a></li>
                                <li><a class="wow fadeInUp animated" href="posao.html" data-wow-delay="0.6s">Posao</a></li>
                            </ul> 
                        </div>
                    </div>
                </div>
            </div>

        </div>

      <script src="assets/js/modernizr-2.6.2.min.js"></script>
        <script src="assets/js/jquery-1.10.2.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-select.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.js"></script>
        <script src="assets/js/easypiechart.min.js"></script>
        <script src="assets/js/jquery.easypiechart.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/wow.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/price-range.js"></script>
        <script src="assets/js/main.js"></script>

         <script type="text/javascript">
          var Settings = {
            base_url: "{{ url('/listaOglasaZaStan') }}",
            //predlozi_url : " {{ url('/predloziOglasaZaStan') }}",
            filtriranjeOglasa_url: "{{ url ('/filtriranjeOglasaZaStan') }}",
            selectZaGrad_url: "{{ url('/selectZaGrad') }}",
            predloziOglasa_url: "{{ url('/predloziOglasa')}}" ,
            predlozi_url: "{{ url('/oglasStan') }}",
            prikazVelikogOglasa_url: "{{ url('/oglasStan') }}",
            proveriPrijavu_url: "{{ url('/proveriPrijavu') }}",
            pocetnaStranica_url: "{{ url('/index') }}",
            odjaviSe_url: "{{ url('session/remove') }}"
          }
      
        </script>
        <script src="assets/js/predloziOglasaZaStan.js"></script>
        <script src="assets/js/stanovanje.js"></script>
        <script src="assets/js/promenaPrijaveUOdjavu.js"></script>
    </body>
</html>