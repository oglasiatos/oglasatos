<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>StudOglas | Stan</title>
        <meta name="description" content="GARO is a real-estate template">
        <meta name="author" content="Kimarotec">
        <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/fontello.css">
        <link href="assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="assets/css/bootstrap-select.min.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/icheck.min_all.css">
        <link rel="stylesheet" href="assets/css/price-range.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">  
        <link rel="stylesheet" href="assets/css/owl.theme.css">
        <link rel="stylesheet" href="assets/css/owl.transitions.css">
        <link rel="stylesheet" href="assets/css/lightslider.min.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
    </head>
    <body>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Body content -->

        <div class="header-connect">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-8  col-xs-12">
                        <div class="header-half header-call">
                            <p>
                                <span><i class="pe-7s-call"></i> +1 234 567 7890</span>
                                <span><i class="pe-7s-mail"></i> oglasi@atos.com</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-5  col-sm-3 col-sm-offset-1  col-xs-12">
                        <div class="header-half header-social">
                            <ul class="list-inline">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-vine"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <!--End top header -->

        <nav class="navbar navbar-default ">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     <a class="navbar-brand" href="index"><img src="assets/img/logo.png" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse yamm" id="navigation">
                    <div class="button navbar-right prijavaRegistracija">
                        <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('prijavljivanje.html')" data-wow-delay="0.45s">Prijavljivanje</button>
                        <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.open('registrovanje.html')" data-wow-delay="0.48s">Registracija</button>
                    </div>
                    <ul class="main-nav nav navbar-nav navbar-right">
                         <li class="wow fadeInDown" data-wow-delay="0.1s"><a class="" href="index">
                            Početna stranica
                        </a></li>
                   

                        <li class="wow fadeInDown" data-wow-delay="0.2s"><a class="active" href="stanovanje.html">Stanovanje</a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.2s"><a class="" href="posao.html">
                        Posao
                        </a></li>
                        <li class="wow fadeInDown" data-wow-delay="0.2s"><a class="" href="literatura.html">
                        Literatura
                        </a></li>
                        <li class="dropdown ymm-sw sakri" data-wow-delay="0.1s">
                            <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Profil <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                <li>
                                    <a href="poruke.html">Poruke</a>
                                </li>
                                <li>
                                    <a href="profil.html">Pregled profila</a>
                                </li>
                                
                                <li>
                                    <a href="brisanjeProfila.html">Brisanje profila</a>
                                </li>
                                
                               <li class="dropdown-submenu">
                            <a href="" class="dropdown-toggle active" data-toggle="dropdown" >Moji oglasi <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="mojiOglasiStan.html">Stan</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiLiteratura.html">Literatura</a>
                                </li>
                                <li>
                                    <a href="mojiOglasiPosao.html">Posao</a>
                                </li>
                                

                            </ul>
                        </li>
    
                            </ul>
                        </li>
                        
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- End of nav bar -->

        <div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title">{{$naslov_oglasa}} </h1>               
                    </div>
                </div>
            </div>
        </div>
        <!-- End page header -->

        <!-- property area -->
        <div class="content-area single-property" style="background-color: #FCFCFC;">&nbsp;
            <div class="container">   

                <div class="clearfix padding-top-40" >

                    <div class="col-md-8 single-property-content prp-style-2">
                        <div class="">
                            <div class="row">
                                <div class="light-slide-item">            
                                    <div class="clearfix">
                                        <div class="favorite-and-print">
                                            <a class="add-to-fav" href="#login-modal" data-toggle="modal">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                            <a class="printer-icon " href="javascript:window.print()">
                                                <i class="fa fa-print"></i> 
                                            </a>
                                        </div> 

                                       <ul id="image-gallery" class="gallery list-unstyled cS-hidden" name="{{count($slike)}}">
                                            @if(count($slike) >= 1)
                                                @foreach ($slike as $slika)
                                                    <li data-thumb="{{$slika}}"> 
                                                        <img src="{{$slika}}" />
                                                    </li>
                                                @endforeach
                                            @else
                                                <li data-thumb="assets/img/property-1/property1.jpg"> 
                                                    <img src="assets/img/property-1/property1.jpg" />
                                                </li>
                                                <li data-thumb="assets/img/property-1/property2.jpg"> 
                                                    <img src="assets/img/property-1/property3.jpg" />
                                                </li>
                                                <li data-thumb="assets/img/property-1/property3.jpg"> 
                                                    <img src="assets/img/property-1/property3.jpg" />
                                                </li>
                                                <li data-thumb="assets/img/property-1/property4.jpg"> 
                                                    <img src="assets/img/property-1/property4.jpg" />
                                                </li>      
                                            @endif                                 
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-property-wrapper">

                                <div class="section">
                                    <h4 class="s-property-title">Opis:</h4>
                                    <div class="s-property-content">
                                        <p>{{$opis_oglasa}}</p>
                                    </div>
                                </div>
                                <!-- End description area  -->

                                <div class="section additional-details">

                                    <h4 class="s-property-title">Osnovne karakteristike:</h4>

                                    <ul class="additional-details-list clearfix">
                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Tip stana</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$kuca_stan}}</label>
                                        </li> 
                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Sprat</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$sprat}}</label>
                                        </li> 
                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Površina</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$povrsina}} <span>m</span><sup>2</sup></label>
                                        </li>

                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Cena</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">
                                                {{$cena}}<span>€</span>
                                            </label>
                                        </li>
                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">
                                                Broj soba
                                            </span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$broj_soba}} </label>
                                        </li>

                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Tip grejanja</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$tip_grejanja}}</label>
                                        </li>

                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Tip stanara</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$cimer_stanar}}</label>
                                        </li>
                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Grad</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$grad}}</label>
                                        </li>
                                        <li>
                                            <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Adresa</span>
                                            <label class="col-xs-6 col-sm-8 col-md-8 add-d-entry">{{$adresa_stana}}</label>
                                        </li>

                                    </ul>
                                </div>  
                                <!-- End additional-details area  -->

                                <div class="section property-features">      

                                    <h4 class="s-property-title">Dodatne karakteristike:</h4>                            
                                    <ul>
                                        @if($internet == true)         
                                            <li><label>internet</label></li>
                                        @endif
                                                
                                        @if($kablovska == true)         
                                            <li><label>kablovska</label></li>
                                        @endif

                                        @if($telefon == true)         
                                            <li><label>telefon</label></li>
                                        @endif

                                        @if($lift == true)         
                                            <li><label>lift</label></li>
                                        @endif
                                        
                                        @if($terasa == true)         
                                            <li><label>terasa</label></li>
                                        @endif

                                        @if($parking == true)         
                                            <li><label>parking</label></li>
                                        @endif

                                        @if($garaza == true)         
                                            <li><label>garaza</label></li>
                                        @endif

                                         @if($podrum == true)         
                                            <li><label>podrum</label></li>
                                        @endif

                                        @if($novogradnja == true)         
                                            <li><label>novogradnja</label></li>
                                        @endif

                                    </ul>

                                </div>

                            </div>
                        </div>

                  
                    </div>

                    <div class="col-md-4 p0">
                        <aside class="sidebar sidebar-property blog-asside-right property-style2">
                            <div class="dealer-widget">
                                <div class="dealer-content">
                                    <div class="inner-wrapper">
                                        <div class="single-property-header">                                          
                                            <h1 class="property-title">{{$naslov_oglasa}}</h1>
                                            <label class="property-price"><span>€</span>{{$cena}}</label>
                                        </div>

                                        <div class="property-meta entry-meta clearfix ">   

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-tag">                                                                                      
                                                    <img src="assets/img/icon/rent-orange.png">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Status</span>
                                                    <span class="property-info-value">Izdavanje</span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info icon-area">
                                                    <img src="assets/img/icon/room-orange.png">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Površina</span>
                                                    <span class="property-info-value">{{$povrsina}}<b class="property-info-unit">m <sup>2</sup></b></span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-bed">
                                                    <img src="assets/img/icon/bed-orange.png">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Broj soba</span>
                                                    <span class="property-info-value">{{$broj_soba}}</span>
                                                </span>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">
                                                <span class="property-info-icon icon-bath">
                                                    <img src="assets/img/icon/cars-orange.png">
                                                </span>
                                                <span class="property-info-entry">
                                                    <span class="property-info-label">Posedovanje garaže</span>
                                                    <span class="property-info-value">@if($garaza == true)         
                                                                                                  DA         
                                                                                            @else
                                                                                                  NE        
                                                                                            @endif
                                                    </span>
                                                </span>
                                            </div>
                                             <div class="col-xs-4 col-sm-4 col-md-4 p-b-15"></div> 
                                            <div class="col-xs-4 col-sm-4 col-md-4 p-b-15">

                                            <div class="button navbar-right">
                                                @if($tip_korisnika_koji_gleda_oglas != "Korisnik")
                                                @elseif($ulogovan != "Nije ulogovan!!!")
                                                    <button class="navbar-btn nav-button wow bounceInRight prijavinaoglas" id="{{$id_oglasa}}"  type="button">Prijavljivanje</button>
                                                @endif
                        </div></div>

                                        </div>
                                        <div class="dealer-section-space">
                                            <span>Informacije o izdavaču</span>
                                        </div>
                                        <div class="clear">
                                            <div class="col-xs-4 col-sm-4 dealer-face">
                                                <a href="">
                                                    <img src="{{$profilna}}" class="img-circle">
                                                </a>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 ">
                                                <h3 class="dealer-name">
                                                    <a>{{$korisnicko_ime_izdavaca_oglasa}}</a>
                                                    <label>{{$ime}} {{$prezime}}</label>        
                                                </h3>
                                                <div class="dealer-social-media">
                                                    <a class="twitter" target="_blank" href="">
                                                        <i class="fa fa-twitter"></i>
                                                    </a>
                                                    <a class="facebook" target="_blank" href="">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                    <a class="gplus" target="_blank" href="">
                                                        <i class="fa fa-google-plus"></i>
                                                    </a>
                                                    <a class="linkedin" target="_blank" href="">
                                                        <i class="fa fa-linkedin"></i>
                                                    </a> 
                                                    <a class="instagram" target="_blank" href="">
                                                        <i class="fa fa-instagram"></i>
                                                    </a>       
                                                </div>

                                            </div>
                                            
                                        </div>
                                        <div class="container">
                                         <div class="row">
                                            <div class="col-lg-12">
                                              <div class="star-rating">
                                                <span class="fa fa-star-o" data-rating="1"></span>
                                                <span class="fa fa-star-o" data-rating="2"></span>
                                                <span class="fa fa-star-o" data-rating="3"></span>
                                                <span class="fa fa-star-o" data-rating="4"></span>
                                                <span class="fa fa-star-o" data-rating="5"></span>
                                                <input type="hidden" name="whatever1" class="rating-value" value="{{$ocena}}">
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                        <ul>
                                                <li><i class="pe-7s-map-marker strong"> </i>&nbsp; {{$kgrad}} </li>
                                                <li><i class="pe-7s-mail strong"></i>&nbsp; {{$email_adresa}}</li>
                                                <li><i class="pe-7s-call strong"> </i>&nbsp; +{{$kontakt_telefon}}</li>
                                            </ul>

                                        </div>

                                    </div>

                                </div>
                                <div class="komentari">
                                    <div>
                                        <h4 class="s-property-title">Komentari</h4>
                                    <div class="background-color listaKomentara">
                                        <!--label>Korisničko ime</label><br>
                                        <div class="clear form-group" style="display: inline-flex;" >
                                            <!--p style="padding: 10px;"> Komentar <span ><i class="pull-right"> x </i></span></p-->
                                            
                                          <label for="firstname">Korisnicko ime</label>
                                            <p class="background-color" style="padding: 10px;">Komentar <i class="pull-right"> x </i></p>
                                        

                                        <!--/div-->
                                    </div>
                                </div>
                                    <br>
                                    @if($ulogovan != "Nije ulogovan!!!")
                                      
                                        <form>  
                                    @else
                                        <form hidden>
                                    @endif
                                    <label>Korisničko ime</label>
                                    
                                        <div class="input-group">   
                                        <input class="form-control komentarunos"   type="text" placeholder="Komentar ... ">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary subscribe komentarisi"
                                             type="button" id="{{$id_oglasa}}"><i class="pe-7s-comment pe-2x"></i></button>
                                        </span>
                                    </div>
                                    <!-- /input-group -->
                                </form> 
                                        </div>
                            </div>
                        </aside>
                    </div>

                </div>

            </div>
        </div>

        <!-- Footer area-->
 <div class="footer-area">

            <div class=" footer">
                <div class="container">
                    <div class="row">

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>O nama </h4>
                                <div class="footer-title-line"></div>

                                <img src="assets/img/logo.png" alt="" class="wow pulse" data-wow-delay="1s">
                                <p>Dobrodošli na StudOglas! Ovde možete naći oglase za stan,literutu, posao ili praksu.</p>
                                <ul class="footer-adress">
                                    <li><i class="pe-7s-map-marker strong"> </i> Niš </li>
                                    <li><i class="pe-7s-mail strong"> </i> oglasi@atos.com</li>
                                    <li><i class="pe-7s-call strong"> </i> +1 234 567 7890</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Linkovi </h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-menu">
                                    <li><a href="index">Početna stranica</a>  </li> 
                                    <li><a href="stanovanje.html">Stanovanje</a>  </li> 
                                    <li><a href="literatura.html">Literatura </a></li> 
                                    <li><a href="posao.html">Posao </a></li> 
                                    <li><a href="profil.html">Profil</a>  </li> 
                                    <li><a href="registrovanje.html">Registracija</a>  </li> 
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>Predloženi oglasi</h4>
                                <div class="footer-title-line"></div>
                                <ul class="footer-blog predlozi">
                                    <li>
                                        <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                            <a href="single.html">
                                                <img src="assets/img/demo/small-proerty-2.jpg">
                                            </a>
                                            <span class="blg-date">12-12-2016</span>

                                        </div>
                                        <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                            <h6> <a href="single.html">Add news functions </a></h6> 
                                            <p style="line-height: 17px; padding: 8px 2px;">Lorem ipsum dolor sit amet, nulla ...</p>
                                        </div>
                                    </li> 

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer news-letter">
                                <h4>Ostanite povezani</h4>
                                <div class="footer-title-line"></div>
                              

                                <div class="social "> 
                                    <ul>
                                        <li><a class="wow fadeInUp animated" href="https://twitter.com"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://www.facebook.com" data-wow-delay="0.2s"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://plus.google.com" data-wow-delay="0.3s"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="wow fadeInUp animated" href="https://instagram.com" data-wow-delay="0.4s"><i class="fa fa-instagram"></i></a></li>
                                        
                                    </ul> 
                                </div>
                            </div>
                        </div> 


                    </div>
                </div>
            </div>

            <div class="footer-copy text-center">
                <div class="container">
                        <div class="row">
                        <div class="pull-left">
                            <span> &#169 ATOSGroup, All rights reserved 2019  </span> 
                        </div> 
                        <div class="bottom-menu pull-right"> 
                              <ul> 
                                <li><a class="wow fadeInUp animated" href="index" data-wow-delay="0.2s">Početna stranica</a></li>
                                <li><a class="wow fadeInUp animated" href="stanovanje.html" data-wow-delay="0.3s">Stanovanje</a></li>
                                <li><a class="wow fadeInUp animated" href="literatura.html" data-wow-delay="0.4s">Literatura</a></li>
                                <li><a class="wow fadeInUp animated" href="posao.html" data-wow-delay="0.6s">Posao</a></li>
                            </ul> 
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <script src="assets/js/modernizr-2.6.2.min.js"></script>
        <script src="assets/js/jquery-1.10.2.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-select.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.js"></script>
        <script src="assets/js/easypiechart.min.js"></script>
        <script src="assets/js/jquery.easypiechart.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/wow.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/price-range.js"></script>
        <script src="assets/js/rating.js"></script>

        <script type="text/javascript" src="assets/js/lightslider.min.js"></script>
        <script src="assets/js/main.js"></script>

        <script>
                            $(document).ready(function () {

                                $('#image-gallery').lightSlider({
                                    gallery: true,
                                    item: 1,
                                    thumbItem: 9,
                                    slideMargin: 0,
                                    speed: 500,
                                    auto: true,
                                    loop: true,
                                    onSliderLoad: function () {
                                        $('#image-gallery').removeClass('cS-hidden');
                                    }
                                });
                            });
        </script>


        <script type="text/javascript">
          var Settings = {
            base_url: "{{ url('/profilizmeni') }}",
            profil_url:"{{ url('/profilizmeni')}}",
            slanjeprijave_url:"{{url('/slanjeprijave')}}",
            komentarisi_url:"{{url('/komentarisi')}}",
            komentari_url:"{{url('/komentari')}}",
            obrisikom_url:"{{url('/obrisikom')}}",
            ocenjivanje_url:"{{url('/ocenjivanje')}}",
            prikazVelikogOglasa_url:"{{ url('/oglasStan') }}",
            predloziOglasa_url: "{{ url('/predloziOglasa')}}" ,
            predlozi_url: "{{ url('/oglasStan') }}",
            proveriPrijavu_url: "{{ url('/proveriPrijavu') }}",
            pocetnaStranica_url: "{{ url('/index') }}",
            odjaviSe_url: "{{ url('session/remove') }}"
            
          }
      
        </script>
        <script type="text/javascript" src="assets/js/prijavaikomentar.js"></script>
        <script src="assets/js/oglasiStan.js"></script>
        <script src="assets/js/predloziOglasaZaStan.js"></script>
        <script src="assets/js/promenaPrijaveUOdjavu.js"></script>


    </body>
</html>