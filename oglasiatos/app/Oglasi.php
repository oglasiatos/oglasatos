<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Oglasi extends Model
{
    public $id_oglasa;
    public $korisnicko_ime_izdavaca_oglasa;
    public $naslov_oglasa;
    public $grad;
    public $opis_oglasa;
    public $vrsta_oglasa;
    public $datum_postavljanja_oglasa;
    public $cena;

    public function __construct($korisnicko_ime_izdavaca_oglasa, $naslov_oglasa, $grad, $opis_oglasa, $vrsta_oglasa, $cena)
    {
    	//$this->id_oglasa=$this->kreirajNoviIdOglasa();
    	$this->korisnicko_ime_izdavaca_oglasa = $korisnicko_ime_izdavaca_oglasa;
    	$this->naslov_oglasa = $naslov_oglasa;
    	$this->grad = $grad;
    	$this->opis_oglasa = $opis_oglasa;
    	$this->vrsta_oglasa=$vrsta_oglasa;
    	//$this->datum_postavljanja_oglasa=(new \DateTime())->format('Y-m-d');
    	$this->cena=$cena;
    }

    public function vratiDatumPostavljanjaOglasa($idOglasa)
    {
        $datum = DB::table('dodatni_oglasi')->where('id_oglasa', $idOglasa)->get();

        return $datum[0]['datum_postavljanja_oglasa']->toDateTime()->format('Y-m-d');
    }

    public function vratiSveOglase()
    {
    	return DB::table('dodatni_oglasi')->get();
    }

    public function vratiOglasNaOsnovuIda($id)
    {
    	$oglas = DB::table('dodatni_oglasi')->where('id_oglasa', $id)->get();

    	if(count ($oglas) < 1)
    	{
    		return "Ne postoji oglas.";
    	}

    	return $oglas;
    }

    public function vratiBrojOglasa()
    {
    	return DB::table('oglasi')->count();
    }

    public function vratiSveOglaseJednogKorisnika($korisnickoIme)
    {
        $string = "SELECT id_oglasa FROM oglasi WHERE vrsta_oglasa IN ('Oglas Za Stan', 'Oglas Za Literaturu', 'Oglas Za Posao') AND korisnicko_ime_izdavaca_oglasa = '".$korisnickoIme."'";
        return DB::raw($string);

    }

    public function vratiSveOglaseZaStanJednogKorisnika($korisnickoIme)
    {
        return DB::table('oglasi')->where('vrsta_oglasa', "Oglas Za Stan")->where('korisnicko_ime_izdavaca_oglasa', $korisnickoIme)->get();
    }

    public function vratiSveOglaseZaLiteraturuJednogKorisnika($korisnickoIme)
    {
        return DB::table('oglasi')->where('vrsta_oglasa', "Oglas Za Literaturu")->where('korisnicko_ime_izdavaca_oglasa', $korisnickoIme)->get();
    }

    public function vratiSveOglaseZaPosaoJednogKorisnika($korisnickoIme)
    {
        return  DB::table('oglasi')->where('vrsta_oglasa', "Oglas Za Posao")->where('korisnicko_ime_izdavaca_oglasa', $korisnickoIme)->get();
    }

    public function vratiSveMaleOglaseZaStan()
    {
        return DB::table('oglasi')->where('vrsta_oglasa', 'Oglas Za Stan')->get();
    }

    public function vratiSveMaleOglaseZaLiteraturu()
    {
    	return DB::table('oglasi')->where('vrsta_oglasa', 'Oglas Za Literaturu')->get();
    }

    public function vratiSveMaleOglaseZaPosao()
    {
        return  DB::table('oglasi')->where('vrsta_oglasa', "Oglas Za Posao")->get();
    }

    public function dodajOglas()
    {
        $this->id_oglasa = $this->kreirajNoviIdOglasa();

        $string = "INSERT INTO oglasi(id_oglasa, korisnicko_ime_izdavaca_oglasa, naslov_oglasa, grad, opis_oglasa, vrsta_oglasa, cena) VALUES (".$this->id_oglasa.", '".$this->korisnicko_ime_izdavaca_oglasa."', '".$this->naslov_oglasa."', '".$this->grad."', '".$this->opis_oglasa."', '".$this->vrsta_oglasa."', ".$this->cena.")";

        DB::raw($string);

        $string = "INSERT INTO dodatni_oglasi(id_oglasa, korisnicko_ime_izdavaca_oglasa, naslov_oglasa, grad, opis_oglasa, vrsta_oglasa, cena) VALUES (".$this->id_oglasa.", '".$this->korisnicko_ime_izdavaca_oglasa."', '".$this->naslov_oglasa."', '".$this->grad."', '".$this->opis_oglasa."', '".$this->vrsta_oglasa."', ".$this->cena.")";

        DB::raw($string);

    }

    public function kreirajNoviIdOglasa()
    {
    	$broj = DB::table('indeksi')->where('ime_tabele', 'oglasi')->select('indeks')->get();
        DB::table('indeksi')->where('ime_tabele', 'oglasi')->update(['indeks' => ($broj[0]['indeks']+1)]);
        return $broj[0]['indeks'];
    }

    public function obrisiOglas($id)
    {
        $oglas = DB::table('dodatni_oglasi')->where('id_oglasa', $id)->get();
    	DB::table('dodatni_oglasi')->where('id_oglasa', $id)->deleteRow();
        DB::table('oglasi')->where('vrsta_oglasa', $oglas[0]['vrsta_oglasa'])
                            ->where('korisnicko_ime_izdavaca_oglasa', $oglas[0]['korisnicko_ime_izdavaca_oglasa'])
                            ->where('grad', $oglas[0]['grad'])
                            ->where('cena', $oglas[0]['cena'])
                            ->where('id_oglasa', intval($oglas[0]['id_oglasa']))
                            ->deleteRow();

        $this->brisanjeOglasaIzTabeleKomentari($id);
        $this->brisanjeOglasaIzTabelePrijaveNaOglase($id);
    }

    public function vratiTriOglasaJedneVrste()
    {
        $string = "SELECT * FROM oglasi WHERE vrsta_oglasa = '".$this->vrsta_oglasa."' LIMIT 3";

        $oglasi = DB::raw($string);

        return $oglasi;
    }

    public function brisanjeOglasaIzTabelePrijaveNaOglase($idOglasa)
    {
        $prijava = DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->get();
        DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->deleteRow();
        DB::table('dodatne_prijave_na_oglase')->where('id_prijave_na_oglas', intval($prijava[0]['id_prijave_na_oglas']))->deleteRow();
    }

   public function brisanjeOglasaIzTabeleKomentari($idOglasa)
   {
        $komentar = DB::table('komentari')->where('id_oglasa', $idOglasa)->get();
        DB::table('komentari')->where('id_oglasa', $idOglasa)->deleteRow();
        DB::table('dodatni_komentari')->where('id_komentara', intval($komentar[0]['id_komentara']))->deleteRow();
   }


   public function kreirajNizIzdavaca()
   {
        $korisnik = DB::table('korisnik')->select('korisnicko_ime')->get();

        $i = 0;
        $granica = DB::table('korisnik')->count();
        $niz = "";

        foreach($korisnik as $value)
        {
            $niz .= "'".$value['korisnicko_ime']."'";
            if($i < ($granica-1))
            {
                $niz .= ", ";
            }
            $i = $i + 1;
        }

        return $niz;
   }

   public function kreirajNizGradova()
       {
            $gradovi = DB::table('fakultet')->select('grad')->get();
            $granica = DB::table('fakultet')->count();
            $i = 0;
            $niz = "";

            foreach($gradovi as $value)
            {
                $niz .= "'".$value['grad']."'";
                if($i < ($granica-1))
                {
                    $niz .= ", ";
                }
                $i = $i + 1;
            }
            return $niz;
       }

   public function formirajPresek($niz1, $niz2)
    {
            $rezultat = null;
            $i = 0;
            if(is_null($niz1))
                return null;
            if(is_null($niz2))
                return null;

            foreach($niz1 as $value)
            {
                foreach($niz2 as $val)
                {
                    if($value == $val)
                    {
                        $rezultat[$i] = $value;
                        $i = $i + 1;
                    }
                }
            }
            if($i == 0)
            {
                return null;
            }

            $string = "";
            $j = 0;

            foreach($rezultat as $value)
            {
                $string .= $value;
                if($j < ($i - 1))
                {
                    $string .= ", ";
                }
                $j = $j + 1;
            }
            return $string;
    }

   

   public function filtrirajOglase($vrsta, $grad, $minimalnaCena, $maksimalnaCena, $stariNiz)
   {

            $query = "SELECT id_oglasa FROM oglasi WHERE vrsta_oglasa = '".$vrsta."' ";
            $query .= " AND korisnicko_ime_izdavaca_oglasa in (".$this->kreirajNizIzdavaca().") ";

            if($grad === "")
            {
                $query .= " AND grad in (".$this->kreirajNizGradova().") ";
            }
            else
            {
                $query .= " AND grad = '".$grad."' ";
            }

            if($minimalnaCena === 0 && $maksimalnaCena === 450)
            {
            }
            else
            {
                $query .= " AND cena < ".$maksimalnaCena." AND cena > ".$minimalnaCena;
            }
            //return $query;


            $idjevi = DB::raw($query);
            $niz = null;
            $i = 0;


            if(!is_null($idjevi[0]))
            {
                foreach($idjevi as $value)
                {
                    $niz[$i] = $value['id_oglasa'];
                    $i = $i + 1;
                }
                //return $stariNiz;
                //return $niz;

                $kompletanNiz = $this->formirajPresek($stariNiz, $niz);
                //return $kompletanNiz;
                if(!is_null($kompletanNiz))
                {
                    return DB::raw("SELECT id_oglasa FROM dodatni_oglasi WHERE id_oglasa in (".$kompletanNiz.")");
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

   }
}
