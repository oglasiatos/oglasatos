<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Komentari extends Model
{
    public $id_komentara;
    public $korisnicko_ime_korisnika_koji_komentarise;
    public $korisnicko_ime_izdavaca_oglasa;
    public $id_oglasa;
    public $sadrzaj_komentara;
    //public $datum_i_vreme_komentarisanja;

    public function vratiBrojKomentara()
    {
    	return DB::table('komentari')->count();
    }

    public function kreirajNoviId()
    {
        $broj = DB::table('indeksi')->where('ime_tabele', 'komentari')->select('indeks')->get();
        DB::table('indeksi')->where('ime_tabele', 'komentari')->update(['indeks' => ($broj[0]['indeks']+1)]);
        return $broj[0]['indeks'];
    }

    public function vratiDatumIVremeKomentarisanja($idKomentara)
    {
        $datumIVreme = DB::table('komentari')->where('id_komentara', $idKomentara)->get();

        return $datumIVreme[0]['datum_i_vreme_komentarisanja']->toDateTime()->format('Y-m-d H-i-s');
    }
    
    public function __construct( $korisnicko_ime_korisnika_koji_komentarise, $id_oglasa, $sadrzaj_komentara)
    {
    	//$this->id_komentara=$this->kreirajNoviId();
    	//$this->korisnicko_ime_izdavaca_oglasa = $korisnicko_ime_izdavaca_oglasa;
    	$this->korisnicko_ime_korisnika_koji_komentarise = $korisnicko_ime_korisnika_koji_komentarise;
    	$this->id_oglasa = $id_oglasa;
    	$this->sadrzaj_komentara=$sadrzaj_komentara;
    	//$this->datum_i_vreme_komentarisanja = (new \DateTime())->format('Y-m-d H-i-s');
    }

    public function dodajKomentar($komentar)
    {
        $this->id_komentara = $this->kreirajNoviId();

    	$string = "INSERT INTO komentari (id_komentara, korisnicko_ime_korisnika_koji_komentarise, id_oglasa, sadrzaj_komentara)
        values (".$this->id_komentara.", '".$this->korisnicko_ime_korisnika_koji_komentarise."', ".$this->id_oglasa.", '".$this->sadrzaj_komentara."')";
        $emp=DB::raw($string);

        $string = "INSERT INTO dodatni_komentari (id_komentara, korisnicko_ime_korisnika_koji_komentarise, id_oglasa, sadrzaj_komentara)
        values (".$this->id_komentara.", '".$this->korisnicko_ime_korisnika_koji_komentarise."', ".$this->id_oglasa.", '".$this->sadrzaj_komentara."')";
        $emp=DB::raw($string);
    }

    public function obrisiKomentar()
    {
        $komentar = DB::table('dodatni_komentari')->where('id_komentara', $this->id_komentara)->get();
    	DB::table('dodatni_komentari')->where('id_komentara', $this->id_komentara)->deleteRow();

        DB::table('komentari')->where('id_oglasa', intval($komentar[0]['id_oglasa']))
                              ->where('korisnicko_ime_korisnika_koji_komentarise', $komentar[0]['korisnicko_ime_korisnika_koji_komentarise'])
                              ->where('id_komentara', $this->id_komentara)
                              ->deleteRow();

    }

    public function vratiSveKomentareJednogOglasa()
    {
        return DB::table('komentari')->where('id_oglasa', $this->id_oglasa)->select('id_komentara,sadrzaj_komentara,korisnicko_ime_korisnika_koji_komentarise')->get();
    }

    public function vratiKomentarPremaIdu()
    {
        return DB::table('dodatni_komentari')->where('id_komentara', $this->id_komentara)->get();
    }
}
