<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Oglasi;

class OglasiZaPosao extends Oglasi
{
    public $naziv_posla;
    public $potrebno_radno_iskustvo;
    public $lokacija_radnog_mesta;
    public $radno_vreme;
    public $tip_posla;

    public function __construct($korisnicko_ime_izdavaca_oglasa, $naslov_oglasa, $grad, $opis_oglasa, $vrsta_oglasa, $cena, $naziv_posla, $potrebno_radno_iskustvo, $lokacija_radnog_mesta, $radno_vreme, $tip_posla)
    {
    	parent::__construct($korisnicko_ime_izdavaca_oglasa, $naslov_oglasa, $grad, $opis_oglasa, $vrsta_oglasa, $cena);

    	$this->naziv_posla = $naziv_posla;
    	$this->potrebno_radno_iskustvo=$potrebno_radno_iskustvo;
    	$this->lokacija_radnog_mesta = $lokacija_radnog_mesta;
    	$this->radno_vreme=$radno_vreme;
    	$this->tip_posla=$tip_posla;
    }

    public function vratiSveOglaseZaPosao()
    {
    	return DB::table('oglasi_za_posao')->get();
    }

    public function vratiOglasZaPosaoPremaIdu($id)
    {
    	return DB::table('dodatni_oglasi_za_posao')->where('id_oglasa_za_posao', $id)->get();
    }

    public function dodajOglasZaPosao()
    {
    	Oglasi::dodajOglas();
        if($this->potrebno_radno_iskustvo === "da")
        {
            $p="true";
        }
        else
        {
            $p="false";
        }
        $string="INSERT INTO oglasi_za_posao(id_oglasa_za_posao, naziv_posla, potrebno_radno_iskustvo, lokacija_radnog_mesta, radno_vreme, tip_posla) values (".$this->id_oglasa.", '".$this->naziv_posla."', ".$p.", '".$this->lokacija_radnog_mesta."', '".$this->radno_vreme."', '".$this->tip_posla."')";
    	DB::raw($string);

        $string="INSERT INTO dodatni_oglasi_za_posao(id_oglasa_za_posao, naziv_posla, potrebno_radno_iskustvo, lokacija_radnog_mesta, radno_vreme, tip_posla) values (".$this->id_oglasa.", '".$this->naziv_posla."', ".$p.", '".$this->lokacija_radnog_mesta."', '".$this->radno_vreme."', '".$this->tip_posla."')";
        DB::raw($string);
    }

    public function obrisiOglasZaPosao()
    {
    	Oglasi::obrisiOglas($this->id_oglasa);
        $oglas = DB::table('dodatni_oglasi_za_posao')->where('id_oglasa_za_posao', $this->id_oglasa)->get();
    	DB::table('dodatni_oglasi_za_posao')->where('id_oglasa_za_posao', $this->id_oglasa)->deleteRow();

        if($oglas[0]['potrebno_radno_iskustvo'])
                {
                  $p = "true";
                }
                else
                {
                  $p = "false";
                }
                $brisanje = "DELETE FROM oglasi_za_posao WHERE naziv_posla = '".$oglas[0]['naziv_posla']."' AND tip_posla = '".$oglas[0]['tip_posla']."' AND potrebno_radno_iskustvo = ".$p." AND id_oglasa_za_posao = ".intval($oglas[0]['id_oglasa_za_posao']);

                DB::raw($brisanje);
    }

    public function filtriranjeOglasaZaPosao($tipPosla, $potrebnoRadnoIskustvo, $nazivPosla, $grad, $minimalnaCena, $maksimalnaCena)
    {
        $query = "SELECT id_oglasa_za_posao FROM oglasi_za_posao WHERE ";

        if($tipPosla === "")
        {
            $query .= " tip_posla in ('Privremeni', 'Sezonski', 'Stalni', 'Praksa') ";
        }
        else
        {
            $query .= " tip_posla = '".$tipPosla."' ";
        }

        if($potrebnoRadnoIskustvo === "da")
        {
            $query .= " AND potrebno_radno_iskustvo = true ";
        }
        else
        {
            $query .= " AND potrebno_radno_iskustvo = false ";
        }

        $idjevi = DB::raw($query);
        $niz = null;
        $i = 0;

        if(!is_null($idjevi[0]))
        {
            if($nazivPosla === "")
            {
                foreach($idjevi as $value)
                {
                    $niz[$i] = $value['id_oglasa_za_posao'];
                    $i = $i + 1;
                }
            }
            else
            {
                foreach($idjevi as $value)
                {
                    $query = "SELECT id_oglasa_za_posao FROM dodatni_oglasi_za_posao WHERE id_oglasa_za_posao = ".$value['id_oglasa_za_posao']." AND naziv_posla LIKE '%".$nazivPosla."%'";
                    if(!is_null(DB::raw($query)[0]))
                    {
                        $niz[$i] = DB::raw($query)[0]['id_oglasa_za_posao'];
                        $i = $i + 1;
                    }
                }
            }
            return Oglasi::filtrirajOglase("Oglas Za Posao", $grad, $minimalnaCena, $maksimalnaCena, $niz);
        }
        else
        {
            return null;
        }
    }
}
