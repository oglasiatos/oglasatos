<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Korisnik extends Model
{
    public $ime;
    public $prezime;
    public $datum_rodjenja;
    public $adresa;
    public $grad;
    public $email_adresa;
    public $korisnicko_ime;
    public $lozinka;
    public $datum_registrovanja;
    public $kontakt_telefon;
    public $tip_korisnika;

    

    public function __construct($ime, $prezime, $datum_rodjenja, $adresa, $grad, $email_adresa, $korisnicko_ime, $lozinka, $datum_registrovanja, $kontakt_telefon, $tip_korisnika)
    {
    	$this->ime=$ime;
    	$this->prezime=$prezime;
    	$this->datum_rodjenja=$datum_rodjenja;
    	$this->adresa=$adresa;
    	$this->grad=$grad;
    	$this->email_adresa=$email_adresa;
    	$this->korisnicko_ime=$korisnicko_ime;
    	$this->lozinka=$lozinka;
    	$this->datum_registrovanja=$datum_registrovanja;
    	$this->kontakt_telefon=$kontakt_telefon;
    	$this->tip_korisnika=$tip_korisnika;
    }

    public function proveriDaLiJeKorisnikBlokiran($email)
    {
        $broj= DB::table('blokirani_korisnici')->where('email_adresa', $email)
        ->count();

        if($broj>0)
        {
            return true;
        }

        return false;

    }

    public function proveriDaLiPostojiKorisnikSaKorisnickimImenom()
    {
        $broj = DB::table('korisnik')->where('korisnicko_ime', $this->korisnicko_ime)->count();
        if($broj>0)
        {
            return true; 
        }
        return false;
    }

    public function proveriDaLiPostojiKorisnikSaEmailom()
    {
        $broj = DB::table('korisnik')->where('korisnicko_ime', $this->korisnicko_ime)
                                     ->where('email_adresa', $this->email_adresa)
                                     ->count();

        if($broj>0)
        {
            return true;
        }
        return false;
    }

    public function registracijaKorisnika()
    {
        
        if($this->proveriDaLiJeKorisnikBlokiran($this->email_adresa))
        {
            return "Korisnik je trajno blokiran!";
        }

        if((DB::table('korisnik')->count())>0)
        {
            if($this->proveriDaLiPostojiKorisnikSaKorisnickimImenom())
            {
                return "Nevalidno korisnicko ime!";
            }

            if($this->proveriDaLiPostojiKorisnikSaEmailom())
            {
                return "Nevalidna email adresa";
            }
        }

        DB::table('korisnik')->insert([
            'ime' => $this->ime,
            'prezime' => $this->prezime,
            'datum_rodjenja' => $this->datum_rodjenja,
            'adresa' => $this->adresa,
            'grad' => $this->grad,
            'email_adresa' => $this->email_adresa,
            'korisnicko_ime' => $this->korisnicko_ime,
            'lozinka' => $this->lozinka,
            'datum_registrovanja' => (new \DateTime())->format('Y-m-d'),
            'kontakt_telefon' => $this->kontakt_telefon,
            'tip_korisnika' => $this->tip_korisnika
        ]);

        return "Uspesna registracija!";
    }

    public function proveriDaLiJeIspravnaLozinka()
    {

        $mail = DB::table('korisnik')->where('korisnicko_ime', $this->korisnicko_ime)
                                     ->select('email_adresa')
                                     ->get();

        $broj = DB::table('korisnik')->select('lozinka')
                                      ->where('korisnicko_ime', $this->korisnicko_ime)
                                      ->where('email_adresa', $mail[0]['email_adresa'])
                                      ->get();

        if($broj[0]['lozinka']== $this->lozinka)
        {
            return true;
        }

        return false;
    }

    public function prijavljivanjeKorisnika()
    {

        if(!$this->proveriDaLiPostojiKorisnikSaKorisnickimImenom())
        {
            return ["Status"=>"Nevalidno korisnicko ime!"];
        }

        $pomocna = DB::table('korisnik')->where('korisnicko_ime', $this->korisnicko_ime)->select('email_adresa','tip_korisnika')->get();

        $mail=$pomocna[0]['email_adresa'];
        $tip=$pomocna[0]['tip_korisnika'];

        if($this->proveriDaLiJeKorisnikBlokiran($mail))
        {
            return ["Status"=>"Korisnik je trajno blokiran!"];
        }

        if(!$this->proveriDaLiJeIspravnaLozinka())
        {
            return ["Status"=>"Neispravna lozinka!"];
        }

        return ["Status"=>"Uspesno prijavljivanje!","Tip"=>$tip];
    }

    public function izmenaKorisnika()
    {
        DB::table('korisnik')->where('korisnicko_ime', $this->korisnicko_ime)
        ->where('email_adresa', $this->email_adresa)
        ->where('tip_korisnika', $this->tip_korisnika)
        ->update([
            'ime' => $this->ime,
            'prezime' => $this->prezime,
            'datum_rodjenja' => $this->datum_rodjenja,
            'adresa' => $this->adresa,
            'grad' => $this->grad,
            'lozinka' => $this->lozinka,
            'kontakt_telefon' => $this->kontakt_telefon
        ]);

        return "Uspesna izmena!";
    }

    public function vratiKorisnikaPremaKorisnickomImenu($korisnickoIme)
    {
        return DB::table('korisnik')->where('korisnicko_ime', $korisnickoIme)->get();
    }





    //OCENE

    public function kreirajIdZaOcene()
    {
        $broj = DB::table('indeksi')->where('ime_tabele', 'ocene')->select('indeks')->get();
        DB::table('indeksi')->where('ime_tabele', 'ocene')->update(['indeks' => ($broj[0]['indeks']+1)]);
        return $broj[0]['indeks'];
    }


    public function vratiUkupanBrojRedovaUOcenama()
    {
        return DB::table('ocene')->count();
    }

    public function proveriDaLiJeKorisnikVecOcenioKorisnika($korisnikKojiOcenjuje, $korisnikKogaOcenjuje)
    {

        $broj = DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $korisnikKogaOcenjuje)
                                  ->where('korisnicko_ime_korisnika_koji_ocenjuje', $korisnikKojiOcenjuje)
                                  ->count();

        if($broj[0]['count']>0)
        {
            return true;
        }
        return false;
    }

    public function oceniKorisnika($ocena, $korisnikKogaOcenjuje)
    {

        if($this->korisnicko_ime === $korisnikKogaOcenjuje)
        {
            return "Nemate mogucnost da ocenite samog sebe!";
        }

        if($this->vratiUkupanBrojRedovaUOcenama()>0)
        {
            if($this->proveriDaLiJeKorisnikVecOcenioKorisnika($this->korisnicko_ime, $korisnikKogaOcenjuje))
            {
                return "Nemate mogucnost ponovnog ocenjivanja korisnika!";
            }
        }
        $this->id_ocene = intval($this->kreirajIdZaOcene());

        DB::table('ocene')->insert([
            'id_ocene' => $this->id_ocene,
            'korisnicko_ime_ocenjenog_korisnika' => $korisnikKogaOcenjuje,
            'korisnicko_ime_korisnika_koji_ocenjuje' => $this->korisnicko_ime,
            'ocena' => $ocena
        ]);

        DB::table('dodatne_ocene')->insert([
            'id_ocene' => $this->id_ocene,
            'korisnicko_ime_ocenjenog_korisnika' => $korisnikKogaOcenjuje,
            'korisnicko_ime_korisnika_koji_ocenjuje' => $this->korisnicko_ime,
            'ocena' => $ocena
        ]);

        return "Uspesno ocenjen korisnik!";
    }


   public function vratiProsecnuOcenuKorisnika()
   {
        $string="SELECT avg(ocena) FROM ocene WHERE korisnicko_ime_ocenjenog_korisnika = '".$this->korisnicko_ime."'";

        $ocena = DB::raw($string);

        return $ocena[0]['system.avg(ocena)'];
   }





   //KONVERTOVANJE DATUMA

   public function vratiDatumRodjenja($korisnickoIme)
   {
        $datum = DB::table('korisnik')->where('korisnicko_ime', $korisnickoIme)->get();

        return $datum[0]['datum_rodjenja']->toDateTime()->format('Y-m-d');
   }

   public function vratiDatumRegistrovanja($korisnickoIme)
   {
        $datum = DB::table('korisnik')->where('korisnicko_ime', $korisnickoIme)->get();

        return $datum[0]['datum_registrovanja']->toDateTime()->format('Y-m-d');
   }







   //BRISANJE NALOGA

   public function obrisiIzTabeleOcene()
   {

        if(DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $this->korisnicko_ime)->count()>0)
        {
            $ocena = DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $this->korisnicko_ime)->get();
            DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $this->korisnicko_ime)->deleteRow();
            DB::table('dodatne_ocene')->where('id_ocene', intval($ocena[0]['id_ocene']));
        }

        $korisnik = DB::table('korisnik')->get();

        foreach($korisnik as $value)
        {
            if(DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $value['korisnicko_ime'])->where('korisnicko_ime_korisnika_koji_ocenjuje', $this->korisnicko_ime)->count()>0)
            {
                $ocena = DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $value['korisnicko_ime'])->where('korisnicko_ime_korisnika_koji_ocenjuje', $this->korisnicko_ime)->get();
                DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $value['korisnicko_ime'])->where('korisnicko_ime_korisnika_koji_ocenjuje', $this->korisnicko_ime)->deleteRow();
                DB::table('dodatne_ocene')->where('id_ocene', intval($ocena[0]['id_ocene']));
            }
        }

   }

   public function obrisiIzTabeleOglasi()
   {

        $string = "SELECT id_oglasa FROM oglasi WHERE vrsta_oglasa IN ('Oglas Za Literaturu', 'Oglas Za Stan', 'Oglas Za Posao') AND korisnicko_ime_izdavaca_oglasa = '".$korisnickoIme."'";
          $oglasi = DB::raw($string);

          foreach($oglasi as $value)
          {
              $this->brisanjeOglasa(intval($value['id_oglasa']));
          }
   }

   public function obrisiIzTabeleKomentari()
   {

        $oglasi = DB::table('oglasi')->get();
        foreach($oglasi as $value)
        {
            if(DB::table('komentari')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_korisnika_koji_komentarise', $this->korisnicko_ime)->count()>0)
            {
                $komentar = DB::table('komentari')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_korisnika_koji_komentarise', $korisnickoIme)->get();
                DB::table('komentari')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_korisnika_koji_komentarise', $this->korisnicko_ime)->deleteRow();
                DB::table('dodatni_komentari')->where('id_komentara', intval($komentar[0]['id_komentara']));
            }
        }
   }

   public function obrisiIzTabelePrijaveNaOglase()
   {

        $oglasi = DB::table('oglasi')->get();

        foreach($oglasi as $value)
        {
            if(DB::table('prijave_na_oglase')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_prijavljenog_korisnika', $this->korisnicko_ime)->count()>0)
            {
                $prijava = DB::table('prijave_na_oglase')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_prijavljenog_korisnika', $this->korisnicko_ime)->get();
                DB::table('prijave_na_oglase')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_prijavljenog_korisnika', $this->korisnicko_ime)->deleteRow();
                DB::table('dodatne_prijave_na_oglase')->where('id_prijave', intval($prijava[0]['id_prijave']));
            }
        }
   }

   public function obrisiKorisnika()
   {
        DB::table('korisnik')->where('korisnicko_ime', $this->korisnicko_ime)->deleteRow();

        $this->obrisiIzTabeleOcene();
        $this->obrisiIzTabeleOglasi();
        $this->obrisiIzTabeleKomentari();
        $this->obrisiIzTabelePrijaveNaOglase();

        return "Uspesno obrisan nalog!";
   }


public function brisanjeOglasa($idOglasa)
   {
        $this->brisanjeIzTabelaZaOglase($idOglasa);

        $this->brisanjeOglasaIzTabelePrijaveNaOglase($idOglasa);
        $this->brisanjeOglasaIzTabeleKomentari($idOglasa);


   }

   public function brisanjeIzTabelaZaOglase($idOglasa)
   {

        $oglas = DB::table('dodatni_oglasi')->where('id_oglasa', $idOglasa)->get();
        DB::table('dodatni_oglasi')->where('id_oglasa', $idOglasa)->deleteRow();
        DB::table('oglasi')->where('vrsta_oglasa', $oglas[0]['vrsta_oglasa'])
                           ->where('korisnicko_ime_izdavaca_oglasa', $oglas[0]['korisnicko_ime_izdavaca_oglasa'])
                           ->where('grad', $oglas[0]['grad'])
                           ->where('cena', $oglas[0]['cena'])
                           ->where('id_oglasa', intval($oglas[0]['id_oglasa']))
                           ->deleteRow();

        if($oglas[0]['vrsta_oglasa'] === "Oglas Za Literaturu")
        {
              $oglas = DB::table('dodatni_oglasi_za_literaturu')->where('id_oglasa_za_literaturu', $idOglasa)->get();
              DB::table('dodatni_oglasi_za_literaturu')->where('id_oglasa_za_literaturu', $idOglasa)->deleteRow();
              DB::table('oglasi_za_literaturu')->where('kupujem_prodajem', $oglas[0]['kupujem_prodajem'])
                                             ->where('fakultet', $oglas[0]['fakultet'])
                                             ->where('naziv_literature', $oglas[0]['naziv_literature'])
                                             ->where('id_oglasa_za_literaturu', intval($oglas[0]['id_oglasa_za_literaturu']))
                                             ->deleteRow(); 
        }

        else if($oglas[0]['vrsta_oglasa'] === "Oglas Za Posao")
        {
                $oglas = DB::table('dodatni_oglasi_za_posao')->where('id_oglasa_za_posao', $idOglasa)->get();
                DB::table('dodatni_oglasi_za_posao')->where('id_oglasa_za_posao', $idOglasa)->deleteRow();

                if($oglas[0]['potrebno_radno_iskustvo'])
                {
                  $p = "true";
                }
                else
                {
                  $p = "false";
                }
                $brisanje = "DELETE FROM oglasi_za_posao WHERE naziv_posla = '".$oglas[0]['naziv_posla']."' AND tip_posla = '".$oglas[0]['tip_posla']."' AND potrebno_radno_iskustvo = ".$p." AND id_oglasa_za_posao = ".intval($oglas[0]['id_oglasa_za_posao']);

                DB::raw($brisanje);
        }

        else if($oglas[0]['vrsta_oglasa'] === "Oglas Za Stan")
        {
              $oglas = DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', $idOglasa)->get();
              DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', $idOglasa)->deleteRow();

              if($oglas[0]['garaza'])
              {
                $g = "true";
              }
              else
              {
                $g = "false";
              }
              if($oglas[0]['terasa'])
              {
                $t = "true";
              }
              else
              {
                $t = "false";
              }
              if($oglas[0]['parking'])
              {
                $p = "true";
              }
              else
              {
                $p = "false";
              }
              if($oglas[0]['lift'])
              {
                $l = "true";
              }
              else
              {
                $l = "false";
              }
              if($oglas[0]['novogradnja'])
              {
                $n = "true";
              }
              else
              {
                $n = "false";
              }
              if($oglas[0]['internet'])
              {
                $i = "true";
              }
              else
              {
                $i = "false";
              }
              if($oglas[0]['kablovska'])
              {
                $k = "true";
              }
              else
              {
                $k = "false";
              }
              if($oglas[0]['telefon'])
              {
                $t = "true";
              }
              else
              {
                $t = "false";
              }
              if($oglas[0]['podrum'])
              {
                $pod = "true";
              }
              else
              {
                $pod = "false";
              }

              $brisanje = "DELETE FROM oglasi_za_stan WHERE kuca_stan = '".$oglas[0]['kuca_stan']."' AND cimer_stanar = '".$oglas[0]['cimer_stanar']."' AND broj_soba = ".intval($oglas[0]['broj_soba'])." AND povrsina = ".intval($oglas[0]['povrsina'])." AND tip_grejanja = '".$oglas[0]['tip_grejanja']."' AND opremljenost = '".$oglas[0]['opremljenost']."' AND sprat = ".intval($oglas[0]['sprat'])." AND garaza = ".$g." AND terasa = ".$t." AND parking = ".$p." AND lift = ".$l." AND novogradnja = ".$n." AND internet = ".$i." AND kablovska = ".$k." AND telefon = ".$t." AND podrum = ".$pod." AND id_oglasa_za_stan = ".intval($oglas[0]['id_oglasa_za_stan']);

              DB::raw($brisanje);
        }
   }


   public function brisanjeOglasaIzTabelePrijaveNaOglase($idOglasa)
   {

      if(DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->count()>0)
      {
            $prijava = DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->get();
            DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->deleteRow();
            DB::table('dodatne_prijave_na_oglase')->where('id_prijave_na_oglas', intval($prijava[0]['id_prijave_na_oglas']))->deleteRow();
      }
   }

   public function brisanjeOglasaIzTabeleKomentari($idOglasa)
   {
      if(DB::table('komentari')->where('id_oglasa', $idOglasa)->count()>0)
      {
          $komentar = DB::table('komentari')->where('id_oglasa', $idOglasa)->get();
          DB::table('komentari')->where('id_oglasa', $idOglasa)->deleteRow();
          DB::table('dodatni_komentari')->where('id_komentara', intval($komentar[0]['id_komentara']))->deleteRow();
      }
   }
}
