<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Admin extends Model
{

    public function __construct()
    {
        
    }


	//BLOKIRANJE 

    public function blokirajKorisnika($korisnickoIme)
    {
    	$pomocna = DB::table('korisnik')->where('korisnicko_ime', $korisnickoIme)->select('email_adresa')->get();

    	$mail = $pomocna[0]['email_adresa'];

    	DB::table('blokirani_korisnici')->insert([
    		'email_adresa' => $mail,
    		'datum_blokiranja' => (new \DateTime())->format('Y-m-d')
    	]);

    	$this->obrisiKorisnika($korisnickoIme);
    }

    public function obrisiIzTabeleOcene($korisnickoIme)
    {
        if(DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $korisnickoIme)->count()>0)
        {
            $ocena = DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $korisnickoIme)->get();
            DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $korisnickoIme)->deleteRow();
            DB::table('dodatne_ocene')->where('id_ocene', intval($ocena[0]['id_ocene']));
        }

        $korisnik = DB::table('korisnik')->get();

        foreach($korisnik as $value)
        {
            if(DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $value['korisnicko_ime'])->where('korisnicko_ime_korisnika_koji_ocenjuje', $korisnickoIme)->count()>0)
            {
                $ocena = DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $value['korisnicko_ime'])->where('korisnicko_ime_korisnika_koji_ocenjuje', $korisnickoIme)->get();
                DB::table('ocene')->where('korisnicko_ime_ocenjenog_korisnika', $value['korisnicko_ime'])->where('korisnicko_ime_korisnika_koji_ocenjuje', $korisnickoIme)->deleteRow();
                DB::table('dodatne_ocene')->where('id_ocene', intval($ocena[0]['id_ocene']));
            }
        }
    }

   public function obrisiIzTabeleOglasi($korisnickoIme)
   {
      $string = "SELECT id_oglasa FROM oglasi WHERE vrsta_oglasa IN ('Oglas Za Literaturu', 'Oglas Za Stan', 'Oglas Za Posao') AND korisnicko_ime_izdavaca_oglasa = '".$korisnickoIme."'";
      $oglasi = DB::raw($string);

      foreach($oglasi as $value)
      {
          $this->brisanjeOglasa(intval($value['id_oglasa']));
      }
   }

   public function obrisiIzTabeleKomentari($korisnickoIme)
   {

        $oglasi = DB::table('oglasi')->get();
        foreach($oglasi as $value)
        {
            if(DB::table('komentari')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_korisnika_koji_komentarise', $korisnickoIme)->count()>0)
            {
                $komentar = DB::table('komentari')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_korisnika_koji_komentarise', $korisnickoIme)->get();
                DB::table('komentari')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_korisnika_koji_komentarise', $korisnickoIme)->deleteRow();
                DB::table('dodatni_komentari')->where('id_komentara', intval($komentar[0]['id_komentara']));
            }
        }
   }

   public function obrisiIzTabelePrijaveNaOglase($korisnickoIme)
   {
        $oglasi = DB::table('oglasi')->get();

        foreach($oglasi as $value)
        {
            if(DB::table('prijave_na_oglase')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_prijavljenog_korisnika', $korisnickoIme)->count()>0)
            {
                $prijava = DB::table('prijave_na_oglase')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_prijavljenog_korisnika', $korisnickoIme)->get();
                DB::table('prijave_na_oglase')->where('id_oglasa', intval($value['id_oglasa']))->where('korisnicko_ime_prijavljenog_korisnika', $korisnickoIme)->deleteRow();
                DB::table('dodatne_prijave_na_oglase')->where('id_prijave', intval($prijava[0]['id_prijave']));
            }
        }
   }

   public function obrisiKorisnika($korisnickoIme)
   {
        DB::table('korisnik')->where('korisnicko_ime', $korisnickoIme)->deleteRow();

        $this->obrisiIzTabeleOcene($korisnickoIme);
        $this->obrisiIzTabeleOglasi($korisnickoIme);
        $this->obrisiIzTabeleKomentari($korisnickoIme);
        $this->obrisiIzTabelePrijaveNaOglase($korisnickoIme);

        return "Uspesno blokiran nalog!";
   }




   //BRISANJE OGLASA

   public function brisanjeOglasa($idOglasa)
   {
   		$this->brisanjeIzTabelaZaOglase($idOglasa);

   		$this->brisanjeOglasaIzTabelePrijaveNaOglase($idOglasa);
   		$this->brisanjeOglasaIzTabeleKomentari($idOglasa);


   }

   public function brisanjeIzTabelaZaOglase($idOglasa)
   {

        $oglas = DB::table('dodatni_oglasi')->where('id_oglasa', $idOglasa)->get();
        DB::table('dodatni_oglasi')->where('id_oglasa', $idOglasa)->deleteRow();
        DB::table('oglasi')->where('vrsta_oglasa', $oglas[0]['vrsta_oglasa'])
                           ->where('korisnicko_ime_izdavaca_oglasa', $oglas[0]['korisnicko_ime_izdavaca_oglasa'])
                           ->where('grad', $oglas[0]['grad'])
                           ->where('cena', $oglas[0]['cena'])
                           ->where('id_oglasa', intval($oglas[0]['id_oglasa']))
                           ->deleteRow();

        if($oglas[0]['vrsta_oglasa'] === "Oglas Za Literaturu")
        {
              $oglas = DB::table('dodatni_oglasi_za_literaturu')->where('id_oglasa_za_literaturu', $idOglasa)->get();
              DB::table('dodatni_oglasi_za_literaturu')->where('id_oglasa_za_literaturu', $idOglasa)->deleteRow();
              DB::table('oglasi_za_literaturu')->where('kupujem_prodajem', $oglas[0]['kupujem_prodajem'])
                                             ->where('fakultet', $oglas[0]['fakultet'])
                                             ->where('naziv_literature', $oglas[0]['naziv_literature'])
                                             ->where('id_oglasa_za_literaturu', intval($oglas[0]['id_oglasa_za_literaturu']))
                                             ->deleteRow(); 
        }

        else if($oglas[0]['vrsta_oglasa'] === "Oglas Za Posao")
        {
                $oglas = DB::table('dodatni_oglasi_za_posao')->where('id_oglasa_za_posao', $idOglasa)->get();
                DB::table('dodatni_oglasi_za_posao')->where('id_oglasa_za_posao', $idOglasa)->deleteRow();

                if($oglas[0]['potrebno_radno_iskustvo'])
                {
                  $p = "true";
                }
                else
                {
                  $p = "false";
                }
                $brisanje = "DELETE FROM oglasi_za_posao WHERE naziv_posla = '".$oglas[0]['naziv_posla']."' AND tip_posla = '".$oglas[0]['tip_posla']."' AND potrebno_radno_iskustvo = ".$p." AND id_oglasa_za_posao = ".intval($oglas[0]['id_oglasa_za_posao']);

                DB::raw($brisanje);
        }

        else if($oglas[0]['vrsta_oglasa'] === "Oglas Za Stan")
        {
              $oglas = DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', $idOglasa)->get();
              DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', $idOglasa)->deleteRow();

              if($oglas[0]['garaza'])
              {
                $g = "true";
              }
              else
              {
                $g = "false";
              }
              if($oglas[0]['terasa'])
              {
                $t = "true";
              }
              else
              {
                $t = "false";
              }
              if($oglas[0]['parking'])
              {
                $p = "true";
              }
              else
              {
                $p = "false";
              }
              if($oglas[0]['lift'])
              {
                $l = "true";
              }
              else
              {
                $l = "false";
              }
              if($oglas[0]['novogradnja'])
              {
                $n = "true";
              }
              else
              {
                $n = "false";
              }
              if($oglas[0]['internet'])
              {
                $i = "true";
              }
              else
              {
                $i = "false";
              }
              if($oglas[0]['kablovska'])
              {
                $k = "true";
              }
              else
              {
                $k = "false";
              }
              if($oglas[0]['telefon'])
              {
                $t = "true";
              }
              else
              {
                $t = "false";
              }
              if($oglas[0]['podrum'])
              {
                $pod = "true";
              }
              else
              {
                $pod = "false";
              }

              $brisanje = "DELETE FROM oglasi_za_stan WHERE kuca_stan = '".$oglas[0]['kuca_stan']."' AND cimer_stanar = '".$oglas[0]['cimer_stanar']."' AND broj_soba = ".intval($oglas[0]['broj_soba'])." AND povrsina = ".intval($oglas[0]['povrsina'])." AND tip_grejanja = '".$oglas[0]['tip_grejanja']."' AND opremljenost = '".$oglas[0]['opremljenost']."' AND sprat = ".intval($oglas[0]['sprat'])." AND garaza = ".$g." AND terasa = ".$t." AND parking = ".$p." AND lift = ".$l." AND novogradnja = ".$n." AND internet = ".$i." AND kablovska = ".$k." AND telefon = ".$t." AND podrum = ".$pod." AND id_oglasa_za_stan = ".intval($oglas[0]['id_oglasa_za_stan']);

              DB::raw($brisanje);
        }
   }


   public function brisanjeOglasaIzTabelePrijaveNaOglase($idOglasa)
   {

      if(DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->count()>0)
      {
            $prijava = DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->get();
            DB::table('prijave_na_oglase')->where('id_oglasa', $idOglasa)->deleteRow();
            DB::table('dodatne_prijave_na_oglase')->where('id_prijave_na_oglas', intval($prijava[0]['id_prijave_na_oglas']))->deleteRow();
      }
   }

   public function brisanjeOglasaIzTabeleKomentari($idOglasa)
   {
      if(DB::table('komentari')->where('id_oglasa', $idOglasa)->count()>0)
      {
          $komentar = DB::table('komentari')->where('id_oglasa', $idOglasa)->get();
          DB::table('komentari')->where('id_oglasa', $idOglasa)->deleteRow();
          DB::table('dodatni_komentari')->where('id_komentara', intval($komentar[0]['id_komentara']))->deleteRow();
      }
   }



   //CITANJE SVIH KORISNIKA I SVIH OGLASA

   public function vratiIzdavacaNaOsnovuIdaOglasa($idOglasa)
   {
        $korisnickoIme=DB::table('dodatni_oglasi')->where('id_oglasa', $idOglasa)->select('korisnicko_ime_izdavaca_oglasa')->get();

        return $korisnickoIme[0]['korisnicko_ime_izdavaca_oglasa'];
   }

   public function vratiSveKorisnike()
   {
   		return DB::table('korisnik')->get();
   }

   public function vratiSveOglase()
   {
   		return DB::table('oglasi')->get();
   }

   public function vratiSveIzdavaceOglasaPremaKljucnojReci($deoKorisnickogImena)
   {
      $oglasi = DB::table('oglasi')->get();
      $niz = null;
      $i = 0;
      //$deoKorisnickogImena = $deoKorisnickogImena;
      foreach($oglasi as $value)
      {
          $string = "SELECT * FROM dodatni_oglasi WHERE id_oglasa = ".$value['id_oglasa']." AND korisnicko_ime_izdavaca_oglasa LIKE '".$deoKorisnickogImena."%'";
          if(DB::raw($string)[0]['id_oglasa']===null)
          {
          }
          else
          {
            $niz[$i] = DB::raw($string);
            $i = $i + 1;
          }
      }

      return $niz;
   }
}
