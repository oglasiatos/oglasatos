<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Oglasi;

class OglasiZaLiteraturu extends Oglasi
{
    public $fakultet;
    public $predmet;
    public $naziv_literature;
    public $kupujem_prodajem;
    public $izdavac;

    public function __construct($korisnicko_ime_izdavaca_oglasa, $naslov_oglasa, $grad, $opis_oglasa, $vrsta_oglasa, $cena, $fakultet, $predmet, $naziv_literature, $kupujem_prodajem, $izdavac)
    {
    	parent::__construct($korisnicko_ime_izdavaca_oglasa, $naslov_oglasa, $grad, $opis_oglasa, $vrsta_oglasa, $cena);

    	$this->fakultet = $fakultet;
    	$this->predmet = $predmet;
    	$this->naziv_literature = $naziv_literature;
    	$this->kupujem_prodajem = $kupujem_prodajem;
    	$this->izdavac = $izdavac;
    }

    public function vratiSveOglaseZaLiteraturu()
    {
    	return DB::table('oglasi_za_literaturu')->get();
    }

    public function vratiOglasZaLiteraturuNaOsnovuId($id)
    {
    	return DB::table('dodatni_oglasi_za_literaturu')->where('id_oglasa_za_literaturu', $id)->get();
    }

    public function dodajOglasZaLiteraturu()
    {
    	Oglasi::dodajOglas();
    	DB::table('oglasi_za_literaturu')->insert([
    		'id_oglasa_za_literaturu' => $this->id_oglasa,
    		'fakultet' => $this->fakultet,
    		'predmet' => $this->predmet,
    		'naziv_literature' => $this->naziv_literature,
    		'kupujem_prodajem' => $this->kupujem_prodajem,
    		'izdavac' => $this->izdavac
    	]);

        DB::table('dodatni_oglasi_za_literaturu')->insert([
            'id_oglasa_za_literaturu' => $this->id_oglasa,
            'fakultet' => $this->fakultet,
            'predmet' => $this->predmet,
            'naziv_literature' => $this->naziv_literature,
            'kupujem_prodajem' => $this->kupujem_prodajem,
            'izdavac' => $this->izdavac
        ]);
    }

    public function obrisiOglasZaLiteraturu()
    {
    	Oglasi::obrisiOglas($this->id_oglasa);
        $oglas = DB::table('dodatni_oglasi_za_literaturu')->where('id_oglasa_za_literaturu', $this->id_oglasa)->get();
    	DB::table('dodatni_oglasi_za_literaturu')->where('id_oglasa_za_literaturu', $this->id_oglasa)->deleteRow();
        DB::table('oglasi_za_literaturu')->where('kupujem_prodajem', $oglas[0]['kupujem_prodajem'])
                                         ->where('fakultet', $oglas[0]['fakultet'])
                                         ->where('naziv_literature', $oglas[0]['naziv_literature'])
                                         ->where('id_oglasa_za_literaturu', intval($oglas[0]['id_oglasa_za_literaturu']))
                                         ->deleteRow();
    }

    public function vratiBrojFakulteta()
    {
    	return DB::table('fakultet')->count();
    }

    public function izlistajSveFakulteteJednogGrada($grad)
    {
        return DB::table('fakultet')->where('grad', $grad)->select('naziv_fakulteta')->get();
    }

    public function izlistajSveUniverzitetskeGradove()
    {
    	return DB::table('fakultet')->select('grad')->get();
    }

    public function izlistajSveFakultete()
    {
        return DB::table('fakultet')->select('naziv_fakulteta')->get();
    }


    

    public function kreirajNizFakulteta()
    {
        $fakultet = DB::table('fakultet')->select('naziv_fakulteta')->get();
        $niz = "";
        $granica = DB::table('fakultet')->count();
        $i = 0;
        foreach($fakultet as $value)
        {
            $niz .= "'".$value['naziv_fakulteta']."'";
            if($i < ($granica - 1))
            {
                $niz .= ", ";
            }
            $i = $i + 1;
        }

        return $niz;
    }





    public function filtriranjeOglasaZaLiteraturu($kljucnaRecZaNazivLiterature, $fakultet, $kupovinaProdaja, $minimalnaCena, $maksimalnaCena, $grad)
    {

        $query = "SELECT id_oglasa_za_literaturu FROM oglasi_za_literaturu WHERE ";

        if($kupovinaProdaja === "")
        {
            $query .= " kupujem_prodajem in ('Kupujem', 'Prodajem') ";
        }
        else
        {
            $query .= " kupujem_prodajem = '".$kupovinaProdaja."' ";
        }



        if($fakultet === "")
        {
            $query .= " AND fakultet in (".$this->kreirajNizFakulteta().") ";
        }
        else
        {
            $query .= " AND fakultet = '".$fakultet."' ";
        }



        $idjevi = DB::raw($query);
        $niz = null;
        $i = 0;

        if(!is_null($idjevi)[0])
        {
            if($kljucnaRecZaNazivLiterature === "")
            {
                foreach($idjevi as $value)
                {
                    $niz[$i] = $value['id_oglasa_za_literaturu'];
                    $i = $i + 1;
                }
            }
            else
            {
                foreach($idjevi as $value)
                {
                    $query = "SELECT id_oglasa_za_literaturu FROM dodatni_oglasi_za_literaturu WHERE id_oglasa_za_literaturu = ".$value['id_oglasa_za_literaturu']." AND naziv_literature LIKE '%".$kljucnaRecZaNazivLiterature."%'";
                    if(!is_null(DB::raw($query)[0]))
                    {
                        $niz[$i] = DB::raw($query)[0]['id_oglasa_za_literaturu'];
                        $i = $i + 1;
                    }
                }
            }

            return Oglasi::filtrirajOglase("Oglas Za Literaturu", $grad, $minimalnaCena, $maksimalnaCena, $niz);
        }
        else
        {
            return null;
        }
    }

    
}
