<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProfilneSlike extends Model
{
    public $korisnicko_ime;
    public $putanja;
    public $naziv_slike;
    public $sadrzaj_slike;

    public function __construct($korisnicko_ime, $putanja, $naziv_slike, $sadrzaj_slike)
    {
    	$this->korisnicko_ime = $korisnicko_ime;
    	$this->putanja = $putanja;
    	$this->naziv_slike = $naziv_slike;
    	$this->sadrzaj_slike = $sadrzaj_slike;
    }

    public function dodajProfilnuSliku()
    {
        
    	$string = "INSERT INTO profilne_slike(korisnicko_ime, putanja, naziv_slike, sadrzaj_slike) values ('".$this->korisnicko_ime."', '".$this->putanja."', '".$this->naziv_slike."', textAsBlob('".$this->sadrzaj_slike."'))";
    	DB::raw($string);
    }

    public function vratiProfilnuSliku()
    {
    	$slika = DB::raw("SELECT blobastext(sadrzaj_slike) FROM profilne_slike WHERE korisnicko_ime = '".$this->korisnicko_ime."'");

    	return $slika[0]['system.blobastext(sadrzaj_slike)'];
    }
}
