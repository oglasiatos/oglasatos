<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Slike extends Model
{
    public $id_slike;
    public $id_oglasa;
    public $putanja;
    public $naziv_slike;
    public $sadrzaj_slike;

    public function __construct($id_oglasa, $putanja, $naziv_slike, $sadrzaj_slike)
    {
    	$this->id_oglasa = $id_oglasa;
    	$this->putanja = $putanja;
    	$this->naziv_slike = $naziv_slike;
    	$this->sadrzaj_slike = $sadrzaj_slike;
    }

    public function kreirajNoviIdSlike()
    {
    	$broj = DB::table('indeksi')->where('ime_tabele', 'slike')->select('indeks')->get();
    	DB::table('indeksi')->where('ime_tabele','slike')->update(['indeks' => ($broj[0]['indeks']+1)]);
    	return $broj[0]['indeks'];
    }

    public function dodajSlikuOglasa()
    {
    	$this->id_slike = $this->kreirajNoviIdSlike();

    	$string = "INSERT INTO slike(id_oglasa, id_slike, putanja, naziv_slike, sadrzaj_slike) values (".$this->id_oglasa.", ".$this->id_slike.", '".$this->putanja."', '".$this->naziv_slike."', textAsBlob('".$this->sadrzaj_slike."'))";
    	DB::raw($string);
    }



    public function vratiSveSlikeJednogOglasa()
    {
    	$slika = DB::raw("SELECT blobastext(sadrzaj_slike) FROM slike WHERE id_oglasa = ".$this->id_oglasa);

        $niz = null;
        $i = 0;

        if(!is_null($slika[0]))
        {
        	foreach($slika as $value)
	        {
	        	$niz[$i] = $value['system.blobastext(sadrzaj_slike)'];
	        	$i = $i + 1;
	        }

	        return $niz;
        }
        else 
        	return null;

    }
}
