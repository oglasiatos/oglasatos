<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Poruke extends Model
{
	public $id_poruke;
	public $korisnicko_ime_posiljaoca;
	public $korisnicko_ime_primaoca;
	public $sadrzaj_poruke;
	public $datum_i_vreme_slanja;
	public $procitana_poruka;

    public function __construct($koSalje, $koPrima, $poruka)
    {
    	//$this->id_poruke = $this->kreirajNoviId();
    	$this->korisnicko_ime_posiljaoca = $koSalje;
    	$this->korisnicko_ime_primaoca = $koPrima;
    	$this->sadrzaj_poruke = $poruka;
    	//$this->datum_i_vreme_slanja = (new \DateTime())->format('Y-m-d H-i-s');
    	$this->procitana_poruka = false;
    }

    public function vratiBrojPoruka()
    {
    	return DB::table('poruke')->count();
    }

    public function kreirajNoviId()
    {
    	$broj = DB::table('indeksi')->where('ime_tabele', 'poruke')->select('indeks')->get();
        DB::table('indeksi')->where('ime_tabele', 'poruke')->update(['indeks' => ($broj[0]['indeks']+1)]);
        return $broj[0]['indeks'];
    }

    public function proveriDaLiPrimalacPostoji($koPrima)
    {
        $broj = DB::table('korisnik')->where('korisnicko_ime', $koPrima)->count();
        if($broj[0]['count']===0)
        {
            return false;
        }
        return true;
    }

    public function posaljiPoruku($poruka, $koSalje, $koPrima)
    {

        if(!$this->proveriDaLiPrimalacPostoji($koPrima))
        {
            return "Primalac ne postoji!";
        }
        $this->id_poruke = $this->kreirajNoviId();
        
    	$string = "INSERT INTO poruke(id_poruke, korisnicko_ime_posiljaoca, korisnicko_ime_primaoca, sadrzaj_poruke, procitana_poruka) values (".$this->id_poruke.", '".$koSalje."','".$koPrima."', '".$poruka."', false)";

    	DB::raw($string);

        $string = "INSERT INTO dodatne_poruke(id_poruke, korisnicko_ime_posiljaoca, korisnicko_ime_primaoca, sadrzaj_poruke, procitana_poruka) values (".$this->id_poruke.", '".$koSalje."','".$koPrima."', '".$poruka."', false)";

        DB::raw($string);

    	return "Uspesno poslata poruka!";
    }

    public function obrisiPoruku($idPoruke)
    {
        $poruka = DB::table('dodatne_poruke')->where('id_poruke', $idPoruke)->get();
    	DB::table('dodatne_poruke')->where('id_poruke', $idPoruke)->deleteRow();
        if($poruka[0]['procitana_poruka'])
        {
            $proc="true";
        }
        else
        {
            $proc = "false";
        }

        $string = "DELETE FROM poruke WHERE korisnicko_ime_primaoca = '".$poruka[0]['korisnicko_ime_primaoca']."' AND korisnicko_ime_posiljaoca = '".$poruka[0]['korisnicko_ime_posiljaoca']."' AND procitana_poruka = ".$proc." AND id_poruke = ".intval($poruka[0]['id_poruke']);
        DB::raw($string);

    	return "Uspesno obrisana poruka!";
    }



    public function proveriDaLiJePorukaProcitana($idPoruke)
    {
        $poruka = DB::table('dodatne_poruke')->where('id_poruke', $idPoruke)->get();
    	$string = "SELECT procitana_poruka FROM dodatne_poruke WHERE id_poruke = ".$idPoruke;

    	if(DB::raw($string)[0]['procitana_poruka'])
    	{
    		return "Procitana";
    	}
    	else
    	{
    		return "Neprocitana";
    	}
    }

    public function vratiCeluKonverzacijuDvaKorisnika($koSalje, $koPrima)
    {
    	$string = "SELECT id_poruke FROM poruke WHERE korisnicko_ime_primaoca IN ('".$koSalje."', '".$koPrima."') AND korisnicko_ime_posiljaoca IN ('".$koSalje."', '".$koPrima."')";

    	$id = DB::raw($string);

        $i = 0;
        $j = 0;
        $niz=null;

        foreach($id as $value)
        {
            $string = "SELECT id_poruke, korisnicko_ime_posiljaoca, sadrzaj_poruke, procitana_poruka FROM dodatne_poruke WHERE id_poruke = ".$value['id_poruke'];

            $niz[$i] = DB::raw($string);
            $i = $i+1;

        }

        return $niz;
    }

    public function vratiSveKorisnikeKojiSuSlaliPorukeAktivnomKorisniku($aktivanKorisnik)
    {
        return DB::table('poruke')->where('korisnicko_ime_primaoca', $aktivanKorisnik)->select('korisnicko_ime_posiljaoca')->get();
    }

    public function vratiSveKorisnikeKojiSuSlaliPorukeAktivnomKorisnikuANijeIhProcitao($aktivanKorisnik)
    {
    	$string = "SELECT korisnicko_ime FROM korisnik";

    	$sviKorisnici = DB::raw($string);

    	$i = 0;
        $j = 0;
        $niz=null;

       

    	foreach($sviKorisnici as $value)
    	{  
            if(intval(DB::raw("SELECT COUNT(*) FROM poruke WHERE korisnicko_ime_primaoca = '".$aktivanKorisnik."' AND korisnicko_ime_posiljaoca = '".$value['korisnicko_ime']."' AND procitana_poruka = false ")[0]['count'])>0)
            {
                
        		$niz[$i] = DB::raw("SELECT id_poruke, korisnicko_ime_posiljaoca, sadrzaj_poruke, procitana_poruka FROM poruke WHERE korisnicko_ime_primaoca = '".$aktivanKorisnik."' AND korisnicko_ime_posiljaoca = '".$value['korisnicko_ime']."' AND procitana_poruka = false ");

        		$i = $i + 1;
            }

            if(intval(DB::raw("SELECT COUNT(*) FROM poruke WHERE korisnicko_ime_primaoca = '".$value['korisnicko_ime']."' AND korisnicko_ime_posiljaoca = '".$aktivanKorisnik."' AND procitana_poruka = false ")[0]['count'])>0)
            {
                $niz[$i] = DB::raw("SELECT id_poruke, korisnicko_ime_primaoca, sadrzaj_poruke, procitana_poruka FROM poruke WHERE korisnicko_ime_primaoca = '".$value['korisnicko_ime']."' AND korisnicko_ime_posiljaoca = '".$aktivanKorisnik."' AND procitana_poruka = false ");
                $i = $i + 1;
            }
    	}

    	return $niz;
    }

    public function vratiSveKorisnikeKojiSuSlaliPorukeAktivnomKorisnikuAProcitaoIhJe($aktivanKorisnik)
    {
        $string = "SELECT korisnicko_ime FROM korisnik";

        $sviKorisnici = DB::raw($string);

        $i = 0;
        $j = 0;
        $niz=null;

        foreach($sviKorisnici as $value)
        {  

            if(intval(DB::raw("SELECT COUNT(*) FROM poruke WHERE korisnicko_ime_primaoca = '".$aktivanKorisnik."' AND korisnicko_ime_posiljaoca = '".$value['korisnicko_ime']."' AND procitana_poruka = true ")[0]['count'])>0)
            {
                
                $niz[$i] = DB::raw("SELECT id_poruke, korisnicko_ime_posiljaoca, sadrzaj_poruke, procitana_poruka FROM poruke WHERE korisnicko_ime_primaoca = '".$aktivanKorisnik."' AND korisnicko_ime_posiljaoca = '".$value['korisnicko_ime']."' AND procitana_poruka = true ");

                $i = $i + 1;
            }

            if(intval(DB::raw("SELECT COUNT(*) FROM poruke WHERE korisnicko_ime_primaoca = '".$value['korisnicko_ime']."' AND korisnicko_ime_posiljaoca = '".$aktivanKorisnik."' AND procitana_poruka = true")[0]['count'])>0)
            {
                $niz[$i] = DB::raw("SELECT id_poruke, korisnicko_ime_primaoca, sadrzaj_poruke, procitana_poruka FROM poruke WHERE korisnicko_ime_primaoca = '".$value['korisnicko_ime']."' AND korisnicko_ime_posiljaoca = '".$aktivanKorisnik."' AND procitana_poruka = true");
                $i = $i + 1;
            }
        }

        return $niz;
    }

    public function oznaciDaSuPorukeProcitane()
    {
    	$string = "SELECT * FROM poruke WHERE korisnicko_ime_primaoca = '".$this->korisnicko_ime_primaoca."' AND korisnicko_ime_posiljaoca = '".$this->korisnicko_ime_posiljaoca."' AND procitana_poruka = false";

    	$redovi = DB::raw($string);

    	$i=0;

    	foreach($redovi as $value)
    	{
            $poruka = DB::table('poruke')->where('korisnicko_ime_primaoca', $this->korisnicko_ime_primaoca)->where('korisnicko_ime_posiljaoca', $this->korisnicko_ime_posiljaoca)->get();
            $id = $this->kreirajNoviId();
    		$stringZaBrisanje = "DELETE FROM poruke WHERE korisnicko_ime_primaoca = '".$this->korisnicko_ime_primaoca."' AND korisnicko_ime_posiljaoca = '".$this->korisnicko_ime_posiljaoca."' AND procitana_poruka = false AND id_poruke = ".$redovi[$i]['id_poruke'];
    		DB::raw($stringZaBrisanje);
    		$stringZaDodavanje = "INSERT INTO poruke(id_poruke, korisnicko_ime_posiljaoca, korisnicko_ime_primaoca, sadrzaj_poruke, procitana_poruka) values (".$id.", '".$redovi[$i]['korisnicko_ime_posiljaoca']."', '".$redovi[$i]['korisnicko_ime_primaoca']."', '".$redovi[$i]['sadrzaj_poruke']."', true)";
    		DB::raw($stringZaDodavanje);

            $stringZaBrisanje = "DELETE FROM dodatne_poruke WHERE id_poruke = ".intval($poruka[0]['id_poruke']);
            DB::raw($stringZaBrisanje);
            $stringZaDodavanje = "INSERT INTO dodatne_poruke(id_poruke, korisnicko_ime_posiljaoca, korisnicko_ime_primaoca, sadrzaj_poruke, procitana_poruka) values (".$id.", '".$redovi[$i]['korisnicko_ime_posiljaoca']."', '".$redovi[$i]['korisnicko_ime_primaoca']."', '".$redovi[$i]['sadrzaj_poruke']."', true)";
            DB::raw($stringZaDodavanje);
    		$i = $i + 1;
    	}

    	return "Uspesno iscitavanje poruka!";
    }

}
