<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrijaveNaOglase extends Model
{
    public $id_prijave_na_oglas;
    public $id_oglasa;
    public $korisnicko_ime_izdavaca_oglasa;
    public $korisnicko_ime_prijavljenog_korisnika;
    //public $datum_prijavljivanja_na_oglas;

    public function __construct($idOglasa, $korisnickoImePrijavljenogKorisnika)
    {
    	//$this->id_prijave_na_oglas = $this->kreirajIdPrijaveNaOglas();
    	$this->id_oglasa = $idOglasa;
    	//$this->korisnicko_ime_izdavaca_oglasa=$korisnickoImeIzdavacaOglasa;
    	$this->korisnicko_ime_prijavljenog_korisnika = $korisnickoImePrijavljenogKorisnika;
    	//$this->datum_prijavljivanja_na_oglas = (new \DateTime())->format('Y-m-d');
    }

    public function vratiDatumPrijavljivanjaNaOglas($idPrijave)
    {
    	$emp = DB::table('prijave_na_oglase')->where('id_prijave_na_oglas', $idPrijave)->get();

    	return $emp[0]['datum_prijavljivanja_na_oglas'];
    }

    public function kreirajIdPrijaveNaOglas()
    {
    	$broj = DB::table('indeksi')->where('ime_tabele', 'prijave_na_oglase')->select('indeks')->get();
        DB::table('indeksi')->where('ime_tabele', 'prijave_na_oglase')->update(['indeks' => ($broj[0]['indeks']+1)]);
        return $broj[0]['indeks'];
    }

    public function vratiUkupanBrojPrijava()
    {
    	return DB::table('prijave_na_oglase')->count();
    }

    public function vratiKorisnickaImenaSvihPrijavljenihKorisnikaNaOglas()
    {
    	$string = "SELECT korisnicko_ime_prijavljenog_korisnika FROM prijave_na_oglase WHERE id_oglasa = ".$this->id_oglasa;

    	return DB::raw($string);
    }

    public function prijaviSeNaOglas()
    {
        $this->id_prijave_na_oglas = $this->kreirajIdPrijaveNaOglas();
        DB::table('prijave_na_oglase')->insert([
            'id_prijave_na_oglas' => $this->id_prijave_na_oglas,
            'id_oglasa' => $this->id_oglasa,
            'korisnicko_ime_prijavljenog_korisnika' => $this->korisnicko_ime_prijavljenog_korisnika
        ]);
    }



}
