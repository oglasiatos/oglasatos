<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OglasiZaLiteraturu;
use App\Oglasi;
use App\Slike;
use App\Korisnik;
use App\ProfilneSlike;


class OglasLiteratura extends Controller
{
    public function ajaxRequestPost(Request $request)
    {
    	$json = $_POST;
    	if($request->session()->has('oglasiatos_korisnik'))
      	{
	         $korisnik=$request->session()->get('oglasiatos_korisnik');

		     $oglas=new OglasiZaLiteraturu($korisnik, $json['naslovOglasa'], $json['grad'], $json['opis'], "Oglas Za Literaturu", intval($json['cena']), $json['fakultet'], $json['predmet'], $json['nazivLiterature'], $json['kupujemProdajem'], $json['izdavac']);

         //echo var_dump($oglas);

		     $oglas->dodajOglasZaLiteraturu();
          $id_oglasa=$oglas->id_oglasa;
         foreach (glob("uploads/".$korisnik."*") as $filename) {
              $type = pathinfo($filename, PATHINFO_EXTENSION);
              $data = file_get_contents($filename);
              $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
              $sl= new Slike( $id_oglasa, $filename, "Lit". $id_oglasa.".".$type, $base64);
              $sl->dodajSlikuOglasa();

          }
          array_map('unlink', glob("uploads/".$korisnik."*"));

         	return response()->json(['Status'=>"Uspesno!"]);
         //return var_dump($as[0]['datum_registrovanja']->toDateTime()->format('Y-m-d'));
      	}
      else
      {
      	 return response()->json(['Status'=>"Prijavi se!"]);
      }
    }

    public function velikiOglasZaLiteraturu($a, Request $request)
    {
      $o = new OglasiZaLiteraturu(null, null, null, null, null, null, null, null, null, null, null);

      $oglasLiteratura = $o->vratiOglasZaLiteraturuNaOsnovuId($a);
      $oglas = $o->vratiOglasNaOsnovuIda($a);

      $k = new Korisnik(null, null, null, null, null, null, $oglas[0]['korisnicko_ime_izdavaca_oglasa'], null, null, null, null);

      $slanje = null;

      $korisnik = $k->vratiKorisnikaPremaKorisnickomImenu($oglas[0]['korisnicko_ime_izdavaca_oglasa']);

      $slanje['id_oglasa']=$a;
      $slanje['korisnicko_ime_izdavaca_oglasa'] = $oglas[0]['korisnicko_ime_izdavaca_oglasa'];
      $slanje['ime'] = $korisnik[0]['ime'];
      $slanje['prezime'] = $korisnik[0]['prezime'];
      $slanje['grad_korisnika'] = $korisnik[0]['grad'];
      $slanje['email_adresa'] = $korisnik[0]['email_adresa'];
      $slanje['kontakt_telefon'] = $korisnik[0]['kontakt_telefon'];
      $slanje['prosecna_ocena'] = $k->vratiProsecnuOcenuKorisnika();

      $slanje['naslov_oglasa'] = $oglas[0]['naslov_oglasa'];
      $slanje['cena'] = $oglas[0]['cena'];
      $slanje['opis_oglasa'] = $oglas[0]['opis_oglasa'];
      $slanje['grad_oglasa'] = $oglas[0]['grad'];

      $slanje['naziv_literature'] = $oglasLiteratura[0]['naziv_literature'];
      $slanje['fakultet'] = $oglasLiteratura[0]['fakultet'];
      $slanje['predmet'] = $oglasLiteratura[0]['predmet'];
      $slanje['kupujem_prodajem'] = $oglasLiteratura[0]['kupujem_prodajem'];
      $slanje['izdavac'] = $oglasLiteratura[0]['izdavac'];

      $sl= new ProfilneSlike( $slanje['korisnicko_ime_izdavaca_oglasa'], null, null, null);
      $slanje['profilna']= $sl->vratiProfilnuSliku();


       $sl= new Slike( $a, null, null, null);
       $slike=$sl->vratiSveSlikeJednogOglasa();
       $i=0;
       if(is_null($slike))
       {
          $slanje["slike"]=null;
       }
       else 
       {
         foreach ($slike as $value) {
            $slanje["slike"][$i]=$value;
            $i=$i+1;
         }
       }



      if($request->session()->has('oglasiatos_korisnik'))
      {   
            $korisnik=$request->session()->get('oglasiatos_korisnik');
            $slanje["ulogovan"]=$korisnik;
      }
      else
      {
            $slanje["ulogovan"]="Nije ulogovan!!!";
      }
      $k2 = new Korisnik(null, null, null, null, null, null, null, null, null, null, null);
      $slanje['tip_korisnika_koji_gleda_oglas'] = $k2->vratiKorisnikaPremaKorisnickomImenu($korisnik)[0]['tip_korisnika'];

      return view ('oglasLiteratura', $slanje);

    }
}
