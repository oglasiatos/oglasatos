<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use DB;
use App\OglasiZaLiteraturu;

class TestController extends Controller
{

	public function proveriDaLiJeDodat($slanje, $grad)
    {
        if(!is_null($slanje[0]))
        {
            foreach($slanje as $value)
            {
                if($value['grad']===$grad)
                    return true;
            }
            return false;
        }
        return false;
    }
	

    public function testFunkcija()
    {
    	$oglasi = new OglasiZaLiteraturu(null, null, null, null, null, null, null, null, null, null, null);

        $rezultat = $oglasi->izlistajSveUniverzitetskeGradove();

        $slanje = null;
        $i = 0;

        foreach($rezultat as $value)
        {
        	//return $value['grad'];
            if(!$this->proveriDaLiJeDodat($slanje, $value['grad']))
            {
                $slanje[$i]["grad"] = $value['grad'];
                $i = $i + 1;
            }
        }
        return $slanje;
    }
}
