<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OglasiZaPosao;
use App\Slike;

class mojiOglasiPosao extends Controller
{
    public function ajaxRequestPost(Request $request)
    {
      $json = $_POST; 
      if($request->session()->has('oglasiatos_korisnik'))
      {   
        	$korisnik=$request->session()->get('oglasiatos_korisnik');
          	$o=new OglasiZaPosao(null, null, null, null, null, null, null, null, null, null, null, null);
          	$oglasi=$o->vratiSveOglaseZaPosaoJednogKorisnika($korisnik);
          	$i=0;
          	$slanje=null;
          	foreach($oglasi as $value)
        		{
        			$posao=$o->vratiOglasZaPosaoPremaIdu(intval($value['id_oglasa']));
        			$slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
        			$slanje[$i]["naziv_oglasa"]=$value['naslov_oglasa'];
        			$slanje[$i]["izdavac_oglasa"]=$value['korisnicko_ime_izdavaca_oglasa'];
        			$slanje[$i]["opis"]=$value['opis_oglasa'];
        			$slanje[$i]["cena"]=$value['cena'];
        			$slanje[$i]["naziv_posla"]=$posao[0]['naziv_posla'];
              $slanje[$i]["potrebno_radno_iskustvo"]=$posao[0]['potrebno_radno_iskustvo'];
              $slanje[$i]["lokacija_radnog_mesta"]=$posao[0]['lokacija_radnog_mesta'];
              $slanje[$i]["radno_vreme"]=$posao[0]['radno_vreme'];
              $slanje[$i]["tip_posla"]=$posao[0]['tip_posla'];

              $sl= new Slike( intval($value['id_oglasa']), null, null, null);
              $slike=$sl->vratiSveSlikeJednogOglasa();
              if(is_null($slike))
              {
                 $slanje[$i]["slike"]=null;
              }
              else 
              {
                 $slanje[$i]["slike"]=$slike[0];
              }


        			

        			$i=$i+1;
        		}
            return response()->json($slanje);
          	//return var_dump($slanje);
      }
      else
      {
      		return response()->json(['Status'=>"Prijavi se!"]);
      }
    }

    
}
