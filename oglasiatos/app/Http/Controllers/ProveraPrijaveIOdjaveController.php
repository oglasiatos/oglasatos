<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Korisnik;

class ProveraPrijaveIOdjaveController extends Controller
{
    public function proveriPrijavu(Request $request)
    {
    	if($request->session()->has('oglasiatos_korisnik'))
	    {   
	        $korisnik=$request->session()->get('oglasiatos_korisnik');
	        $slanje["ulogovan"]=$korisnik;
	    }
	    else
	    {
	        $slanje["ulogovan"]="Nije ulogovan!!!";
	    }

	    return response()->json($slanje);
    }
}
