<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OglasiZaStan;
use App\Oglasi;
use App\Korisnik;
use App\Slike;
use App\ProfilneSlike;


class OglasStan extends Controller
{
    
    
    public function vratistan($a,Request $request)
    {

      $o=new OglasiZaStan(null, null, null, null, "Oglas Za Stan", null, null, null, null, null, null,null, null, null, null , null , null, null , null, null , null , null , null);
      $o2=new Oglasi(null,null,null,null,null,null);

      


      $n=$o->vratiOglasZaStanPremaIdu($a);
      $n2=$o2->vratiOglasNaOsnovuIda($a);
      $k=new Korisnik(null, null, null, null, null, null, $n2[0]['korisnicko_ime_izdavaca_oglasa'], null, null, null, null);
      $kk=$k->vratiKorisnikaPremaKorisnickomImenu($n2[0]['korisnicko_ime_izdavaca_oglasa']);
      $slanje["ocena"]=$k->vratiProsecnuOcenuKorisnika();
      $slanje["id_oglasa"]=$a;
      $slanje["email_adresa"]=$kk[0]["email_adresa"];
      $slanje["kontakt_telefon"]=$kk[0]["kontakt_telefon"];
      $slanje['kgrad']=$kk[0]["grad"];
      $slanje['ime']=$kk[0]["ime"];
      $slanje["prezime"]=$kk[0]["prezime"];
      if($request->session()->has('oglasiatos_korisnik'))
      {   
            $korisnik=$request->session()->get('oglasiatos_korisnik');
            $slanje["ulogovan"]=$korisnik;
      }
      else
      {
            $slanje["ulogovan"]="Nije ulogovan!!!";
      }
      $k2 = new Korisnik(null, null, null, null, null, null, null, null, null, null, null);
      $slanje['tip_korisnika_koji_gleda_oglas'] = $k2->vratiKorisnikaPremaKorisnickomImenu($korisnik)[0]['tip_korisnika'];
      $slanje["naslov_oglasa"]=$n2[0]['naslov_oglasa'];
      $slanje["cena"]=$n2[0]['cena'];
      $slanje["grad"]=$n2[0]['grad'];
      $slanje["korisnicko_ime_izdavaca_oglasa"]=$n2[0]["korisnicko_ime_izdavaca_oglasa"];
      $slanje['opis_oglasa']=$n2[0]['opis_oglasa'];
      $slanje['adresa_stana']=$n[0]['adresa_stana'];
      $slanje['broj_soba']=$n[0]['broj_soba'];
      $slanje['cimer_stanar']=$n[0]['cimer_stanar'];
      $slanje['garaza']=$n[0]['garaza'];
      $slanje['internet']=$n[0]['internet'];
      $slanje['kablovska']=$n[0]['kablovska'];
      $slanje['kuca_stan']=$n[0]['kuca_stan'];
      $slanje['lift']=$n[0]['lift'];
      $slanje['novogradnja']=$n[0]["novogradnja"];
      $slanje["opremljenost"]=$n[0]["opremljenost"];
      $slanje['parking']=$n[0]["parking"];
      $slanje["podrum"]=$n[0]["podrum"];
      $slanje["povrsina"]=$n[0]["povrsina"];
      $slanje["sprat"]=$n[0]["sprat"];
      $slanje["telefon"]=$n[0]["telefon"];
      $slanje["terasa"]=$n[0]["terasa"];
      $slanje["tip_grejanja"]=$n[0]["tip_grejanja"];

      $sl= new ProfilneSlike( $slanje['korisnicko_ime_izdavaca_oglasa'], null, null, null);
      $slanje['profilna']= $sl->vratiProfilnuSliku();

       $sl= new Slike( $a, null, null, null);
       $slike=$sl->vratiSveSlikeJednogOglasa();
       $i=0;
       if(is_null($slike))
       {
          $slanje["slike"]=null;
       }
       else 
       {
         foreach ($slike as $value) {
            $slanje["slike"][$i]=$value;
            $i=$i+1;
         }
       }




      //$slanje["cena"]=$n[0]['cena'];


      //return var_dump($slanje);
      return view("oglasStan",$slanje);
    }

    
    public function ajaxRequest()
    {

    }

      public function ajaxRequestPost(Request $request)
    {
    	$json = $_POST; 
    	if($request->session()->has('oglasiatos_korisnik'))
        {   
          $korisnik=$request->session()->get('oglasiatos_korisnik');
        	$o=new OglasiZaStan($korisnik, $json["naslovOglasa"], $json["grad"], $json["opis"], "Oglas Za Stan", intval($json["cena"]), $json["tipStanara"], $json["adresa"], intval($json["brSoba"]), $json["povrsina"], $json["tipGrejanja"],$json["opremljenost"], $json["tipStana"], $json["sprat"], $json["garaza"] , $json["terasa"] , $json["parking"], $json["lift"] , $json["novogradnja"], $json["internet"] , $json["kablovska"] , $json["telefon"] , $json["podrum"]);
        	$o->dodajOglasZaStan();

         $id_oglasa=$o->id_oglasa;
         foreach (glob("uploads/".$korisnik."*") as $filename) {
              $type = pathinfo($filename, PATHINFO_EXTENSION);
              $data = file_get_contents($filename);
              $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
              $sl= new Slike( $id_oglasa, $filename, "Lit". $id_oglasa.".".$type, $base64);
              $sl->dodajSlikuOglasa();

          }
          array_map('unlink', glob("uploads/".$korisnik."*"));
        	return response()->json(['Status'=>"Uspesno!"]);
   		}
   		else
   		{
   			return response()->json(['Status'=>"Prijavi se!"]);
   		}
    }

    public function predloziOglasaZaStanUVelikomOglasu(Request $request)
    {
        $json = $_POST;
        $oglas = new OglasiZaStan(null, null, null, null, "Oglas Za Stan", null, null, null, null, null, null,null, null, null, null , null , null, null , null, null , null , null , null);

        $rezultat = $oglas->vratiTriOglasaJedneVrste();
        $slanje = null;
        $i = 0;

        if(!is_null($rezultat[0]))
        {
          foreach($rezultat as $value)
          {
            $slanje[$i]['id_oglasa'] = intval($value['id_oglasa']);
            $slanje[$i]['naslov_oglasa'] = $value['naslov_oglasa'];
            $slanje[$i]['cena'] = intval($value['cena']);

            $o = $oglas->vratiOglasZaStanPremaIdu(intval($value['id_oglasa']));

            $slanje[$i]['povrsina'] = $o[0]['povrsina'];
            $i = $i + 1;
          }
        }
        
        return response()->json($slanje);
    }
}
