<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;  
use App\OglasiZaStan;
use App\OglasiZaLiteraturu;
use App\Oglasi;
use App\Slike;

class StanovanjeController extends Controller
{

	public function predloziOglasaZaStan(Request $request)
	{
		$json = $_POST;
    	$oglas = new Oglasi (null, null, null, null, null, null, null);
    	$oglas->vrsta_oglasa = "Oglas Za Stan";
    	$sviZaStan = $oglas->vratiTriOglasaJedneVrste();

    	$i=0;
		$slanje=null;

		foreach($sviZaStan as $value)
		{
			$slanje[$i]["naslov_oglasa"]=$value['naslov_oglasa'];
			$slanje[$i]["cena"]=$value['cena'];
            $slanje[$i]["opis"]=$value['opis_oglasa'];
            $slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
			$i=$i+1;
		}
		return response()->json($slanje);
	}

    public function proveriDaLiJeDodat($slanje, $grad)
    {
        if(!is_null($slanje[0]))
        {
            foreach($slanje as $value)
            {
                if($value['grad']===$grad)
                    return true;
            }
            return false;
        }
        return false;
    }

    public function selectZaGrad(Request $request)
    {
        $json = $_POST;
        $oglasi = new OglasiZaLiteraturu(null, null, null, null, null, null, null, null, null, null, null);

        $rezultat = $oglasi->izlistajSveUniverzitetskeGradove();

        $slanje = null;
        $i = 0;

        foreach($rezultat as $value)
        {
            if(!$this->proveriDaLiJeDodat($slanje, $value['grad']))
            {
                $slanje[$i]["grad"] = $value['grad'];
                $i = $i + 1;
            }
        }

        return response()->json($slanje);
    }


    public function listaOglasaZaStan(Request $request)
    {
    	$json=$_POST;
    	$oglas = new OglasiZaStan(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

    	$sviZaStan = $oglas->vratiSveMaleOglaseZaStan();

    	$i=0;
		$slanje=null;
		foreach($sviZaStan as $value)
		{
            $slanje[$i]["id_oglasa"] = intval($value['id_oglasa']);
			$slanje[$i]["naslov_oglasa"]=$value['naslov_oglasa'];
			$slanje[$i]["opis"]=$value['opis_oglasa'];
			$slanje[$i]["cena"]=$value['cena'];

			$posao = $oglas->vratiOglasZaStanPremaIdu(intval($value['id_oglasa']));

            $sl= new Slike( intval($value['id_oglasa']), null, null, null);
            $slike=$sl->vratiSveSlikeJednogOglasa();
            if(is_null($slike))
            {
                $slanje[$i]["slike"]=null;
            }
            else 
            {
                $slanje[$i]["slike"]=$slike[0];
            }

			$slanje[$i]['povrsina']=$posao[0]['povrsina'];
			$i=$i+1;
		}
		return response()->json($slanje);
    }

    public function filtriranjeOglasaZaStan(Request $request)
    {
    	$json = $_POST;

    	$oglas = new OglasiZaStan(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

    	$slanje = null;
    	$i = 0;

    	$rezultat = $oglas->filtrirajOglaseZaStan($json['kucaStan'], $json['cimer_stanar'], $json['grejanje'], $json['opremljenost'], $json['garaza'], $json['terasa'], $json['parking'], $json['lift'], $json['novogradnja'], $json['internet'], $json['kablovska'], $json['telefon'], $json['podrum'], intval($json['minimalniBrojSoba']), intval($json['maksimalniBrojSoba']), intval($json['minimalnaPovrsina']), intval($json['maksimalnaPovrsina']), intval($json['minimalniSprat']), intval($json['maksimalniSprat']), $json['grad'], intval($json['minimalna_cena']), intval($json['maksimalna_cena']));

        //return response()->json($rezultat);
        if(!is_null($rezultat[0]))
        {
            foreach($rezultat as $value)
            {
                $o = $oglas->vratiOglasZaStanPremaIdu(intval($value['id_oglasa']));
                $slanje[$i]['povrsina'] = $o[0]['povrsina'];

                $ogl = $oglas->vratiOglasNaOsnovuIda(intval($value['id_oglasa']));

                $slanje[$i]['id_oglasa'] = intval($ogl[0]['id_oglasa']);
                $slanje[$i]['naslov_oglasa'] = $ogl[0]['naslov_oglasa'];
                $slanje[$i]['opis'] = $ogl[0]['opis_oglasa'];
                $slanje[$i]['cena'] = intval($ogl[0]['cena']);
                
                $sl= new Slike( intval($value['id_oglasa']), null, null, null);
                $slike=$sl->vratiSveSlikeJednogOglasa();
                if(is_null($slike))
                {
                    $slanje[$i]["slike"]=null;
                }
                else 
                {
                    $slanje[$i]["slike"]=$slike[0];
                }

                $i = $i + 1;
            }
        }
    	return response()->json($slanje);
    }
}
