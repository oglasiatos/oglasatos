<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OglasiZaLiteraturu;
use App\Slike;

class mojiOglasiLiteraturaController extends Controller
{
    public function ajaxRequest(Request $request)
    {
      //$json = $_POST; 
      if($request->session()->has('oglasiatos_korisnik'))
      {   
        	$korisnik=$request->session()->get('oglasiatos_korisnik');
          	$o=new OglasiZaLiteraturu(null,null,null,null,null,null,null,null,null,null,null,null);
          	$oglasi=$o->vratiSveOglaseZaLiteraturuJednogKorisnika($korisnik);
          	$i=0;
          	$slanje=null;
          	foreach($oglasi as $value)
			{
				$literatura=$o->vratiOglasZaLiteraturuNaOsnovuId(intval($value['id_oglasa']));
				$slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
				$slanje[$i]["naziv_oglasa"]=$value['naslov_oglasa'];
				$slanje[$i]["izdavac_oglasa"]=$value['korisnicko_ime_izdavaca_oglasa'];
				//$slanje[$i]["datum_postavljanja"]=$value['datum_postavljanja_oglasa'];
				$slanje[$i]["opis"]=$value['opis_oglasa'];
				$slanje[$i]["cena"]=$value['cena'];
				$slanje[$i]["predmet"]=$literatura[0]['predmet'];
				

				$i=$i+1;
			}
          	return var_dump($slanje);
      }
      else
      {
      		return response()->json(['Status'=>"Prijavi se!"]);
      }
    }

     public function ajaxRequestPost(Request $request)
    {
    	$json = $_POST; 
    	  if($request->session()->has('oglasiatos_korisnik'))
      {   
        	$korisnik=$request->session()->get('oglasiatos_korisnik');
          	$o=new OglasiZaLiteraturu(null,null,null,null,null,null,null,null,null,null,null,null);
          	$oglasi=$o->vratiSveOglaseZaLiteraturuJednogKorisnika($korisnik);
          	$i=0;
          	$slanje=null;
          	foreach($oglasi as $value)
      			{
      				$literatura=$o->vratiOglasZaLiteraturuNaOsnovuId(intval($value['id_oglasa']));
      				$slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
      				$slanje[$i]["naziv_oglasa"]=$value['naslov_oglasa'];
      				$slanje[$i]["izdavac_oglasa"]=$value['korisnicko_ime_izdavaca_oglasa'];
      				//$slanje[$i]["datum_postavljanja"]=$value['datum_postavljanja_oglasa'];
      				$slanje[$i]["opis"]=$value['opis_oglasa'];
      				$slanje[$i]["cena"]=$value['cena'];
      				$slanje[$i]["predmet"]=$literatura[0]['predmet'];

              $sl= new Slike( intval($value['id_oglasa']), null, null, null);
              $slike=$sl->vratiSveSlikeJednogOglasa();
              if(is_null($slike))
              {
                 $slanje[$i]["slike"]=null;
              }
              else 
              {
                 $slanje[$i]["slike"]=$slike[0];
              }

      				$i=$i+1;
      			}
          	return response()->json($slanje);;
      }
      else
      {
      		return response()->json(['Status'=>"Prijavi se!"]);
      }
    }
}
