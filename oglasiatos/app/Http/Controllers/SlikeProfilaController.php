<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; // Required Dependencies
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;


class SlikeProfilaController extends Controller
{
    
    public function post_upload(){

        $destinationPath = public_path() . '/uploadsProfil/'; // upload folder, set whatever you like
        $image=Input::file('file');
        $fileNameWithExtension = Input::file('file')->getClientOriginalName();

        $image_resize = Image::make($image->getRealPath());              
    	$image_resize->resize(300, 300);
    	$upload_success=$image_resize->save(public_path('/uploadsProfil/' .$fileNameWithExtension));

        //$upload_success = Input::file('file')->move($destinationPath, $fileNameWithExtension); // uploading file to given path

        if ($upload_success) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }

	}
}
