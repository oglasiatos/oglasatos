<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Korisnik;
use App\ProfilneSlike;

class Profil extends Controller
{
    public function ajaxRequest(Request $request) {
      if($request->session()->has('oglasiatos_korisnik'))
      {
         $korisnik=$request->session()->get('oglasiatos_korisnik');
         $k=new Korisnik(null,null,null,null,null,null,null,null,null,null,null);
         $procitano=$k->vratiKorisnikaPremaKorisnickomImenu($korisnik);
         $k->ime=$procitano[0]['ime'];
  	     $k->prezime=$procitano[0]['prezime'];
  	     $k->datum_rodjenja=$procitano[0]['datum_rodjenja']->toDateTime()->format('Y-m-d');
  	     $k->adresa=$procitano[0]['adresa'];
  	     $k->grad=$procitano[0]['grad'];
  	     $k->email_adresa=$procitano[0]['email_adresa'];
  	     $k->korisnicko_ime=$procitano[0]['korisnicko_ime'];
  	     $k->lozinka=$procitano[0]['lozinka'];
  	     $k->datum_registrovanja=$procitano[0]['datum_registrovanja']->toDateTime()->format('Y-m-d');
  	     $k->kontakt_telefon=$procitano[0]['kontakt_telefon'];
  	     $k->tip_korisnika=$procitano[0]['tip_korisnika'];

         $sl= new ProfilneSlike( $korisnik, null, null, null);
         $slika= $sl->vratiProfilnuSliku();


	     //return var_dump($k['ime']);
         return view("profil",['korisnik' => $k,"slika"=>$slika]);
         //return var_dump($as[0]['datum_registrovanja']->toDateTime()->format('Y-m-d'));
      }
      else
      {
      	 return view('prijavljivanje');
      }

         //echo 'No data in the session';
    	/*public $ime;
	    public $prezime;
	    public $datum_rodjenja;
	    public $adresa;
	    public $grad;
	    public $email_adresa;
	    public $korisnicko_ime;
	    public $lozinka;
	    public $datum_registrovanja;
	    public $kontakt_telefon;
	    public $tip_korisnika;*/

    }

    public function brisanjeProfila(Request $request) {
      if($request->session()->has('oglasiatos_korisnik'))
      {
         $korisnik=$request->session()->get('oglasiatos_korisnik');
         $k=new Korisnik(null,null,null,null,null,null,null,null,null,null,null);
         $procitano=$k->vratiKorisnikaPremaKorisnickomImenu($korisnik);
         $k->ime=$procitano[0]['ime'];
       $k->prezime=$procitano[0]['prezime'];
       $k->datum_rodjenja=$procitano[0]['datum_rodjenja']->toDateTime()->format('Y-m-d');
       $k->adresa=$procitano[0]['adresa'];
       $k->grad=$procitano[0]['grad'];
       $k->email_adresa=$procitano[0]['email_adresa'];
       $k->korisnicko_ime=$procitano[0]['korisnicko_ime'];
       $k->lozinka=$procitano[0]['lozinka'];
       $k->datum_registrovanja=$procitano[0]['datum_registrovanja']->toDateTime()->format('Y-m-d');
       $k->kontakt_telefon=$procitano[0]['kontakt_telefon'];
       $k->tip_korisnika=$procitano[0]['tip_korisnika'];
       //return var_dump($k['ime']);
         return view("brisanjeProfila",['korisnik' => $k]);
         //return var_dump($as[0]['datum_registrovanja']->toDateTime()->format('Y-m-d'));
      }
      else
      {
         return view('prijavljivanje');
      }

         //echo 'No data in the session';
      /*public $ime;
      public $prezime;
      public $datum_rodjenja;
      public $adresa;
      public $grad;
      public $email_adresa;
      public $korisnicko_ime;
      public $lozinka;
      public $datum_registrovanja;
      public $kontakt_telefon;
      public $tip_korisnika;*/

    }

     public function izmeni(Request $request)
    {
    	///$ime, $prezime, $datum_rodjenja, $adresa, $grad, $email_adresa, $korisnicko_ime, $lozinka, $datum_registrovanja, $kontakt_telefon, $tip_korisnika
        $json = $_POST;
        $k=new Korisnik($json['ime'],$json['prezime'],$json['datum_rodjenja'],$json['adresa'],$json['grad'],$json['email'],$json['korisnicko'],$json['lozinka'],date('Y-m-d'),$json['broj'],$json['tipKorisnika']);
           
        $id=$k->izmenaKorisnika();

       foreach (glob("uploadsProfil/".$json['korisnicko']."*") as $filename) {
            $type = pathinfo($filename, PATHINFO_EXTENSION);
            $data = file_get_contents($filename);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                   ///$korisnicko_ime, $putanja, $naziv_slike, $sadrzaj_slike 
            $sl= new ProfilneSlike( $json["korisnicko"], $filename, $json["korisnicko"].".".$type, $base64);
            $sl->dodajProfilnuSliku();

        }
        array_map('unlink', glob("uploadsProfil/".$json['korisnicko']."*"));

        //$request->session()->put('oglasiatos_korisnik',$k->korisnicko_ime);
        return response()->json(['Status'=>$id]);

    }

      public function brisanje(Request $request)
    {
      ///$ime, $prezime, $datum_rodjenja, $adresa, $grad, $email_adresa, $korisnicko_ime, $lozinka, $datum_registrovanja, $kontakt_telefon, $tip_korisnika
        $korisnik=$request->session()->get('oglasiatos_korisnik');
        $json = $_POST;
        $k=new Korisnik(null,null,null,null,null,null,$korisnik,null,null,null,null);
           
        $id=$k->obrisiKorisnika();
        $request->session()->forget('oglasiatos_korisnik');

        //$request->session()->put('oglasiatos_korisnik',$k->korisnicko_ime);
        return response()->json(['Status'=>$id]);

    }


      public function ajaxRequestPost(Request $request)
    {
    	///$ime, $prezime, $datum_rodjenja, $adresa, $grad, $email_adresa, $korisnicko_ime, $lozinka, $datum_registrovanja, $kontakt_telefon, $tip_korisnika
        $json = $_POST; ///////EMAIL DA SE PREPRAVI U KORISNICKO IME/////
        $k=new Korisnik(null,null,null,null,null,null,$json['email'],$json['lozinka'],null,null,null);
           
        $id=$k->prijavljivanjeKorisnika();
        $request->session()->put('oglasiatos_korisnik',$k->korisnicko_ime);
        return response()->json(['Status'=>$id]);

    }
}
