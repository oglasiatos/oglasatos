<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; // Required Dependencies
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use App\OglasiZaLiteraturu;
use App\Slike;
use Intervention\Image\ImageManagerStatic as Image;

class SlikeController extends Controller
{
	public function test(Request $request)
	{
		$sl= new Slike( 11, null, null, null);
        $slika=$sl->vratiSveSlikeJednogOglasa();
        echo '<img src="'.$slika[1].'"/>';
		/*foreach (glob("uploads/jox1*") as $filename) {
		    echo "$filename size " . filesize($filename) . "<br>";
		}
		array_map('unlink', glob("uploads/jox1*"));*/
	}
    public function post_upload(){

        $destinationPath = public_path() . '/uploads/'; // upload folder, set whatever you like
        $image=Input::file('file');
        $fileNameWithExtension = Input::file('file')->getClientOriginalName();

        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(850, 570);
        $upload_success=$image_resize->save(public_path('/uploads/' .$fileNameWithExtension));

        //$upload_success = Input::file('file')->move($destinationPath, $fileNameWithExtension); // uploading file to given path

        if ($upload_success) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }

	}


}
