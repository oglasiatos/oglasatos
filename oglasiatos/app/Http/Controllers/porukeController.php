<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poruke;

class porukeController extends Controller
{
    //
    function test(Request $request)
    {
    	$poruke=new Poruke(null,null,null);
        if($request->session()->has('oglasiatos_korisnik'))
          {
            $korisnik=$request->session()->get('oglasiatos_korisnik');
            $neprocitane=$poruke->vratiSveKorisnikeKojiSuSlaliPorukeAktivnomKorisnikuANijeIhProcitao($korisnik);
            return var_dump($neprocitane[1][0]);
            $i=0;
            $slanje=null;
            if(!($neprocitane === null))
            {
              foreach($neprocitane as $value)
              {
                $slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_posiljaoca'];
                $slanje[$i]["procitana_poruka"]=$value[0]['procitana_poruka'];
                $i=$i+1;
              }
            }
            $neprocitane=$poruke->vratiSveKorisnikeKojiSuSlaliPorukeAktivnomKorisnikuAProcitaoIhJe($korisnik);
            if(!($neprocitane === null))
            {
              foreach($neprocitane as $value)
              {
                $slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_posiljaoca'];
                $slanje[$i]["procitana_poruka"]=$value[0]['procitana_poruka'];
                $i=$i+1;
              }
            }
            return var_dump($slanje);
          }
	    
    }
    

    function brisanjePoruke(Request $request){
      $json = $_POST;
      $poruke=new Poruke(null,null,null);
        $slanje="Prijavi korisnika!";
        if($request->session()->has('oglasiatos_korisnik'))
        {
            $korisnik=$request->session()->get('oglasiatos_korisnik');
            $neprocitane=$poruke->obrisiPoruku($json['id_poruke']);
            
            return response()->json(["Status"=>"Procitano!"]);

        }
        return response()->json(["Status"=>"Prijavi se!"]);

    }


    function procitajporuke(Request $request)
    {

        $json = $_POST;
        $poruke=new Poruke(null,null,null);
        $slanje="Prijavi korisnika!";
        if($request->session()->has('oglasiatos_korisnik'))
        {
            $korisnik=$request->session()->get('oglasiatos_korisnik');
            $poruke->korisnicko_ime_primaoca=$korisnik;
            $poruke->korisnicko_ime_posiljaoca=$json['kime'];
            $neprocitane=$poruke->oznaciDaSuPorukeProcitane();
            
            return response()->json(["Status"=>"Procitano!"]);

        }
        return response()->json(["Status"=>"Prijavi se!"]);
    }


    function vratisveporuke(Request $request)
    {
    		$poruke=new Poruke(null,null,null);
    		if($request->session()->has('oglasiatos_korisnik'))
      		{
		        $korisnik=$request->session()->get('oglasiatos_korisnik');
      			$neprocitane=$poruke->vratiSveKorisnikeKojiSuSlaliPorukeAktivnomKorisnikuANijeIhProcitao($korisnik);
      			$i=0;
          	$slanje=null;
            if(!($neprocitane === null))
            {
            	foreach($neprocitane as $value)
        			{
                if(isset($value[0]['korisnicko_ime_posiljaoca']))
                {
                  $slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_posiljaoca'];
                }
                else {
                  $slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_primaoca'];
                }
        				
        				$slanje[$i]["procitana_poruka"]=$value[0]['procitana_poruka'];
        				$i=$i+1;
        			}
            }
            $neprocitane=$poruke->vratiSveKorisnikeKojiSuSlaliPorukeAktivnomKorisnikuAProcitaoIhJe($korisnik);
            if(!($neprocitane === null))
            {
              foreach($neprocitane as $value)
              {

                if(isset($value[0]['korisnicko_ime_posiljaoca']))
                {
                  $slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_posiljaoca'];
                }
                else {
                  $slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_primaoca'];
                }

                //$slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_posiljaoca'];
                $slanje[$i]["procitana_poruka"]=$value[0]['procitana_poruka'];
                $i=$i+1;
              }
            }
          	return response()->json($slanje);
	    	}
    }

    function ucitajkonverzaciju(Request $request)
    {
    	$json = $_POST;
    	$poruke=new Poruke(null,null,null);
    	$slanje="Prijavi korisnika!";
    	if($request->session()->has('oglasiatos_korisnik'))
      	{
  			$korisnik=$request->session()->get('oglasiatos_korisnik');
			  $neprocitane=$poruke->vratiCeluKonverzacijuDvaKorisnika($korisnik, $json["drugikorisnik"]);
    		$i=0;
    		$slanje=null;
        if(!($neprocitane === null))
    		{
          foreach($neprocitane as $value)
  			{
  				$slanje[$i]["korisnicko_ime_posiljaoca"]=$value[0]['korisnicko_ime_posiljaoca'];
  				$slanje[$i]["procitana_poruka"]=$value[0]['procitana_poruka'];
  				$slanje[$i]["sadrzaj_poruke"]=$value[0]['sadrzaj_poruke'];
  				$slanje[$i]["id_poruke"]=intval($value[0]["id_poruke"]);

  				$i=$i+1;
  			}
      }
			return response()->json($slanje);


      	}
    	return response()->json(["Status"=>$slanje]);
    }

    function posaljiporuku(Request $request)
    {
    	$json = $_POST;
    	$poruke=new Poruke(null,null,null);
    	$slanje="Prijavi korisnika!";
    	if($request->session()->has('oglasiatos_korisnik'))
      	{
      		$korisnik=$request->session()->get('oglasiatos_korisnik');
      		$slanje=$poruke->posaljiPoruku($json["poruka"], $korisnik, $json["primalac"]);

      	}
    	return response()->json(["Status"=>$slanje]);

    }
}
