<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Oglasi;
use App\Http\Controllers\OglasStan;
class AdminController extends Controller
{
    public function listaOglasa(Request $request)
	{
		$json=$_POST;
		$admin=new Admin();
		$oglasi=$admin->vratiSveOglase();
		$i=0;
		$slanje=null;
		foreach($oglasi as $value)
		{
			$slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
			$slanje[$i]["naziv_oglasa"]=$value['naslov_oglasa'];
			$slanje[$i]["izdavac_oglasa"]=$value['korisnicko_ime_izdavaca_oglasa'];
			//$slanje[$i]["datum_postavljanja"]=$value['datum_postavljanja_oglasa'];
			$slanje[$i]["opis"]=$value['opis_oglasa'];
			$slanje[$i]["cena"]=$value['cena'];
			$slanje[$i]["vrsta_oglasa"]=$value["vrsta_oglasa"];
			$i=$i+1;
		}
		return response()->json($slanje);
	}

	public function brisanjeOglasa(Request $request)
	{
		$json = $_POST;
		$admin=new Admin();
		return response()->json($admin->brisanjeOglasa($json['id']));
	}

	public function blokiranjeKorisnika(Request $request)
	{
		$json = $_POST;
		$admin=new Admin();
		$id=$json['id'];
		$korisnickoIme = $admin->vratiIzdavacaNaOsnovuIdaOglasa($id);
		return response()->json($admin->blokirajKorisnika($korisnickoIme));
	}

	public function pretraziKorisnike(Request $request)
	{
		$json = $_POST;
		$admin=new Admin();
		$deoKorisnickogImena=$json['deoKorisnickogImena'];
		$oglasi=$admin->vratiSveIzdavaceOglasaPremaKljucnojReci($deoKorisnickogImena);
		$i=0;
		$slanje=null;
		foreach($oglasi as $value)
		{
			$slanje[$i]["id_oglasa"]=intval($value[0]['id_oglasa']);
			$slanje[$i]["naziv_oglasa"]=$value[0]['naslov_oglasa'];
			$slanje[$i]["izdavac_oglasa"]=$value[0]['korisnicko_ime_izdavaca_oglasa'];
			//$slanje[$i]["datum_postavljanja"]=$value['datum_postavljanja_oglasa'];
			$slanje[$i]["opis"]=$value[0]['opis_oglasa'];
			$slanje[$i]["cena"]=$value[0]['cena'];
			$i=$i+1;
		}
		return response()->json($slanje);
	}

	
}
