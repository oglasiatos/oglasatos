<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PrijaveNaOglase;
use App\Komentari;
use App\Oglasi;
use App\Korisnik;

class prijavaikomentar extends Controller
{
    public function slanjeprijave(Request $request)
    {
    	$json=$_POST;
    	if($request->session()->has('oglasiatos_korisnik'))
        {   
        	$korisnik=$request->session()->get('oglasiatos_korisnik');
        	$pr=new PrijaveNaOglase(intval($json['id_oglasa']), $korisnik);
        	$t=$pr->prijaviSeNaOglas();
        	return response()->json($t);
        }
    	return response()->json(["Status"=>"Nije ulogovan!!"]);
    }


    public function obrisikom(Request $request)
    {
        $json=$_POST;
        if($request->session()->has("oglasiatos_korisnik"))
        {
            $korisnik=$request->session()->get('oglasiatos_korisnik');
            $kom=new Komentari( $korisnik,null, null);
            $kom->id_komentara=intval($json["id_komentara"]);
            $t=$kom->obrisiKomentar();
            return response()->json(["Status"=>"Uspesno!!!"]);
        }
        return response()->json(["Status"=>"Nije ulogovan!!"]);
    }
    
    public function ocenjivanje(Request $request)
    {
        $json=$_POST;
        if($request->session()->has("oglasiatos_korisnik"))
        {
            $korisnik=$request->session()->get('oglasiatos_korisnik');
            $k=new Korisnik(null, null, null, null, null, null, $korisnik, null, null, null, null);

            $k->oceniKorisnika(intval($json["ocena"]), $json["korisnik"]);
            return response()->json(["Status"=>"Uspesno!!"]);
        }
        return response()->json(["Status"=>"Nije ulogovan!!"]);
    }
    public function komentari(Request $request)
    {
    	$json=$_POST;
    	if($request->session()->has("oglasiatos_korisnik"))
    	{
    		$korisnik=$request->session()->get('oglasiatos_korisnik');
    		$kom=new Komentari( $korisnik,intval($json['id_oglasa']), null);
    		$t=$kom->vratiSveKomentareJednogOglasa();
    		$slanje=null;
    		$i=0;
    		foreach ($t as  $value) {
    			$slanje[$i]["sadrzaj"]=$value['sadrzaj_komentara'];
    			$slanje[$i]["korisnik"]=$value['korisnicko_ime_korisnika_koji_komentarise'];
                $slanje[$i]["licni"]=$korisnik;
                $slanje[$i]['id_komentara']=intval($value['id_komentara']);
                $i=$i+1;
    		}
    		return response()->json($slanje);
        }
    	return response()->json(["Status"=>"Nije ulogovan!!"]);
    }

    public function komentarisi(Request $request)
    {
    	$json=$_POST;
    	if($request->session()->has("oglasiatos_korisnik"))
    	{
    		$korisnik=$request->session()->get('oglasiatos_korisnik');
    		$kom=new Komentari( $korisnik,intval($json['id_oglasa']), $json['sadrzaj']);
    		$t=$kom->dodajKomentar($json['sadrzaj']);
    		return response()->json($t);
        }
    	return response()->json(["Status"=>"Nije ulogovan!!"]);
    }
}
