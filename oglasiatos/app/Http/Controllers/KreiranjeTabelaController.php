<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use DB;

class KreiranjeTabelaController extends Controller
{
	public function kreiranjeTabeleKorisnik()
	{
		DB::raw('CREATE TABLE korisnik(
			ime text,
			prezime text,
			datum_rodjenja date,
			adresa text,
			grad text,
			email_adresa text,
			korisnicko_ime text,
			lozinka text,
			datum_registrovanja date,
			kontakt_telefon text,
			tip_korisnika text,
			PRIMARY KEY(korisnicko_ime, email_adresa, tip_korisnika)
		)');
	}

	public function kreiranjeTabeleBlokiraniKorisnici()
	{
		DB::raw ('CREATE TABLE blokirani_korisnici (
			email_adresa text,
			datum_blokiranja date,
			PRIMARY KEY (email_adresa)
		)');
	}

	public function kreiranjeTabeleFakultet()
	{

		DB::raw ('CREATE TABLE fakultet(
			naziv_fakulteta text,
			grad text,
			PRIMARY KEY(grad, naziv_fakulteta)
		)');

		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Ekonomski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Gradjevinsko-Arhitektonski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Elektronski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Fakultet Umetnosti']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Fakultet Zastite Na Radu']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Fakultet Sporta I Fizickog Vaspitanja']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Filozofski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Masinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Medicinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Pravni Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Nis','naziv_fakulteta' => 'Prirodno-Matematicki Fakultet']);

		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Za Fizicku Hemiju']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Matematicki Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Ekonomski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Arhitektonski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Za Specijalnu Edukaciju I Rehabilitaciju']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Muzicke Umetnosti']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fizicki Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Sporta I Fizickog Vaspitanja']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Filoloski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Gradjevinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Medicinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Saobracajni Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Bioloski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Veterinarske Medicine']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Organizacionih Nauka']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Filozofski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Hemijski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Pravni Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Stomatoloski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Uciteljski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Vojna Akademija']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Dramskih Umetnosti']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Politickih Nauka']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Masinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Kriminalisticko Policijska Akademija']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Pravoslavni Bogoslovski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Poljoprivredni Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Sumarski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Vojnomedicinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Elektrotehnicki Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Likovnih Umetnosti']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Primenjenih Umetnosti']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Fakultet Bezbednosti']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Farmaceutski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Geografski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Rudarsko-Geoloski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Beograd','naziv_fakulteta' => 'Tehnolosko-Metalurski Fakultet']);

		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Tehnoloski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Prirodno-Matematicki Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Akademija Umetnosti']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Fakultet Sporta I Fizickog Vaspitanja']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Fakultet Tehnickih Nauka']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Filozofski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Ekonomski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Medicinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Poljoprivredni Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Novi Sad','naziv_fakulteta' => 'Pravni Fakultet']);

		DB::table('fakultet')->insert(['grad' => 'Kragujevac','naziv_fakulteta' => 'Pravni Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Kragujevac','naziv_fakulteta' => 'Ekonomski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Kragujevac','naziv_fakulteta' => 'Fakultet Inzenjerskih Nauka']);
		DB::table('fakultet')->insert(['grad' => 'Kragujevac','naziv_fakulteta' => 'Fakultet Medicinskih Nauka']);
		DB::table('fakultet')->insert(['grad' => 'Kragujevac','naziv_fakulteta' => 'Filolosko-Umetnicki Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Kragujevac','naziv_fakulteta' => 'Prirodno-Matematicki-Fakultet']);

		DB::table('fakultet')->insert(['grad' => 'Kosovska Mitrovica','naziv_fakulteta' => 'Fakultet Tehnickih Nauka']);
		DB::table('fakultet')->insert(['grad' => 'Kosovska Mitrovica','naziv_fakulteta' => 'Ekonomski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Kosovska Mitrovica','naziv_fakulteta' => 'Pravni Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Kosovska Mitrovica','naziv_fakulteta' => 'Medicinski Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Kosovska Mitrovica','naziv_fakulteta' => 'Prirodno-Matematicki Fakultet']);
		DB::table('fakultet')->insert(['grad' => 'Kosovska Mitrovica','naziv_fakulteta' => 'Filozofski Fakultet']);
	}

	public function kreiranjeTabeleKomentari()
	{
		DB::raw('CREATE TABLE komentari(
			id_komentara bigint,
			id_oglasa bigint,
			korisnicko_ime_korisnika_koji_komentarise text,
			sadrzaj_komentara text,
			PRIMARY KEY(id_oglasa, korisnicko_ime_korisnika_koji_komentarise, id_komentara))
		');
	}

	public function kreiranjeTabeleDodatniKomentari()
	{
		DB::raw('CREATE TABLE dodatni_komentari(
			id_komentara bigint,
			id_oglasa bigint,
			korisnicko_ime_korisnika_koji_komentarise text,
			sadrzaj_komentara text,
			PRIMARY KEY(id_komentara)
		)');
	}

	public function kreiranjeTabeleOcene()
	{
		DB::raw('CREATE TABLE ocene (
			id_ocene bigint, 
			korisnicko_ime_ocenjenog_korisnika text,
			korisnicko_ime_korisnika_koji_ocenjuje text,
			ocena int,
			PRIMARY KEY (korisnicko_ime_ocenjenog_korisnika, korisnicko_ime_korisnika_koji_ocenjuje, id_ocene)
		)');
	}

	public function kreiranjeTabeleDodatneOcene()
	{
		DB::raw('CREATE TABLE dodatne_ocene (
			id_ocene bigint, 
			korisnicko_ime_ocenjenog_korisnika text,
			korisnicko_ime_korisnika_koji_ocenjuje text,
			ocena int,
			PRIMARY KEY (id_ocene)
		)');
	}

	public function kreiranjeTabeleOglasi()
	{
		DB::raw('CREATE TABLE oglasi(
			id_oglasa bigint, 
			korisnicko_ime_izdavaca_oglasa text,
			naslov_oglasa text,
			grad text,
			opis_oglasa text,
			vrsta_oglasa text,
			cena int,
			PRIMARY KEY(vrsta_oglasa, korisnicko_ime_izdavaca_oglasa, grad, cena, id_oglasa))
			WITH CLUSTERING ORDER BY (korisnicko_ime_izdavaca_oglasa ASC, grad ASC, cena ASC, id_oglasa DESC)
		');
		
	}

	public function kreiranjeTabeleDodatniOglasi()
	{
		DB::raw('CREATE TABLE dodatni_oglasi (
			id_oglasa bigint,
			korisnicko_ime_izdavaca_oglasa text,
			naslov_oglasa text,
			grad text,
			opis_oglasa text, 
			vrsta_oglasa text,
			cena int,
			PRIMARY KEY(id_oglasa) )');
	}



	public function kreiranjeTabeleOglasiZaLiteraturu()
	{
		DB::raw('CREATE TABLE oglasi_za_literaturu(
			id_oglasa_za_literaturu bigint, 
			fakultet text,
			predmet text,
			naziv_literature text,
			kupujem_prodajem text,
			izdavac text,
			PRIMARY KEY(kupujem_prodajem, fakultet, naziv_literature, id_oglasa_za_literaturu)
		)');
	}

	public function kreiranjeTabeleDodatniOglasiZaLiteraturu()
	{
		DB::raw('CREATE TABLE dodatni_oglasi_za_literaturu(
			id_oglasa_za_literaturu bigint,
			fakultet text,
			predmet text,
			naziv_literature text,
			kupujem_prodajem text,
			izdavac text,
			PRIMARY KEY(id_oglasa_za_literaturu)
		)');
	}

	public function kreiranjeTabeleOglasiZaStan()
	{
		DB::raw('CREATE TABLE oglasi_za_stan(
			id_oglasa_za_stan bigint, 
			adresa_stana text,
			cimer_stanar text,
			broj_soba int,
			povrsina int,
			tip_grejanja text,
			opremljenost text,
			kuca_stan text,
			sprat int,
			garaza boolean, 
			terasa boolean,
			parking boolean,
			lift boolean,
			novogradnja boolean,
			internet boolean,
			kablovska boolean,
			telefon boolean,
			podrum boolean,
			PRIMARY KEY (kuca_stan, cimer_stanar, tip_grejanja, opremljenost, garaza, terasa, parking, lift, novogradnja, internet, kablovska, telefon, podrum, broj_soba, povrsina, sprat, id_oglasa_za_stan)
		)');
	}

	public function kreiranjeTabeleDodatniOglasiZaStan()
	{
		DB::raw('CREATE TABLE dodatni_oglasi_za_stan(
			id_oglasa_za_stan bigint, 
			adresa_stana text,
			cimer_stanar text,
			broj_soba int,
			povrsina int,
			tip_grejanja text,
			opremljenost text,
			kuca_stan text,
			sprat int,
			garaza boolean, 
			terasa boolean,
			parking boolean,
			lift boolean,
			novogradnja boolean,
			internet boolean,
			kablovska boolean,
			telefon boolean,
			podrum boolean,
			PRIMARY KEY (id_oglasa_za_stan)
		)');
	}

	public function kreiranjeTabelePoruke()
	{
		DB::raw('CREATE TABLE poruke(
			id_poruke bigint,
			korisnicko_ime_posiljaoca text,
			korisnicko_ime_primaoca text,
			sadrzaj_poruke text,
			procitana_poruka boolean,
			PRIMARY KEY(korisnicko_ime_primaoca, korisnicko_ime_posiljaoca, procitana_poruka, id_poruke)
		)');
	}

	public function kreiranjeTabeleDodatnePoruke()
	{
		DB::raw('CREATE TABLE dodatne_poruke(
			id_poruke bigint,
			korisnicko_ime_posiljaoca text,
			korisnicko_ime_primaoca text,
			sadrzaj_poruke text,
			procitana_poruka boolean,
			PRIMARY KEY(id_poruke)
		)');
	}

	public function kreiranjeTabelePrijaveNaOglase()
	{
		DB::raw('CREATE TABLE prijave_na_oglase(
			id_prijave_na_oglas bigint,
			id_oglasa bigint,
			korisnicko_ime_prijavljenog_korisnika text,
			PRIMARY KEY (id_oglasa, korisnicko_ime_prijavljenog_korisnika, id_prijave_na_oglas)
		)');
	}

	public function kreiranjeTabeleDodatnePrijaveNaOglase()
	{
		DB::raw('CREATE TABLE dodatne_prijave_na_oglase(
			id_prijave_na_oglas bigint,
			id_oglasa bigint,
			korisnicko_ime_prijavljenog_korisnika text,
			PRIMARY KEY (id_prijave_na_oglas)
		)');
	}


	public function kreiranjeTabeleOglasiZaPosao()
	{
		DB::raw('CREATE TABLE oglasi_za_posao(
			id_oglasa_za_posao bigint, 
			naziv_posla text,
			potrebno_radno_iskustvo boolean,
			lokacija_radnog_mesta text,
			radno_vreme text,
			tip_posla text,
			PRIMARY KEY (tip_posla, potrebno_radno_iskustvo, naziv_posla, id_oglasa_za_posao)
		)');
	}

	public function kreiranjeTabeleDodatniOglasiZaPosao()
	{
		DB::raw('CREATE TABLE dodatni_oglasi_za_posao(
			id_oglasa_za_posao bigint, 
			naziv_posla text,
			potrebno_radno_iskustvo boolean,
			lokacija_radnog_mesta text,
			radno_vreme text,
			tip_posla text,
			PRIMARY KEY (id_oglasa_za_posao)
		)');
	}

	public function kreiranjeTabeleZaSlike()
	{
		DB::raw('CREATE TABLE slike(
			id_slike bigint,
			id_oglasa bigint,
			putanja text, 
			naziv_slike text,
			sadrzaj_slike blob,
			PRIMARY KEY(id_oglasa,id_slike)
		)');
	}

	public function kreiranjeTabeleProfilneSlike()
	{
		DB::raw('CREATE TABLE profilne_slike(
			korisnicko_ime text,
			putanja text,
			naziv_slike text,
			sadrzaj_slike blob,
			PRIMARY KEY (korisnicko_ime)
		)');
	}

	public function kreiranjeTabeleIndeksi()
	{
		DB::raw('CREATE TABLE indeksi(
			ime_tabele text,
			indeks int,
			PRIMARY KEY(ime_tabele)
		)');
		DB::table('indeksi')->insert(['ime_tabele' => 'slike', 'indeks' => 0]);
		DB::table('indeksi')->insert(['ime_tabele' => 'prijave_na_oglase', 'indeks' => 0]);
		DB::table('indeksi')->insert(['ime_tabele' => 'poruke', 'indeks' => 0]);
		DB::table('indeksi')->insert(['ime_tabele' => 'oglasi', 'indeks' => 0]);
		DB::table('indeksi')->insert(['ime_tabele' => 'komentari', 'indeks' => 0]);
	}

	public function kreiranjeIndeksaZaTabele()
	{
		$string = "CREATE CUSTOM INDEX  fn_prefix1 ON dodatni_oglasi (korisnicko_ime_izdavaca_oglasa)
					USING 'org.apache.cassandra.index.sasi.SASIIndex';";
		DB::raw($string);

		$string = "CREATE CUSTOM INDEX fn_contains1 ON dodatni_oglasi_za_literaturu (naziv_literature) USING 'org.apache.cassandra.index.sasi.SASIIndex'
				WITH OPTIONS = { 'mode': 'CONTAINS' };";

		DB::raw($string);

		$string = "CREATE CUSTOM INDEX fn_contains2 ON dodatni_oglasi_za_posao (naziv_posla) USING 'org.apache.cassandra.index.sasi.SASIIndex'
					WITH OPTIONS = { 'mode': 'CONTAINS' };";

		DB::raw($string);
	}

    public function kreiranjeTabela()
    {

		  	
    	$this->kreiranjeTabeleKorisnik();
    	$this->kreiranjeTabeleBlokiraniKorisnici();
    	$this->kreiranjeTabeleFakultet();
    	$this->kreiranjeTabeleOglasi();
    	$this->kreiranjeTabeleDodatniOglasi();
    	$this->kreiranjeTabeleOglasiZaPosao();
    	$this->kreiranjeTabeleDodatniOglasiZaPosao();
    	$this->kreiranjeTabeleOglasiZaStan();
    	$this->kreiranjeTabeleDodatniOglasiZaStan();
    	$this->kreiranjeTabeleOglasiZaLiteraturu();
    	$this->kreiranjeTabeleDodatniOglasiZaLiteraturu();
    	$this->kreiranjeTabeleOcene();
    	$this->kreiranjeTabeleDodatneOcene();
    	$this->kreiranjeTabeleKomentari();
    	$this->kreiranjeTabeleDodatniKomentari();
    	$this->kreiranjeTabelePoruke();
    	$this->kreiranjeTabeleDodatnePoruke();
    	$this->kreiranjeTabelePrijaveNaOglase();
    	$this->kreiranjeTabeleDodatnePrijaveNaOglase();
    	$this->kreiranjeTabeleZaSlike();
    	$this->kreiranjeTabeleProfilneSlike();
    	$this->kreiranjeTabeleIndeksi();
    	$this->kreiranjeIndeksaZaTabele();
  			

  		DB::table("korisnik")->insert(['ime'=> "",
  				'prezime'=> "",
  				'datum_rodjenja' => (new \DateTime())->format('Y-m-d'),
  				'adresa' => "",
  				'grad' => "",
  				"email_adresa" => "admin123@gmail.com",
  				"korisnicko_ime" => "admin123",
  				'lozinka'=>"jaSamAdmin123",
  				"datum_registrovanja"=>(new \DateTime())->format('Y-m-d'),
  				'kontakt_telefon'=>"",
  				'tip_korisnika'=>"Admin"]);
  		
    	return "Uspesno kreirana baza!";
    }

}
