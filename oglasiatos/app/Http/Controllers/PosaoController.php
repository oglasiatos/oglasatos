<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OglasiZaPosao;
use App\Oglasi;
use App\Slike;

class PosaoController extends Controller
{
    public function listaOglasaZaPosao(Request $request)
    {
    	$json = $_POST;
    	$oglas = new OglasiZaPosao (null, null, null, null, null, null, null, null, null, null, null, null);
    	$sviZaStan = $oglas->vratiSveMaleOglaseZaPosao();

    	$i=0;
		$slanje=null;
		if(!is_null($sviZaStan[0]))
        {
            foreach($sviZaStan as $value)
            {
                $slanje[$i]['id_oglasa']=intval($value['id_oglasa']);
                $slanje[$i]["naslov_oglasa"]=$value['naslov_oglasa'];
                $slanje[$i]["opis"]=$value['opis_oglasa'];
                $slanje[$i]["cena"]=$value['cena'];


                $posao = $oglas->vratiOglasZaPosaoPremaIdu(intval($value['id_oglasa']));

                $sl= new Slike( intval($value['id_oglasa']), null, null, null);
                $slike=$sl->vratiSveSlikeJednogOglasa();
                if(is_null($slike))
                {
                    $slanje[$i]["slike"]=null;
                }
                else 
                {
                    $slanje[$i]["slike"]=$slike[0];
                }


                $slanje[$i]['naziv_posla']=$posao[0]['naziv_posla'];
                $i=$i+1;
            }
        }
		return response()->json($slanje);
    }

    public function predloziZaPosao(Request $request)
    {
    	$json = $_POST;
    	$oglas = new Oglasi (null, null, null, null, null, null, null);
    	$oglas->vrsta_oglasa = "Oglas Za Posao";
    	$sviZaStan = $oglas->vratiTriOglasaJedneVrste();

    	$i=0;
		$slanje=null;

		foreach($sviZaStan as $value)
		{
			$slanje[$i]["naslov_oglasa"]=$value['naslov_oglasa'];
			$slanje[$i]["cena"]=$value['cena'];
            $slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
            $slanje[$i]["opis"]=$value['opis_oglasa'];
			$i=$i+1;
		}
		return response()->json($slanje);
    }

    public function filtiranjeOglasaZaPosao(Request $request)
    {
    	$json = $_POST;
    	$oglas = new OglasiZaPosao(null, null, null, null, null, null, null, null, null, null, null, null);
    	$slanje = null;
    	$i = 0;

    	$rezultat = $oglas->filtriranjeOglasaZaPosao($json['tip_posla'], $json['radno_iskustvo'], $json['naziv_posla'], $json['grad'], intval($json['minimalna_cena']), intval($json['maksimalna_cena']));

        if(!is_null($rezultat))
        {
            foreach($rezultat as $value)
            {
                $o = $oglas->vratiOglasZaPosaoPremaIdu(intval($value['id_oglasa']));
                $slanje[$i]['naziv_posla'] = $o[0]['naziv_posla'];

                $ogl = $oglas->vratiOglasNaOsnovuIda(intval($value['id_oglasa']));

                $slanje[$i]['id_oglasa'] = intval($ogl[0]['id_oglasa']);
                $slanje[$i]['naslov_oglasa'] = $ogl[0]['naslov_oglasa'];
                $slanje[$i]['opis'] = $ogl[0]['opis_oglasa'];
                $slanje[$i]['cena'] = intval($ogl[0]['cena']);

                $sl= new Slike( intval($value['id_oglasa']), null, null, null);
                $slike=$sl->vratiSveSlikeJednogOglasa();
                if(is_null($slike))
                {
                    $slanje[$i]["slike"]=null;
                }
                else 
                {
                    $slanje[$i]["slike"]=$slike[0];
                }

                $i = $i + 1;
            }
        }


    	return response()->json($slanje);
    }
}
