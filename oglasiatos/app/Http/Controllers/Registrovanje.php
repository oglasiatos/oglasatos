<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Korisnik;


class Registrovanje extends Controller
{

	 public function ajaxRequest()

    {
    	/*public $ime;
    public $prezime;
    public $datum_rodjenja;
    public $adresa;
    public $grad;
    public $email_adresa;
    public $korisnicko_ime;
    public $lozinka;
    public $datum_registrovanja;
    public $kontakt_telefon;
    public $tip_korisnika;*/

    }

      public function ajaxRequestPost(Request $request)
    {
    	///$ime, $prezime, $datum_rodjenja, $adresa, $grad, $email_adresa, $korisnicko_ime, $lozinka, $datum_registrovanja, $kontakt_telefon, $tip_korisnika
        $json = $_POST;
        $k=new Korisnik($json['ime'],$json['prezime'],$json['datumRodjenja'],$json['adresa'],$json['grad'],$json['email'],$json['korisnickoIme'],$json['lozinka'],date('Y-m-d'),$json['broj'],$json['tip']);
           
        $id=$k->registracijaKorisnika();
        $request->session()->put('oglasiatos_korisnik',$k->korisnicko_ime);
        return response()->json(['Status'=>$id]);

    }
}
