<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OglasiZaStan;
use App\Slike;

class mojiOglasiStan extends Controller
{
    //
    public function ajaxRequest(Request $request)
    {
      //$json = $_POST; 
      if($request->session()->has('oglasiatos_korisnik'))
      {   
        	$korisnik=$request->session()->get('oglasiatos_korisnik');
          	$o=new OglasiZaStan(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
          	$oglasi=$o->vratiSveOglaseZaStanJednogKorisnika($korisnik);
          	$i=0;
          	$slanje=null;
          	foreach($oglasi as $value)
			{
				$stan=$o->vratiOglasZaStanNaOsnovuIda(intval($value['id_oglasa']));
				$slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
				$slanje[$i]["naziv_oglasa"]=$value['naslov_oglasa'];
				$slanje[$i]["izdavac_oglasa"]=$value['korisnicko_ime_izdavaca_oglasa'];
				//$slanje[$i]["datum_postavljanja"]=$value['datum_postavljanja_oglasa'];
				$slanje[$i]["opis"]=$value['opis_oglasa'];
				$slanje[$i]["cena"]=$value['cena'];
				$slanje[$i]["brsoba"]=$stan[0]['broj_soba'];
				$slanje[$i]["povrsina"]=$stan[0]['povrsina'];
				$slanje[$i]["garaza"]=$stan[0]['garaza'];

				$i=$i+1;
			}
          	return var_dump($slanje);
      }
      else
      {
      		return response()->json(['Status'=>"Prijavi se!"]);
      }
    }

     public function ajaxRequestPost(Request $request)
    {
    	$json = $_POST; 
    	  if($request->session()->has('oglasiatos_korisnik'))
      {   
        	$korisnik=$request->session()->get('oglasiatos_korisnik');
          	$o=new OglasiZaStan(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
          	$oglasi=$o->vratiSveOglaseZaStanJednogKorisnika($korisnik);
          	$i=0;
          	$slanje=null;
          	foreach($oglasi as $value)
      			{
      				$stan=$o->vratiOglasZaStanNaOsnovuIda(intval($value['id_oglasa']));
      				$slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
      				$slanje[$i]["naziv_oglasa"]=$value['naslov_oglasa'];
      				$slanje[$i]["izdavac_oglasa"]=$value['korisnicko_ime_izdavaca_oglasa'];
      				//$slanje[$i]["datum_postavljanja"]=$value['datum_postavljanja_oglasa'];
      				$slanje[$i]["opis"]=$value['opis_oglasa'];
      				$slanje[$i]["cena"]=$value['cena'];
      				$slanje[$i]["brsoba"]=$stan[0]['broj_soba'];
      				$slanje[$i]["povrsina"]=$stan[0]['povrsina'];
              $slanje[$i]["garaza"]=$stan[0]['garaza'];
      				//$slanje[$i]["povrsina"]=$stan[0]['broj_soba'];

              $sl= new Slike( intval($value['id_oglasa']), null, null, null);
              $slike=$sl->vratiSveSlikeJednogOglasa();
              if(is_null($slike))
              {
                 $slanje[$i]["slike"]=null;
              }
              else 
              {
                 $slanje[$i]["slike"]=$slike[0];
              }
      				$i=$i+1;
              
      			}
          	return response()->json($slanje);;
      }
      else
      {
      		return response()->json(['Status'=>"Prijavi se!"]);
      }
    }
}
