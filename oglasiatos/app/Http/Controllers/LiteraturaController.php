<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OglasiZaLiteraturu;
use App\Oglasi;
use App\Slike;

class LiteraturaController extends Controller
{

	public function listaPredlogaZaLiteraturu(Request $request)
	{
		$json = $_POST;
    	$oglas = new Oglasi (null, null, null, null, null, null, null);
    	$oglas->vrsta_oglasa = "Oglas Za Literaturu";
    	$sviZaStan = $oglas->vratiTriOglasaJedneVrste();

    	$i=0;
		$slanje=null;

		foreach($sviZaStan as $value)
		{
			$slanje[$i]["naslov_oglasa"]=$value['naslov_oglasa'];
			$slanje[$i]["cena"]=$value['cena'];
            $slanje[$i]["opis"]=$value['opis_oglasa'];
            $slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
			$i=$i+1;
		}
		return response()->json($slanje);
	}


    public function listaOglasaZaLiteraturu(Request $request)
    {
    	$json = $_POST;
	    $oglas = new OglasiZaLiteraturu(null, null, null, null, null, null, null, null, null, null, null, null);

	    $sviZaStan = $oglas->vratiSveMaleOglaseZaLiteraturu();

	    	$i=0;
			$slanje=null;
			foreach($sviZaStan as $value)
			{
                $slanje[$i]["id_oglasa"]=intval($value['id_oglasa']);
				$slanje[$i]["naslov_oglasa"]=$value['naslov_oglasa'];
				$slanje[$i]["opis"]=$value['opis_oglasa'];
				$slanje[$i]["cena"]=$value['cena'];

				$literatura = $oglas->vratiOglasZaLiteraturuNaOsnovuId(intval($value['id_oglasa']));

				$slanje[$i]['naziv_literature']=$literatura[0]['naziv_literature'];

                $sl= new Slike( intval($value['id_oglasa']), null, null, null);
                $slike=$sl->vratiSveSlikeJednogOglasa();
                if(is_null($slike))
                {
                    $slanje[$i]["slike"]=null;
                }
                else 
                {
                    $slanje[$i]["slike"]=$slike[0];
                }

				$i=$i+1;
			}
			return response()->json($slanje);
    }

    public function filtriraniOglasiZaLiteraturu(Request $request)
    {
    	$json = $_POST;
    	$oglas = new OglasiZaLiteraturu(null, null, null, null, null, null, null, null, null, null, null);
    	$slanje = null;
    	$i = 0;

    	$rezultat = $oglas->filtriranjeOglasaZaLiteraturu($json['naziv_literature'], $json['fakultet'], $json['kupovina_prodaja'], intval($json['minimalna_cena']), intval($json['maksimalna_cena']), $json['grad']);

        if(!is_null($rezultat))
        {
            foreach($rezultat as $value)
            {
                $o = $oglas->vratiOglasZaLiteraturuNaOsnovuId(intval($value['id_oglasa']));
                $slanje[$i]['naziv_literature'] = $o[0]['naziv_literature'];

                $ogl = $oglas->vratiOglasNaOsnovuIda(intval($value['id_oglasa']));

                $slanje[$i]['id_oglasa'] = intval($ogl[0]['id_oglasa']);
                $slanje[$i]['naslov_oglasa'] = $ogl[0]['naslov_oglasa'];
                $slanje[$i]['opis'] = $ogl[0]['opis_oglasa'];
                $slanje[$i]['cena'] = intval($ogl[0]['cena']);

                $sl= new Slike( intval($value['id_oglasa']), null, null, null);
                $slike=$sl->vratiSveSlikeJednogOglasa();
                if(is_null($slike))
                {
                    $slanje[$i]["slike"]=null;
                }
                else 
                {
                    $slanje[$i]["slike"]=$slike[0];
                }
                
                $i = $i + 1;
            }
        }


    	return response()->json($slanje);
    }

    public function proveriDaLiJeDodat($slanje, $grad)
    {
        if(!is_null($slanje[0]))
        {
            foreach($slanje as $value)
            {
                if($value['naziv_fakulteta']===$grad)
                    return true;
            }
            return false;
        }
        return false;
    }


    public function listaSvihFakulteta(Request $request)
    {
        $json = $_POST;
        $oglas = new OglasiZaLiteraturu(null, null, null, null, null, null, null, null, null, null, null);
        $slanje = null;
        $i = 0;

        if($json["grad"] === "")
        {
            $rezultat = $oglas->izlistajSveFakultete();
            foreach($rezultat as $value)
            {
                if(!$this->proveriDaLiJeDodat($slanje, $value['naziv_fakulteta']))
                {
                    $slanje[$i]['naziv_fakulteta'] = $value['naziv_fakulteta'];
                    $i = $i + 1;
                }
            }
        }
        else
        {
            $rezultat = $oglas->izlistajSveFakulteteJednogGrada($json["grad"]);
            foreach($rezultat as $value)
            {
                $slanje[$i]['naziv_fakulteta'] = $value['naziv_fakulteta'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }
}
