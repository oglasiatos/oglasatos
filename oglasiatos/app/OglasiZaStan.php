<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Oglasi;

class OglasiZaStan extends Oglasi
{
    public $cimer_stanar;
    public $adresa_stana;
    public $broj_soba;
    public $povrsina;
    public $tip_grejanja;
    public $opremljenost;
    public $kuca_stan;
    public $sprat;
    public $garaza;
    public $terasa;
    public $parking;
    public $lift;
    public $novogradnja;
    public $internet;
    public $kablovska;
    public $telefon;
    public $podrum;

    public function __construct($korisnicko_ime_izdavaca_oglasa, $naslov_oglasa, $grad, $opis_oglasa, $vrsta_oglasa, $cena, $cimer_stanar, $adresa_stana, $broj_soba, $povrsina, $tip_grejanja, $opremljenost, $kuca_stan, $sprat, $garaza, $terasa, $parking, $lift, $novogradnja, $internet, $kablovska, $telefon, $podrum)
    {
        parent::__construct($korisnicko_ime_izdavaca_oglasa, $naslov_oglasa, $grad, $opis_oglasa, $vrsta_oglasa, $cena);
        $this->cimer_stanar=$cimer_stanar;
        $this->adresa_stana=$adresa_stana;
        $this->broj_soba=$broj_soba;
        $this->povrsina=$povrsina;
        $this->tip_grejanja=$tip_grejanja;
        $this->opremljenost=$opremljenost;
        $this->kuca_stan=$kuca_stan;
        $this->sprat=$sprat;
        $this->garaza=$garaza;
        $this->terasa=$terasa;
        $this->parking=$parking;
        $this->lift=$lift;
        $this->novogradnja=$novogradnja;
        $this->internet=$internet;
        $this->kablovska=$kablovska;
        $this->telefon=$telefon;
        $this->podrum=$podrum;
    }

    public function vratiSveOglaseZaStan()
    {
        return DB::table('oglasi_za_stan')->get();
    }

    public function vratiOglasZaStanNaOsnovuIda($id)
    {
        return DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', $id)->get();
    }

    public function dodajOglasZaStan()
    {
        Oglasi::dodajOglas();

        if($this->opremljenost==="da")
        {
          $opremljen = "Opremljen";
        }
        else
        {
          $opremljen = "Nije opremljen";
        }

        $string = "INSERT INTO oglasi_za_stan (id_oglasa_za_stan, cimer_stanar, adresa_stana, broj_soba, povrsina, tip_grejanja, opremljenost, kuca_stan, sprat, garaza, terasa, parking, lift, novogradnja, internet, kablovska, telefon, podrum) VALUES (".$this->id_oglasa.", '".$this->cimer_stanar."', '".$this->adresa_stana."', ".$this->broj_soba.", ".$this->povrsina.", '".$this->tip_grejanja."', '".$opremljen."', '".$this->kuca_stan."', ".$this->sprat.", ".$this->garaza.", ".$this->terasa.", ".$this->parking.", ".$this->lift.", ".$this->novogradnja.", ".$this->internet.", ".$this->kablovska.", ".$this->telefon.", ".$this->podrum.")";
        DB::raw($string);

        $string = "INSERT INTO dodatni_oglasi_za_stan (id_oglasa_za_stan, cimer_stanar, adresa_stana, broj_soba, povrsina, tip_grejanja, opremljenost, kuca_stan, sprat, garaza, terasa, parking, lift, novogradnja, internet, kablovska, telefon, podrum) VALUES (".$this->id_oglasa.", '".$this->cimer_stanar."', '".$this->adresa_stana."', ".$this->broj_soba.", ".$this->povrsina.", '".$this->tip_grejanja."', '".$opremljen."', '".$this->kuca_stan."', ".$this->sprat.", ".$this->garaza.", ".$this->terasa.", ".$this->parking.", ".$this->lift.", ".$this->novogradnja.", ".$this->internet.", ".$this->kablovska.", ".$this->telefon.", ".$this->podrum.")";
        DB::raw($string);
    }

    public function obrisiOglasZaStan()
    {
        Oglasi::obrisiOglas($this->id_oglasa);
        $oglas = DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', $this->id_oglasa)->get();
        DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', $this->id_oglasa)->deleteRow();
              if($oglas[0]['opremljenost']==="da")
              {
                $opremljen = "Opremljen";
              }
              else
              {
                $opremljen = "Nije opremljen";
              }

              $brisanje = "DELETE FROM oglasi_za_stan WHERE kuca_stan = '".$oglas[0]['kuca_stan']."' AND cimer_stanar = '".$oglas[0]['cimer_stanar']."' AND tip_grejanja = '".$oglas[0]['tip_grejanja']."' AND opremljenost = '".$opremljen."' AND garaza = ".$oglas[0]['garaza']." AND terasa = ".$oglas[0]['terasa']." AND parking = ".$oglas[0]['parking']." AND lift = ".$oglas[0]['lift']." AND novogradnja = ".$oglas[0]['novogradnja']." AND internet = ".$oglas[0]['internet']." AND kablovska = ".$kablovska[0]['kablovska']." AND telefon = ".$oglas[0]['telefon']." AND podrum = ".$oglas[0]['podrum']." AND broj_soba = ".$oglas[0]['broj_soba']." AND povrsina = ".$oglas[0]['povrsina']." AND sprat = ".$oglas[0]['sprat']." AND id_oglasa_za_stan = ".intval($oglas[0]['id_oglasa_za_stan']);

              DB::raw($brisanje);
    }

    public function vratiOglasZaStanPremaIdu($idOglasa)
    {
        $string = "SELECT * FROM dodatni_oglasi_za_stan WHERE id_oglasa_za_stan = ".$idOglasa;
        return DB::raw($string);
    }



    public function filtrirajBooleanVrednosti($atribut, $ime, $query)
    {
          if($atribut==="true")
          {
            $query .= " AND ".$ime." = true ";
          }
          else
          {
            $query .= " AND ".$ime." in (true, false) ";
          }
          return $query;
    }


    public function filtrirajOglaseZaStan($kucaStan, $cimerStanar, $tipGrejanja, $opremljenost, $garaza, $terasa, $parking, $lift, $novogradnja, $internet, $kablovska, $telefon, $podrum, $minBrojSoba, $maksBrojSoba, $minPovrsina, $maksPovrsina, $minSprat, $maksSprat, $grad, $minCena, $maksCena)
    {
          $query = "SELECT id_oglasa_za_stan FROM oglasi_za_stan WHERE ";

          if($kucaStan === "")
        {
            $query .= " kuca_stan in ('Kuca', 'Stan') ";
        }
        else
        {
            $query .= " kuca_stan = '".$kucaStan."' ";
        }

        if($cimerStanar === "")
        {
            $query .= " AND cimer_stanar in ('Cimer', 'Stanar') ";
        }
        else
        {
            $query .= " AND cimer_stanar = '".$cimerStanar."' ";
        }

        if($tipGrejanja === "")
        {
            $query .= " AND tip_grejanja in ('Centralno grejanje', 'TA', 'Ostalo') ";
        }
        else
        {
            $query .= " AND tip_grejanja = '".$tipGrejanja."' ";
        }

        if($opremljenost==="true")
        {
            $query .= " AND opremljenost = 'Opremljen' ";
        }
        else
        {
            $query .= " AND opremljenost in ('Opremljen', 'Nije opremljen') ";
        }

        $query = $this->filtrirajBooleanVrednosti($garaza, "garaza", $query);
        $query = $this->filtrirajBooleanVrednosti($terasa, "terasa", $query);
        $query = $this->filtrirajBooleanVrednosti($parking, "parking", $query);
        $query = $this->filtrirajBooleanVrednosti($lift, "lift", $query);
        $query = $this->filtrirajBooleanVrednosti($novogradnja, "novogradnja", $query);
        $query = $this->filtrirajBooleanVrednosti($internet, "internet", $query);
        $query = $this->filtrirajBooleanVrednosti($kablovska, "kablovska", $query);
        $query = $this->filtrirajBooleanVrednosti($telefon, "telefon", $query);
        $query = $this->filtrirajBooleanVrednosti($podrum, "podrum", $query);

        $query .= " AND broj_soba <= ".$maksBrojSoba." AND broj_soba >= ".$minBrojSoba;

        //return $query;

        $idjevi = DB::raw($query);

        $niz = null;
        $i = 0;

        if(!is_null($idjevi[0]))
        {
            foreach($idjevi as $value)
            {
                $oglasStan = DB::table('dodatni_oglasi_za_stan')->where('id_oglasa_za_stan', intval($value['id_oglasa_za_stan']))->select('povrsina', 'sprat')->get();
                if(intval($oglasStan[0]['povrsina'])<=$maksPovrsina && intval($oglasStan[0]['povrsina'])>=$minPovrsina && intval($oglasStan[0]['sprat'])>=$minSprat && intval($oglasStan[0]['sprat'])<=$maksSprat)
                {
                    $niz[$i] = $value['id_oglasa_za_stan'];
                    $i = $i + 1;
                }
            }
            //return $niz;
            return Oglasi::filtrirajOglase("Oglas Za Stan", $grad, $minCena, $maksCena, $niz);
        }
        else
        {
            return null;
        }
    }
}
