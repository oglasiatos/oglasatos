<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('index');
});

//index-2
Route::get('/index-2.html', function () {
    return view('index-2');
});


Route::get('/adminProfil.html', function () {
    return view('admin');
});


Route::get('/stanovanje.html', function () {
    return view('stanovanje');
});

Route::get('/registrovanje.html', function () {
    return view('registrovanje');
});

Route::get('/prijavljivanje.html', function () {
    return view('prijavljivanje');
});

Route::get('/posao.html', function () {
    return view('posao');
});

Route::get('/literatura.html', function () {
    return view('literatura');
});

Route::get('/oglasStan.html', function () {
    return view('oglasStan');
});

Route::get('/oglasLiteratura.html', function () {
    return view('oglasLiteratura');
});

Route::get('/oglasPosao.html', function () {
    return view('oglasPosao');
});

Route::get('/dodavanjeOglasaStan.html', function () {
    return view('dodavanjeOglasaStan');
});

Route::get('/dodavanjeOglasaLiteratura.html', function () {
    return view('dodavanjeOglasaLiteratura');
});

Route::get('/dodavanjeOglasaPosao.html', function () {
    return view('dodavanjeOglasaPosao');
});

Route::get('/novaPoruka.html', function () {
    return view('novaPoruka');
});

Route::get('/poruke.html', function () {
    return view('poruke');
});
Route::get('/mojiOglasiStan.html', function () {
    return view('mojiOglasiStan');
});

Route::get('/mojiOglasiLiteratura.html', function () {
    return view('mojiOglasiLiteratura');
 });

Route::get('/mojiOglasiPosao.html', function () {
    return view('mojiOglasiPosao');
});

Route::get('/azuriranjeLozinke.html', function () {
    return view('azuriranjeLozinke');
});

Route::get('/brisanjeProfila.html', 'Profil@brisanjeProfila');

Route::get('/prijavljeniKorisniciNaOglas.html', function () {
    return view('citanjePrijava');
});


Route::get('/test', function () {
     Schema::drop('student');
    Schema::create(
        'student', function ($table) {
            $table->int('id');
            $table->text('name');
            $table->primary(['id']);
      });
    DB::table('student')->insert(['id' => 0, 'name' => 'Pera']);
    $emp = DB::table('student')->get();
    return $emp[0];
});

Route::get('/kreiranjeBazePodataka', 'KreiranjeTabelaController@kreiranjeTabela');

Route::get('/testiranjeModela', 'TestController@testFunkcija');

//--------------------Jovanov ruter----------------------// 
//-------------------------------------------------------//
Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');

Route::get('/registrovanje', 'Registrovanje@ajaxRequest');
Route::post('/registrovanje', 'Registrovanje@ajaxRequestPost');

Route::get('/prijavljivanje', 'Prijavljivanje@ajaxRequest');
Route::post('/prijavljivanje', 'Prijavljivanje@ajaxRequestPost');

Route::get('/profil.html', 'Profil@ajaxRequest');
Route::post('/profil', 'Profil@ajaxRequestPost');
Route::post('/profilizmeni', 'Profil@izmeni');
Route::post('/dodajOglasStan', 'OglasStan@ajaxRequestPost');


//mojiOglasiStan
Route::post('/mojiOglasiStan', 'mojiOglasiStan@ajaxRequestPost');
Route::get('/mojiOglasiStan', 'mojiOglasiStan@ajaxRequest');

Route::post('/listanjeporuka', 'porukeController@vratisveporuke');

//slanjeporuke
Route::post('/slanjeporuke', 'porukeController@posaljiporuku');
//ucitajkonverzaciju
Route::post('/ucitajkonverzaciju', 'porukeController@ucitajkonverzaciju');
Route::post('/procitajpk','porukeController@procitajporuke');
Route::get('/slanjeporuke', 'porukeController@test');
Route::post('/obrisipk','porukeController@brisanjePoruke');

Route::post("/slanjeprijave", "prijavaikomentar@slanjeprijave");
Route::post("/komentarisi", "prijavaikomentar@komentarisi");

Route::post("/komentari", "prijavaikomentar@komentari");

Route::post("/obrisikom","prijavaikomentar@obrisikom");

Route::post("/ocenjivanje","prijavaikomentar@ocenjivanje");


///slike profil
Route::post("/slikeProfil","SlikeProfilaController@post_upload")->name('dropzone.store');

///-----------------------------------------------------//


//------------------JOXY------------------------------//
//---------------------------------------------------//

Route::post('/dodajOglasLiteratura', 'OglasLiteratura@ajaxRequestPost');

Route::get('/mojiOglasiLiteratura', 'mojiOglasiLiteraturaController@ajaxRequest');
Route::post('/mojiOglasiLiteratura', 'mojiOglasiLiteraturaController@ajaxRequestPost');
Route::post('/brisanjeOglasa', 'AdminController@brisanjeOglasa');
Route::get('/prijavljeniNaOglas{a}', 'prijavljeniNaOglas@pocetna');
Route::post('/prijavljeniNaOglas', 'prijavljeniNaOglas@izlistajPrijave');


Route::post('/mojiOglasiPosao', 'mojiOglasiPosao@ajaxRequestPost');
Route::post('/dodajOglasPosao', 'OglasPosao@ajaxRequestPost');

Route::post('/mojiOglasiStan', 'mojiOglasiStan@ajaxRequestPost');
Route::post('/dodajOglasStan', 'OglasStan@ajaxRequestPost');
Route::get('/oglasStan{a}',"OglasStan@vratistan");

Route::post('/predloziOglasa',"StanovanjeController@predloziOglasaZaStan");

Route::post('/predloziOglasaLitaratura',"LiteraturaController@listaPredlogaZaLiteraturu");

Route::post('/predloziOglasaPosao',"PosaoController@predloziZaPosao");

Route::post('/citanjePrijava', "prijavljeniNaOglas@izlistajPrijave");

Route::get('/novaPoruka{a}', "prijavljeniNaOglas@novaPoruka");
//------------------------------------------------------//


//------------------KALE---------------//
//-------------------------------------//

Route::post('/listaOglasaZaStan', 'StanovanjeController@listaOglasaZaStan');
Route::post('/predloziOglasaZaStan', 'StanovanjeController@predloziOglasaZaStan');
Route::post('/filtriranjeOglasaZaStan','StanovanjeController@filtriranjeOglasaZaStan');
Route::post('/selectZaGrad', 'StanovanjeController@selectZaGrad');
//Route::post('/selectZaGrejanje', 'StanovanjeController@selectZaGrejanje');

Route::post('/listaOglasaZaLiteraturu','LiteraturaController@listaOglasaZaLiteraturu');
Route::post('/listaPredlogaZaLiteraturu','LiteraturaController@listaPredlogaZaLiteraturu');
Route::post('/filtriraniOglasiZaLiteraturu','LiteraturaController@filtriraniOglasiZaLiteraturu');
Route::post('/selectZaFakultete','LiteraturaController@listaSvihFakulteta');

Route::post('/listaOglasaZaPosao','PosaoController@listaOglasaZaPosao');
Route::post('/predloziZaPosao','PosaoController@predloziZaPosao');
Route::post('/filtiranjeOglasaZaPosao','PosaoController@filtiranjeOglasaZaPosao');

Route::post('/listaOglasa','AdminController@listaOglasa');
Route::post('/brisanjeOglasa','AdminController@brisanjeOglasa');
Route::post('/blokiranjeKorisnika','AdminController@blokiranjeKorisnika');
Route::post('/pretraziKorisnike','AdminController@pretraziKorisnike');

Route::get('/oglasPosao{a}', 'OglasPosao@velikiOglasZaPosao');
Route::get('/oglasLiteratura{a}', 'OglasLiteratura@velikiOglasZaLiteraturu');

Route::post('/predloziOglasaZaStanUVelikomOglasu','OglasStan@predloziOglasaZaStanUVelikomOglasu');

Route::post("/slike","SlikeController@post_upload")->name('dropzone.store');
Route::get('/sliketest','SlikeController@test');
Route::post("/proveriPrijavu","ProveraPrijaveIOdjaveController@proveriPrijavu");

Route::post("/session/remove","SessionController@deleteSessionData");