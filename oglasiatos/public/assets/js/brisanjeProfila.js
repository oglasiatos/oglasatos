(function ($) {
    "use strict";
    
    /*==================================================================
    [ Validate ]*/
    var input = $('.form-control');
    var sacuvajizmeni="izmeni";
    
    $(document).ready(function(){
             for(var i=0; i<(input.length)-1; i++) {
            $(input[i]).prop( "disabled", true );}


        });
    //$(document).ready(function(){
    /*$("#brisanje.btn").click(function(){

        
        
    });*/
    $("#brisanje.btn").confirm({
    title: 'Brisanje profila!',
    content: 'Da li ste sigurni da želite da obrišete profil?',
    buttons: {
        Da: function () {
            var niz= {};
            niz['komanda']='brisanje';
            sendajx(niz);
            $.alert('Profil je uspešno obrisan.');
        },
        Ne: function () {
            $.alert('');
        },
       
    }
});
    

    function test () {   
        var check = true;
        var stringArray = {};
        

        for(var i=0; i<(input.length)-1; i++) {
            console.log($(input[i]).attr('name'));
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }else
            {
                stringArray[$(input[i]).attr('name') ] = $(input[i]).val();
                //console.log($(input[i]).attr('name'));
            }
        }
        if(check==true)
        {
            console.log(stringArray);
            sendajx(stringArray);
        }    
        return check;
    };

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $('.form-control').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function sendajx(stringArray)
    {
       

        console.log(stringArray);
    
        var json_string = JSON.stringify(stringArray);
        console.log(json_string);
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                if(data['Status']==='Uspesno obrisan nalog!')
                {
                     
                     //setujkuki(data['Status']);
                     window.location = Settings.profil_url;
                     //$(".login100-form").attr("hidden",true);

                     //$(".uspesna").html("<span class='uspesno-txt'>Uspesno ste registrovani mozete pregledati svoj profil </span><a href='profil' class='uspesno-txt'>ovde</a>");
                }
            },
            dataType: "json"
            //traditional: true
        });

            
    };
    function validate (input) {

        /*if($(input).attr('id') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }*/
        if($(input).attr('id') == 'lozinka')
        {
             //console.log($(input).val());
            if($(input).val().length<8)
            {
                showValidate(input);
                //console.log("Kratka je sifra");
                return false;
            }
        }
        else if($(input).val().length<2)
        {
                return false;
        }


    }
    function zabrani()
    {
        var zaZabranu=$('.zabrani');
        for(var i=0; i<(zaZabranu.length); i++) {
            $(zaZabranu[i]).prop("disabled",true);
        }

    }

    function showValidate(input) {

        var thisAlert = $(input).parent();;
        //console.log(thisAlert);
        //$(input).attr('id', 'inputWarning');
        $(thisAlert).addClass('has-error has-feedback');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('has-error has-feedback');
    }
    

    

})(jQuery);