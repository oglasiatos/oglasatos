(function ($) {
    "use strict";
    
    /*==================================================================
    [ Validate ]*/
    var input = $('.kontrole');
    var sacuvajizmeni="izmeni";

    $(document).ready(function(){
        izlistajSelectOpcijeZaGrad();
    });
    
    $("#dodaj.btn").click(function(){
        /*for(var i=0; i<(input.length)-1; i++) {
            console.log($(input[i]).attr('name'));
            if($(input[i]).attr('type')==="checkbox")
            {
                console.log("cek");
                console.log($(input[i]).is(':checked'));
            }
            else
            {
                console.log($(input[i]).attr('value'));
            }
        }*/
        test();
        //console.log("izmeni");
        
    });

    function izlistajSelectOpcijeZaGrad()
    {
        $('.odabirGrada[name="grad"]').empty();
        var s="";

        $.ajax({
            method: "POST",
            url: Settings.selectZaGrad_url,
            success: function(data)
            {
                console.log(data);
                $.each(data, function(i, l){
                    console.log(l);

                    s="<option>"+l['grad']+"</option>";

                    $('.odabirGrada[name="grad"]').append(s);
                    $('.odabirGrada').selectpicker('refresh');
                });
            },
            dataType: "json"

        });
    };

    $("#prethodni.btn").click(function(){
        if($("#step4").hasClass('active'))
        {
             $("#sledeci.btn").show();
        }
    });
    $("#sledeci.btn").click(function(){

          //console.log("sledeci");
          var input2 = $('.kontrole:visible');
          var validan=true;
          //console.log(input2);
          for(var i=0;i<(input2.length);i++)
          {
            if(validate(input[i]) === false){
                showValidate(input[i]);
                validan=false;
            }
          }
          if(validan===true)
          {

            if($("#step3").hasClass('active'))
            {
                $("#sledeci.btn").hide();
                
            }
           
             $(".btn-next").trigger( "click" );
          }
           $("#sledeci.btn").blur()
    });
    $("#prethodni.btn").click(function(){
          console.log("prethodni");
     });
    function test () {   
        var check = true;
        var stringArray = {};
        

        for(var i=0; i<(input.length); i++) {
            //console.log($(input[i]).attr('name'));
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }else
            {
                if($(input[i]).attr('type')==="checkbox")
                {
                    stringArray[$(input[i]).attr('name') ] = $("#"+$(input[i]).attr('name')).is(":checked");
                }
                else if($(input[i]).attr('type')==="radio")
                {
                     stringArray[$(input[i]).attr('name') ] =$("[name='"+$(input[i]).attr('name')+"']:checked").val();
                     console.log(stringArray[$(input[i]).attr('name') ]);   
                }
                else 
                {
                    stringArray[$(input[i]).attr('name') ] = $(input[i]).val();
                }
                //console.log($(input[i]).attr('name'));
            }
        }
        if(check==true)
        {
            //console.log(stringArray);
            sendajx(stringArray);
        }    
        return check;
    };

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $('.form-control').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function sendajx(stringArray)
    {
       

        console.log(stringArray);
    
        var json_string = JSON.stringify(stringArray);
        console.log(json_string);
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                if(data['Status']==='Uspesno!')
                {
                     /*for(var i=0; i<(input.length)-1; i++) {
                      $(input[i]).prop( "disabled", true );}
                    */
                     //setujkuki(data['Status']);
                     window.location = Settings.mojoglas_url;
                     //$(".login100-form").attr("hidden",true);

                     //$(".uspesna").html("<span class='uspesno-txt'>Uspesno ste registrovani mozete pregledati svoj profil </span><a href='profil' class='uspesno-txt'>ovde</a>");
                }
                else
                {
                        window.location = Settings.prijava_url;
                }
            },
            dataType: "json"
            //traditional: true
        });

            
    };
    function validate (input) {

        if($(input).attr('type') == 'text'  && $(input).val().length<1)
        {
              return false;
        }else if($(input).attr('name') == 'opis'  && $(input).val().length<1)
        {
             return false;
        }else if($(input).hasClass('selectpicker') && $(input).val().length<1)
        {
             return false;
        }
    }
    
    function zabrani()
    {
        var zaZabranu=$('.zabrani');
        for(var i=0; i<(zaZabranu.length); i++) {
            $(zaZabranu[i]).prop("disabled",true);
        }

    }

    function showValidate(input) {

        var thisAlert = $(input).parent();
        //console.log(thisAlert);
        //$(input).attr('id', 'inputWarning');
        $(thisAlert).addClass('has-error has-feedback');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('has-error has-feedback');
    }
    

    

})(jQuery);