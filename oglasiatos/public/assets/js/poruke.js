(function ($) {
    "use strict";

    $(function(){
        console.log("Usao");
        izlistajPoruks();
        console.log("Izaso");
    });

    function izlistajPoruks()
    {
        $('.konverzecije1').empty();
        $('.konverzecije2').empty();
        var s = "";
        console.log("Usao2");

        $.ajax({
            method: "POST",
            url: Settings.base_url,
            success: function(data)
            {
                console.log(data);

                $.each( data, function(i,l){

                    console.log($("."+l["korisnicko_ime_posiljaoca"]).length);
                    if($("."+l["korisnicko_ime_posiljaoca"]).length===0)
                    {

                        if(l['procitana_poruka'])
                        {
                            s="<label ><span class='strong "+l["korisnicko_ime_posiljaoca"]+"'>"+l["korisnicko_ime_posiljaoca"];
                            s+="</span></label><button type='button' class='pull-right  btn"+l["korisnicko_ime_posiljaoca"]+"'><span ";
                            s+="class='glyphicon glyphicon-trash'></span></button><hr>";
                            $('.konverzecije2').append(s);
                        }
                        else
                        {
                        	
                            s="<label ><span class='orange strong "+l["korisnicko_ime_posiljaoca"]+"'><b>"+l["korisnicko_ime_posiljaoca"]+"</b>";
                            s+="</span></label><button type='button' class='pull-right btn"+l["korisnicko_ime_posiljaoca"]+"'>";
                            s+="<span class='glyphicon glyphicon-trash'></span></button><hr>";
                             $('.konverzecije1').append(s);
                        }
                        
                        $("."+l["korisnicko_ime_posiljaoca"]).click(function(d)
                        {
                            console.log("TEst");
                            console.log(d.target.innerText);
                            ucitajkonverzaciju(d.target.innerText);
                        });
                    }
                });
            },
            dataType: "json"
        });
    }

    $(".posaljiovomkorisniku").click(function(){
         $(".posaljiovomkorisniku").prop('disabled', true);
        console.log($(".kimekonverz").text());
        var poruka={};
        poruka['poruka']=$("#brzaporuka").val();
        poruka['primalac']=$(".kimekonverz").text();
        slanjePoruke(poruka);
        izlistajPoruks();
        ucitajkonverzaciju(poruka['primalac']);
        $("#brzaporuka").empty();
        $(".posaljiovomkorisniku").prop('disabled', false);
    });

    $(".prikazislanje").click(function()
	{
		$(".slanjeforma").show();
	});

	$(".posalji").click(function(){
		if($("#primalac").val().length >1)
		{
			var poruka={};
			poruka['poruka']=$("#poruka").val();
			poruka['primalac']=$("#primalac").val();
			slanjePoruke(poruka);
			$(".slanjeforma").hide();
            ucitajkonverzaciju(poruka['primalac']);
		}
		else
		{
			$("#primalac").addClass('has-error has-feedback');	
		}
	});
    function procitajpk(str)
    {
         var poruka={};
         poruka['kime']=str;
         $.ajax({
            method: "POST",
            url: Settings.procitajpk_url,
            data: poruka,
            success: function(data)
            {
                console.log(data);
                if(data['Status']=="Prijavi se!")
                {
                    window.location(Settings.prijava_url);
                }

            },
            dataType: "json"
        });    
    }

     function obrisipk(str)
    {
         var poruka={};
         poruka['id_poruke']=str;
         $.ajax({
            method: "POST",
            url: Settings.obrisipk_url,
            data: poruka,
            success: function(data)
            {
                console.log(data);
                if(data['Status']=="Prijavi se!")
                {
                    window.location(Settings.prijava_url);
                }
                else
                {
                    ucitajkonverzaciju($(".kimekonverz").text());
                }

            },
            dataType: "json"
        });    
    }

    function ucitajkonverzaciju(str)
    {
         var poruka={};
         poruka['drugikorisnik']=str;
         $.ajax({
            method: "POST",
            url: Settings.ucitajkonverzaciju_url,
            data: poruka,
            success: function(data)
            {
                console.log(data);
                $(".kimekonverz").empty();
                $(".kimekonverz").append(str);
                $(".konvesadrzaj").empty();
                var s="";
                var pom;
                var data2=sortByKeyAsc(data, "id_poruke");
                console.log(data2);
                $.each( data2, function(i,l){

                    //console.log(l);
                    pom=l;
                    s="<label >"+l['korisnicko_ime_posiljaoca']+"</label>";
                    if(!(pom["korisnicko_ime_posiljaoca"]===str))
                    {

                        s+="<p class='popover-title' style='padding: 10px;'>"+l["sadrzaj_poruke"]+" <i class='pull-right' id='"+l['id_poruke']+"'> x </i></p>";
                    }
                    else
                    {
                        
                        s+="<p class='popover-title' style='padding: 10px;'>"+l["sadrzaj_poruke"]+"</p>";
                    }
                    
                    //console.log(!(l["korisnicko_ime_posiljaoca"]===str));
                    
                    $(".konvesadrzaj").append(s);
                    $("#"+l['id_poruke']).click(function(d){
                            console.log(l['id_poruke']);
                            console.log($(".kimekonverz").text());
                            obrisipk(l['id_poruke']);
                    });
                });
                if(pom["korisnicko_ime_posiljaoca"]===str)
                {
                   s="<span class='glyphicon glyphicon-ok pull-right' aria-hidden='true'></span>";
                }
                else
                {
                    if(pom['procitana_poruka'])
                    {
                       s="<span class='glyphicon glyphicon-ok pull-right' aria-hidden='true'></span>";

                    }
                    else
                    {
                        s="";
                    }
                }
                $(".konvesadrzaj").append(s);
                $(".dolesk").scrollTop(jQuery(".dolesk")[0].scrollHeight);
                procitajpk(str);

                $(".posaljiovomkorisniku").prop("disabled",false);
                $("#brzaporuka").prop("disabled",false);

            },
            dataType: "json"
        });    
    }
    
    function sortByKeyAsc(array, key) {
        return array.sort(function (a, b) {
            console.log("sort");
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

	function slanjePoruke(poruka)
    {
       
        $.ajax({
            method: "POST",
            url: Settings.slanjeporuka_url,
            data: poruka,
            success: function(data)
            {
                console.log(data);
                if(data['Status']==="Primalac ne postoji!")
                {
                		$("#primalac").addClass('has-error has-feedback');
                }
                else if(data['Status']==="Prijavi korisnika!")
                {
                		window.location(Settings.prijava_url);
                }

            },
            dataType: "json"
        }); 
    }

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });



    

    

})(jQuery);