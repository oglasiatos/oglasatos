(function ($) {
    "use strict";
   

    $(function(){
        console.log("Usao");
        predlozeniOglasi();
        
    });

    function predlozeniOglasi()
    {
        $('.predloziLiteratura').empty();
        var s = "";

        $.ajax({
            method: "POST",
            url: Settings.predloziOglasaLiteratura_url,
            success: function(data)
            {
                console.log(data);
                $.each( data, function(i,l){
                    console.log(l);
                    s = "<li><div class='col-md-3 col-sm-4 col-xs-4 blg-thumb p0'>";
                    s+="<a href='"+Settings.predlozi_url+l['id_oglasa']+"'><img src='assets/img/demo/small-proerty-2.jpg'></a>";
                    s+="<span class='blg-date'>"+l['cena']+"€</span></div><div class='col-md-8  col-sm-8 col-xs-8  blg-entry'  style='white-space: nowrap;width: 150px;overflow: hidden;text-overflow: clip;'>";
                    s+="<h6> <a href='"+Settings.predlozi_url+l['id_oglasa']+"'>"+l['naslov_oglasa']+"</a></h6> ";
                    s+="<p style='line-height: 17px; padding: 8px 2px; white-space: nowrap;width: 100px;overflow: hidden;text-overflow: clip;'>"+l['opis']+"</p></div></li>";

                    $('.predloziLiteratura').append(s);

                });
            },
            dataType: "json"
        });
    };

         $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

 })(jQuery);