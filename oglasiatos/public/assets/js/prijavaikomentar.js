
(function ($) {
    "use strict";
    
    /*==================================================================
    [ Validate ]*/

    $(document).ready(function(d)
    {
        izlistajkomentare($(".komentarisi").attr("id"));
        //$(".sakri").hide();
    });
    $(".star-rating").click(function()
    {
        console.log($(".dealer-name").children("a").text());
        oceni($(".rating-value").val(),$(".dealer-name").children("a").text());
        
    });

    function oceni(ocena,korisnik)
    {
        var niz={};
        niz["ocena"]=ocena;
        niz["korisnik"]=korisnik;
        $.ajax({
            method: "POST",
            url: Settings.ocenjivanje_url,
            data:niz,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                //izlistajkomentare($(".komentarisi").attr("id"));
            },
            dataType: "json"
            //traditional: true
        });
    }

    $(".prijavinaoglas").click(function(d)
    {
        prijavise(d.target.id);
    });

    $(".komentarisi").click(function(d)
    {
        var kom=$(".komentarunos").val();
        $(".komentarunos").prop("value","");
        var niz={};
        niz["id_oglasa"]=$(".komentarisi").attr("id");
        niz["sadrzaj"]=kom;
        komentarisi(niz);
        console.log(kom);
    });


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    function izlistajkomentare(str)
    {
        var niz={};
        niz["id_oglasa"]=str;
        $.ajax({
            method: "POST",
            url: Settings.komentari_url,
            data:niz,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                 $(".listaKomentara").empty();
                 $.each( data, function(i,l){
                    console.log(l);
                    var s = "<label for='firstname'>"+l['korisnik']+"</label>";
                    s+="<p class='background-color' style='padding: 10px;'>";
                    s+=l['sadrzaj'];
                    if(l['licni']===l['korisnik'])
                    {
                        s+=" <i class='pull-right ' id='"+l['id_komentara']+"'> x </i></p>";
                    }
                    else
                    {
                        s+="</p>";
                    }
                    $('.listaKomentara').append(s);

                    $("#"+l["id_komentara"]).click(function()
                    {
                        console.log(l["id_komentara"]);
                        obrisikom(l["id_komentara"]);
                        izlistajkomentare($(".komentarisi").attr("id"));
                    });

                });
            },
            dataType: "json"
            //traditional: true
        });

    };

    function obrisikom(str)
    {
        var niz={};
        niz["id_komentara"]=str;
        $.ajax({
            method: "POST",
            url: Settings.obrisikom_url,
            data:niz,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                izlistajkomentare($(".komentarisi").attr("id"));
            },
            dataType: "json"
            //traditional: true
        });
    }
    
    function komentarisi(str)
    {
        console.log(str);
        $.ajax({
            method: "POST",
            url: Settings.komentarisi_url,
            data:str,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                izlistajkomentare($(".komentarisi").attr("id"));
            },
            dataType: "json"
            //traditional: true
        });

            
    };


    function prijavise(str)
    {
        var niz={};
        niz["id_oglasa"]=str;
        $.ajax({
            method: "POST",
            url: Settings.slanjeprijave_url,
            data:niz,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
            },
            dataType: "json"
            //traditional: true
        });

            
    };
    

})(jQuery);