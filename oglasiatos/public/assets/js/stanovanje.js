(function ($) {
    "use strict";
    var trenutne=1;
    var brojstrana=0;

    $(function(){
        console.log("Usao");
        brojacstrana();
        izlistajOglase();
        //predlozeniOglasi();
        selectZaGradove();
    });

   

    function selectZaGradove()
    {
        $('.odabirGrada[name="grad"]').empty();

        var s="";

        $.ajax({
            method: "POST",
            url: Settings.selectZaGrad_url,
            success: function(data)
            {
                console.log(data);
                $.each(data, function(i, l){
                    console.log(l);

                    s="<option>"+l['grad']+"</option>";

                    $('.odabirGrada[name="grad"]').append(s);
                    $('.odabirGrada').selectpicker('refresh');
                })
            },
            dataType: "json"
        });
    };

    function izlistajOglase()
    {
        $('.oglasi').empty();
        var s="<div class='col-sm-6 col-md-4 p0'>";
        s+="<div class='box-two proerty-item'><div class='item-thumb'>";
        s+="<a href='property-1.html' ><img src='assets/img/demo/property-3.jpg'></a>";
        s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'>Naziv oglasa</a></h5>";
        s+="<div class='dot-hr'></div><span class='pull-left'><b>Površina:</b> 120m </span>";
        s+="<span class='proerty-price pull-right'> $ 300,000</span><p style='display: none;''>Suspendisse ultricies Suspendisse ultricies Nulla quis dapibus nisl. Suspendisse ultricies commodo arcu nec pretium ...</p>";
        s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
        s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div>";

        console.log("CAO, KALE");
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            success: function(data)
            {
                console.log(data);
                $.each( data, function(i,l){

                    console.log(l);

                    s="<a href = '"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+"'><div class='col-sm-6 col-md-4 p0'>";
                    s+="<div class='box-two proerty-item'><div class='item-thumb'>";
                    if(l["slike"]!==null)
                    {
                        s+="<img src='"+l["slike"]+"'>";
                    }
                    else
                    {
                        s+="<img src='assets/img/demo/property-3.jpg'>";
                    }
                    s+="</div><div class='item-entry overflow'><h5 style='white-space: nowrap;width: 350px;overflow: hidden;text-overflow: clip;'>"+l['naslov_oglasa']+"</h5>";
                    s+="<div class='dot-hr'></div><span class='pull-left'><b>Površina:</b> "+l['povrsina']+" </span>";
                    s+="<span class='proerty-price pull-right'>"+l['cena']+"€</span><p style='display: none;''>"+l['opis']+"</p>";
                    s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
                    s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div></a>";

                    $('.oglasi').append(s);

                });
            },
            dataType: "json"
        }); 
    }


     function brojacstrana()
    {
       
        
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            success: function(data){
                    //var array = JSON.parse(data)
                    console.log(data);
                    brojstrana=data.length;
                    console.log(brojstrana);
                    $(".stranice").empty();
                    var s="<li><a class='strpre'>Prethodna</a></li>";
                    $(".stranice").append(s);

                     $(".strpre").click(function(d)
                        {
                            if(trenutne>1)
                            {
                                $("#"+trenutne).parent().removeClass("page-item active")
                                trenutne=trenutne-1;
                                $("#"+trenutne).parent().addClass("page-item active")
                                vratiOglase();
                            }
                            
                    });

                    for (var i = 0; i < brojstrana/9; i++) {
                        var indeks=i+1;
                        console.log(i);
                        s="<li><a class='str' id='"+indeks+"'>"+indeks+"</a></li>";
                        $(".stranice").append(s);
                        $("#"+indeks+".str").click(function(d)
                        {
                            console.log(d.target.id);
                            $("#"+trenutne).parent().removeClass("page-item active")
                            trenutne=d.target.id;
                            $("#"+trenutne).parent().addClass("page-item active")
                            vratiOglase();
                        });
                    }
                    s="<li><a class='sldstr'>Sledeca</a></li>";
                    $(".stranice").append(s);
                    $(".sldstr").click(function(d)
                        {

                            if(trenutne<(brojstrana/9))
                            {
                                console.log("aSad");
                                $("#"+trenutne).parent().removeClass("page-item active")
                                trenutne=trenutne+1;
                                $("#"+trenutne).parent().addClass("page-item active")
                                vratiOglase();
                            }
                            
                    });
                    $("#"+trenutne).parent().addClass("page-item active")

            },
            dataType: "json"
            //traditional: true
        });

            
    };



    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


    var inputiZaPretragu = $('.pretraga');
    
    var slajderCena;
    var slajderPovrsina;
    var slajderSprat;
    var slajderBrojSoba;
    var proveraCena = false;
    var proveraPovrsina = false;
    var proveraSprat = false;
    var proveraBrojSoba = false;

    $('.sliderCena').on('slide', function (ev) {
                               slajderCena = ($('.slider1').val());
                               proveraCena = true;
                           });

    $('.sliderPovrsina').on('slide', function (ev) {
                               slajderPovrsina = ($('.slider2').val());
                               proveraPovrsina = true;
                           });

    $('.sliderSprat').on('slide', function (ev) {
                               slajderSprat = ($('.slider3').val());
                               proveraSprat = true;
                           });

    $('.sliderBrojSoba').on('slide', function (ev) {
                               slajderBrojSoba = ($('.slider4').val());
                               proveraBrojSoba = true;
                           });

    function formirajMinimalneVrednostiSlajdera(slajder, niz)
    {
        for (var i = 0; i< slajder.length;i++)
        {
            if(slajder[i]==",")
            {
                break;
            }
            niz = niz.concat(slajder[i]);
        }
        return niz;
    };

    function formirajMaksimalneVrednostiSlajdera(slajder, niz)
    {
        for(var i = 0; i < slajder.length; i++)
        {
            if(slajder[i] == ",")
                break;
        }
        for(var j = i+1; j < slajder.length; j++)
        {
            niz = niz.concat(slajder[j]);
        }
        return niz;
    };

    $('.pretrazi_oglase.btn').click(function(){
        console.log('Pocela pretraga');
        inputiZaPretragu = $('.pretraga');

        var minimalnaCena = 0;
        var maksimalnaCena = 100000;
        var minimalnaPovrsina = 0;
        var maksimalnaPovrsina = 12000;
        var minimalniSprat = 0;
        var maksimalniSprat = 20;
        var minimalniBrojSoba = 1;
        var maksimalniBrojSoba = 10;

        var niz1 = "";
        var niz2 = "";

        if(proveraCena)
        {
            minimalnaCena = parseInt(formirajMinimalneVrednostiSlajdera(slajderCena, niz1));
            maksimalnaCena = parseInt(formirajMaksimalneVrednostiSlajdera(slajderCena, niz2));
        }

        console.log(minimalnaCena);
        console.log(maksimalnaCena);

        niz1 = "";
        niz2 = "";

        if(proveraPovrsina)
        {
            minimalnaPovrsina = parseInt(formirajMinimalneVrednostiSlajdera(slajderPovrsina, niz1));
            maksimalnaPovrsina = parseInt(formirajMaksimalneVrednostiSlajdera(slajderPovrsina, niz2));
        }

        console.log(minimalnaPovrsina);
        console.log(maksimalnaPovrsina);

        niz1 = "";
        niz2 = "";

        if(proveraSprat)
        {
            minimalniSprat = parseInt(formirajMinimalneVrednostiSlajdera(slajderSprat, niz1));
            maksimalniSprat = parseInt(formirajMaksimalneVrednostiSlajdera(slajderSprat, niz2));
        }

        console.log(minimalniSprat);
        console.log(maksimalniSprat);

        niz1 = "";
        niz2 = "";

        if(proveraBrojSoba)
        {
            minimalniBrojSoba = parseInt(formirajMinimalneVrednostiSlajdera(slajderBrojSoba, niz1));
            maksimalniBrojSoba = parseInt(formirajMaksimalneVrednostiSlajdera(slajderBrojSoba, niz2));
        }

        console.log(minimalniBrojSoba);
        console.log(maksimalniBrojSoba);


        var stringArray = {};
        for(var i=0; i<(inputiZaPretragu.length); i++) {
            if($(inputiZaPretragu[i]).attr('type')==="checkbox")
                {
                    stringArray[$(inputiZaPretragu[i]).attr('name') ] = $(inputiZaPretragu[i]).is(':checked');
                }
                else if($(inputiZaPretragu[i]).attr('type')==="radio")
                {
                     stringArray[$(inputiZaPretragu[i]).attr('name') ] =$("[name='"+$(inputiZaPretragu[i]).attr('name')+"']:checked").val();  
                }
                else 
                {
                    stringArray[$(inputiZaPretragu[i]).attr('name') ] = $(inputiZaPretragu[i]).val();
                }
                stringArray['minimalna_cena'] = minimalnaCena;
                stringArray['maksimalna_cena'] = maksimalnaCena;
                stringArray['minimalnaPovrsina'] = minimalnaPovrsina;
                stringArray['maksimalnaPovrsina'] = maksimalnaPovrsina;
                stringArray['minimalniSprat'] = minimalniSprat;
                stringArray['maksimalniSprat'] = maksimalniSprat;
                stringArray['minimalniBrojSoba'] = minimalniBrojSoba;
                stringArray['maksimalniBrojSoba'] = maksimalniBrojSoba;
        }
        console.log('stringArray');
        console.log(stringArray);
        //filtrirajOglase(stringArray);
        filtrirajOglase(stringArray)
    });

    function filtrirajOglase(stringArray)
    {
        $('.oglasi').empty();
        var s="<div class='col-sm-6 col-md-4 p0'>";
        s+="<div class='box-two proerty-item'><div class='item-thumb'>";
        s+="<img src='assets/img/demo/property-3.jpg'>";
        s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'>Naziv oglasa</a></h5>";
        s+="<div class='dot-hr'></div><span class='pull-left'><b>Površina:</b> 120m </span>";
        s+="<span class='proerty-price pull-right'> $ 300,000</span><p style='display: none;''>Suspendisse ultricies Suspendisse ultricies Nulla quis dapibus nisl. Suspendisse ultricies commodo arcu nec pretium ...</p>";
        s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
        s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div>";

        console.log("CAO, KALE");
        $.ajax({
            method: "POST",
            url: Settings.filtriranjeOglasa_url,
            data: stringArray,
            success: function(data)
            {
                console.log(data);
                $.each( data, function(i,l){

                    console.log(l);

                    s="<a href = '"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+"'><div class='col-sm-6 col-md-4 p0'>";
                    s+="<div class='box-two proerty-item'><div class='item-thumb'>";
                    if(l["slike"]!==null)
                    {
                        s+="<img src='"+l["slike"]+"'>";
                    }
                    else
                    {
                        s+="<img src='assets/img/demo/property-3.jpg'>";
                    }
                    s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'>"+l['naslov_oglasa']+"</a></h5>";
                    s+="<div class='dot-hr'></div><span class='pull-left'><b>Površina:</b> "+l['povrsina']+" </span>";
                    s+="<span class='proerty-price pull-right'>"+l['cena']+"</span><p style='display: none;''>"+l['opis']+"</p>";
                    s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
                    s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div></a>";

                    $('.oglasi').append(s);

                });
            },
            dataType: "json"
        }); 
    };


    

    

})(jQuery);