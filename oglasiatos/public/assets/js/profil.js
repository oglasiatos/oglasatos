(function ($) {
    "use strict";
    
    /*==================================================================
    [ Validate ]*/
    var input = $('.form-control');
    var sacuvajizmeni="izmeni";
    
    $(document).ready(function(){
             for(var i=0; i<(input.length); i++) {
            $(input[i]).prop( "disabled", true );}


        });
    //$(document).ready(function(){
    $("#izmeni.btn").click(function(){
        if(sacuvajizmeni==="izmeni")
        {
            console.log("izmeni");
            $(".prikazslike").hide();
            $(".ucitavanjeSlike").show();
            $("#ponisti.btn").show();
            $("#izmeni.btn").prop( "id", "sacuvaj" );
            $("#sacuvaj.btn").prop("value","Sačuvaj");
             for(var i=0; i<(input.length); i++) {
                $(input[i]).prop( "disabled", false );
            }
            zabrani();
            sacuvajizmeni="sacuvaj";
        }
        else
        {
            test();
            console.log("sacuvaj");
            sacuvajizmeni="izmeni";
            $("#ponisti.btn").hide();
            $("#sacuvaj.btn").prop( "id", "izmeni" );
            $("#izmeni.btn").prop("value","Izmeni");
            
        }
        
    });
    $("#ponisti.btn").click(function(){
        console.log("ponisti");
        location.reload();
    });
    function test () {   
        var check = true;
        var stringArray = {};
        

        for(var i=0; i<(input.length); i++) {
            console.log($(input[i]).attr('name'));
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }else
            {
                stringArray[$(input[i]).attr('name') ] = $(input[i]).val();
                //console.log($(input[i]).attr('name'));
            }
        }
        if(check==true)
        {
            console.log(stringArray);
            sendajx(stringArray);
        }    
        return check;
    };

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $('.form-control').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function sendajx(stringArray)
    {
       

        console.log(stringArray);
    
        var json_string = JSON.stringify(stringArray);
        console.log(json_string);
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                if(data['Status']==='Uspesna izmena!')
                {
                     for(var i=0; i<(input.length); i++) {
                      $(input[i]).prop( "disabled", true );}

                     location.reload();
            
                     //setujkuki(data['Status']);
                     //window.location = Settings.profil_url;
                     //$(".login100-form").attr("hidden",true);

                     //$(".uspesna").html("<span class='uspesno-txt'>Uspesno ste registrovani mozete pregledati svoj profil </span><a href='profil' class='uspesno-txt'>ovde</a>");
                }
            },
            dataType: "json"
            //traditional: true
        });

            
    };
    function validate (input) {

        /*if($(input).attr('id') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }*/
        if($(input).attr('id') == 'lozinka')
        {
             //console.log($(input).val());
            if($(input).val().length<8)
            {
                showValidate(input);
                //console.log("Kratka je sifra");
                return false;
            }
        }
        else if($(input).val().length<2)
        {
                return false;
        }


    }
    function zabrani()
    {
        var zaZabranu=$('.zabrani');
        for(var i=0; i<(zaZabranu.length); i++) {
            $(zaZabranu[i]).prop("disabled",true);
        }

    }

    function showValidate(input) {

        var thisAlert = $(input).parent();;
        //console.log(thisAlert);
        //$(input).attr('id', 'inputWarning');
        $(thisAlert).addClass('has-error has-feedback');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('has-error has-feedback');
    }
    

    

})(jQuery);