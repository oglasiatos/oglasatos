(function ($) {
    "use strict";
    
    /*==================================================================
    [ Validate ]*/
    var input = $('.form-control');
    var sacuvajizmeni="izmeni";
    var brojstrana=0;
    var trenutne=1;
    $(document).ready(function(){
             var niz={};
             niz['komanda']="Listajstan";
             brojacstrana(niz);
             sendajx(niz);

             
    });
    
    function test () {   
        var check = true;
        var stringArray = {};
        

        for(var i=0; i<(input.length)-1; i++) {
            console.log($(input[i]).attr('name'));
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }else
            {
                stringArray[$(input[i]).attr('name') ] = $(input[i]).val();
                //console.log($(input[i]).attr('name'));
            }
        }
        if(check==true)
        {
            console.log(stringArray);
            sendajx(stringArray);
        }    
        return check;
    };

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $('.form-control').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function sendajx(stringArray)
    {
       

        console.log(stringArray);
    
        var json_string = JSON.stringify(stringArray);
        console.log(json_string);
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                if(data==="Prijavi se!")
                {

                }
                else if(! $.isEmptyObject(data))
                {
                    var s="";
                    var l=data[0];
                    $(".dodajgaovde").empty();
                     var granica=trenutne*9;
                    if(trenutne*9>data.length)
                    {
                        granica=data.length;
                    }
                    data=data.slice((trenutne-1)*9, granica);
                    $.each( data, function(i,l) {

                        
                        console.log("id je -"+l['id_oglasa']);
                        s="<div class='box-two proerty-item'>";
                        s+="<div class='item-thumb'><a href='"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+ "'>";
                        s+="<img src='";
                        if(l["slike"]!== null)
                        {
                            s+=l["slike"]+"'></a></div><div class='item-entry overflow'>";
                        }
                        else
                        {
                            s+="assets/img/demo/property-3.jpg'></a></div><div class='item-entry overflow'>";
                        }
                        s+="<h5><a href='"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+ "'>"+l['naziv_oglasa']+"</a></h5><div class='dot-hr'></div>";
                        s+="<span class='pull-left'><b> Površina :</b> "+l["povrsina"]+"m<sup>2</sup> </span>";
                        s+="<span class='proerty-price pull-right'> € "+l["cena"]+"</span>";
                        s+="<p style='display: none;'>"+l["opis"]+"</p><div class='property-icon'>";
                        s+= "<img src='assets/img/icon/bed.png'>("+l["brsoba"]+")|";
                        s+="<img src='assets/img/icon/cars.png'>(";
                        if(l['garaza']=true)
                        {
                            s+="1";
                        }
                        else
                        {
                            s=+"0";
                        }
                        s+=")<div class='dealer-action pull-right'>";                                        
                        s+="<a id='"+l['id_oglasa']+"' class='button delete_user_car brisanje_oglasa"+l['id_oglasa']+"'>";
                        s+="Brisanje oglasa </a><a id='"+l['id_oglasa']+"' href='"+Settings.prijavljeni_url+l['id_oglasa']+ "' class='button prijavljeni"+l['id_oglasa']+"'>Prijavljeni korisnici </a>";
                        s+="</div></div></div></div>";
                        $(".dodajgaovde").append(s);
                    
                        $(".brisanje_oglasa"+l['id_oglasa']).confirm({
                            title: 'Brisanje oglasa!',
                            content: 'Da li ste sigurni da želite da obrišete oglas?',
                            buttons: {
                                    Da: function () {
                                                //brisanjeOglasa(d.currentTarget.id);
                                                console.log(l['id_oglasa']);
                                                brisanjeOglasa(l['id_oglasa']);
                                            },
                                    Ne: function () {
                                                $.alert('');
                                           },
       
                                    }
                            });
                        console.log("ponovo");

                    });
                }
                else
                {
                    $(".dodajgaovde").empty();
                }
            },
            dataType: "json"
            //traditional: true
        });

            
    };

    $('#dodaj.btn').click(function (){
        window.location = Settings.dodavanjeOglasa_url;
    });


    function brisanjeOglasa(id)
    {
        var stringArray={};
        stringArray['id']=id;
        console.log(stringArray);
        //var json_string = JSON.stringify(stringArray);
        $.ajax({
            method: "POST",
            url: Settings.brisanjeOglasa_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                sendajx("");
            },
            dataType: "json"
        });
    }

     function brojacstrana()
    {
       
        
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            success: function(data){
                    //var array = JSON.parse(data)
                    console.log(data);
                    brojstrana=data.length;
                    console.log(brojstrana);
                    $(".stranice").empty();
                    var s="<li><a class='strpre'>Prethodna</a></li>";
                    $(".stranice").append(s);

                     $(".strpre").click(function(d)
                        {
                            if(trenutne>1)
                            {
                                $("#"+trenutne).parent().removeClass("page-item active")
                                trenutne=trenutne-1;
                                $("#"+trenutne).parent().addClass("page-item active")
                                vratiOglase();
                            }
                            
                    });

                    for (var i = 0; i < brojstrana/9; i++) {
                        var indeks=i+1;
                        console.log(i);
                        s="<li><a class='str' id='"+indeks+"'>"+indeks+"</a></li>";
                        $(".stranice").append(s);
                        $("#"+indeks+".str").click(function(d)
                        {
                            console.log(d.target.id);
                            $("#"+trenutne).parent().removeClass("page-item active")
                            trenutne=d.target.id;
                            $("#"+trenutne).parent().addClass("page-item active")
                            vratiOglase();
                        });
                    }
                    s="<li><a class='sldstr'>Sledeca</a></li>";
                    $(".stranice").append(s);
                    $(".sldstr").click(function(d)
                        {

                            if(trenutne<(brojstrana/9))
                            {
                                console.log("aSad");
                                $("#"+trenutne).parent().removeClass("page-item active")
                                trenutne=trenutne+1;
                                $("#"+trenutne).parent().addClass("page-item active")
                                vratiOglase();
                            }
                            
                    });
                    $("#"+trenutne).parent().addClass("page-item active")

            },
            dataType: "json"
            //traditional: true
        });

            
    };

    
    function validate (input) {

        /*if($(input).attr('id') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }*/
        if($(input).attr('id') == 'lozinka')
        {
             //console.log($(input).val());
            if($(input).val().length<8)
            {
                showValidate(input);
                //console.log("Kratka je sifra");
                return false;
            }
        }
        else if($(input).val().length<2)
        {
                return false;
        }


    }
    function zabrani()
    {
        var zaZabranu=$('.zabrani');
        for(var i=0; i<(zaZabranu.length); i++) {
            $(zaZabranu[i]).prop("disabled",true);
        }

    }

    function showValidate(input) {

        var thisAlert = $(input).parent();;
        //console.log(thisAlert);
        //$(input).attr('id', 'inputWarning');
        $(thisAlert).addClass('has-error has-feedback');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('has-error has-feedback');
    }
    

    

})(jQuery);