
(function ($) {
    "use strict";

     $(function() { 
	 	console.log("Uso");
	    vratiOglase();
        //$('.pretraga');
   	 });

     function vratiOglase()
    {
        $('.oglasi').empty();

        var s="<div class='box-two proerty-item'>";
        s+="<div class='item-thumb'><a href='property-1.html' ><img src='assets/img/demo/property-3.jpg'></a>";
        s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'> Naslov oglasa <label class='pull-right'style='color: gray;'>Korisničko ime</label> </a></h5>";
        s+="<div class='dot-hr'></div><span hidden class='idoglasa'>ID</span>";
        s+="<span class='proerty-price pull-right'> € 300,000</span><p style='display: none;''>Opis ...</p>";
        s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
        s+="<!--img src='assets/img/icon/cars.png'--> <div class='dealer-action pull-right'>";
        s+="<a href='#' class='button delete_user_car'>Brisanje oglasa</a>";
        s+="<a href='#' class='button'>Blokiranje korisnika</a></div></div></div></div>";
       
        //$('.oglasi').append(s);
        
        $.ajax({
            method: "POST",
            url: Settings.listaOglasa_url,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                $.each( data, function(i,l){

                    

                    s="<div class='box-two proerty-item'>";
                    s+="<div class='item-thumb'><a href = '";

                    if(l['vrsta_oglasa']==="Oglas Za Stan")
                    {
                      s+=Settings.prikazVelikogOglasaStan_url+l['id_oglasa']+"'>";
                    }
                    else if(l['vrsta_oglasa']==="Oglas Za Literaturu")
                    {
                        s+=Settings.prikazVelikogOglasaLiteratura_url+l['id_oglasa']+"'>";
                    }
                    else if(l['vrsta_oglasa']==="Oglas Za Posao")
                    {
                         s+=Settings.prikazVelikogOglasaPosao_url+l['id_oglasa']+"'>";
                    }

                    s+="<img src='assets/img/demo/property-3.jpg'></a>";
                    s+="</div><div class='item-entry overflow'><h5 style='white-space: nowrap;width: 350px;overflow: hidden;text-overflow: clip;><a  href = '"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+"'>"+l['naziv_oglasa']+"</a></h5>";
                    s+="<div class='dot-hr'></div><h5><br><label class='pull-right'style='color: gray;'>"+l['izdavac_oglasa']+"</label></h5><span hidden class='idoglasa'>"+l['id_oglasa']+"</span>";
                    s+="<span class='proerty-price pull-left'>"+l['cena']+"€</span><p style='display: none;''>"+l['opis']+"</p>";
                    s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
                    s+="<!--img src='assets/img/icon/cars.png'--> <div class='dealer-action pull-right'>";
                    s+="<a id='"+l['id_oglasa']+"' href='#' class='button delete_user_car brisanje_oglasa"+l['id_oglasa']+"'>Brisanje oglasa</a>";
                    s+="<a id='"+l['id_oglasa']+"' href='#' class='button blokiranje_korisnika"+l['id_oglasa']+"'>Blokiranje korisnika</a></div></div></div></a>";

                    $('.oglasi').append(s);

                    /*$('.brisanje_oglasa'+l['id_oglasa'] ).click(function() {
                            console.log(event);
                        //console.log("JOVAN");
                        //brisanjeOglasa(d.currentTarget.id);    
                    });*/

                    $('.brisanje_oglasa'+l['id_oglasa']).confirm({
                            title: 'Brisanje oglasa!',
                            content: 'Da li ste sigurni da želite da obrišete oglas?',
                            buttons: {
                                    Da: function () {
                                                //brisanjeOglasa(d.currentTarget.id);
                                                console.log(l['id_oglasa']);
                                                brisanjeOglasa(l['id_oglasa']);
                                            },
                                    Ne: function () {
                                                $.alert('');
                                            },
       
                                    }
                            });

                    $('.blokiranje_korisnika'+l['id_oglasa']).confirm({
                            title: 'Blokiranje korisnika!',
                            content: 'Da li ste sigurni da želite da blokirate profil?',
                            buttons: {
                                    Da: function () {
                                                //brisanjeOglasa(d.currentTarget.id);
                                                console.log(l['id_oglasa']);
                                                blokiranjeKorisnika(l['id_oglasa']);
                                            },
                                    Ne: function () {
                                                $.alert('');
                                            },
       
                                    }
                            });
                });
        },
            dataType: "json"
        }); 
    }
       function brisanjeOglasa(id)
    {
        var stringArray={};
        stringArray['id']=id;
        console.log(stringArray);
        //var json_string = JSON.stringify(stringArray);
        $.ajax({
            method: "POST",
            url: Settings.brisanjeOglasa_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                vratiOglase();
            },
            dataType: "json"
        });
    }

    function blokiranjeKorisnika(id)
    {
        var stringArray={};
        stringArray['id']=id;
        console.log(stringArray);
        $.ajax({
            method: "POST",
            url: Settings.blokiranjeKorisnika_url,
            data: stringArray,
            success: function(data)
            {
                console.log(data);
                vratiOglase();
            },
            dataType: "json"
        });
    }

    function pretraziKorisnikePremaKorisnickimImenima(deoKorisnickogImena)
    {
        $('.oglasi').empty();

        var s="<div class='box-two proerty-item'>";
        s+="<div class='item-thumb'><a href='property-1.html' ><img src='assets/img/demo/property-3.jpg'></a>";
        s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'> Naslov oglasa <label class='pull-right'style='color: gray;'>Korisničko ime</label> </a></h5>";
        s+="<div class='dot-hr'></div><span hidden class='idoglasa'>ID</span>";
        s+="<span class='proerty-price pull-right'> € 300,000</span><p style='display: none;''>Opis ...</p>";
        s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
        s+="<!--img src='assets/img/icon/cars.png'--> <div class='dealer-action pull-right'>";
        s+="<a href='#' class='button delete_user_car'>Brisanje oglasa</a>";
        s+="<a href='#' class='button'>Blokiranje korisnika</a></div></div></div></div>";

        var stringArray={};
        stringArray['deoKorisnickogImena']=deoKorisnickogImena;
        console.log(stringArray);

        $.ajax({
            method: "POST",
            url: Settings.pretraziKorisnike_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                var r=0;
                $.each( data, function(i,l){

                    console.log(l);

                    s="<a href = '"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+"'><div class='box-two proerty-item'>";
                    s+="<div class='item-thumb'><a href='property-1.html' ><img src='assets/img/demo/property-3.jpg'></a>";
                    s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'>"+l['naziv_oglasa']+"<a href='"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+"'><label class='pull-right'style='color: gray;'>"+l['izdavac_oglasa']+"</label></a> </a></h5>";
                    s+="<div class='dot-hr'></div><span hidden class='idoglasa'>"+l['id_oglasa']+"</span>";
                    s+="<span class='proerty-price pull-right'>"+l['cena']+"</span><p style='display: none;''>"+l['opis']+"</p>";
                    s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
                    s+="<!--img src='assets/img/icon/cars.png'--> <div class='dealer-action pull-right'>";
                    s+="<a id='"+l['id_oglasa']+"' href='#' class='button delete_user_car brisanje_oglasa"+l['id_oglasa']+"'>Brisanje oglasa</a>";
                    s+="<a id='"+l['id_oglasa']+"' href='#' class='button blokiranje_korisnika"+l['id_oglasa']+"'>Blokiranje korisnika</a></div></div></div></a>";

                    $('.oglasi').append(s);


                    $('.brisanje_oglasa'+l['id_oglasa']).confirm({
                            title: 'Brisanje oglasa!',
                            content: 'Da li ste sigurni da želite da obrišete oglas?',
                            buttons: {
                                    Da: function () {
                                                //brisanjeOglasa(d.currentTarget.id);
                                                console.log(l['id_oglasa']);
                                                brisanjeOglasa(l['id_oglasa']);
                                            },
                                    Ne: function () {
                                                $.alert('');
                                            },
       
                                    }
                            });

                    $('.blokiranje_korisnika'+l['id_oglasa']).confirm({
                            title: 'Blokiranje korisnika!',
                            content: 'Da li ste sigurni da želite da blokirate profil?',
                            buttons: {
                                    Da: function () {
                                                //brisanjeOglasa(d.currentTarget.id);
                                                console.log(l['id_oglasa']);
                                                blokiranjeKorisnika(l['id_oglasa']);
                                            },
                                    Ne: function () {
                                                $.alert('');
                                            },
       
                                    }
                            });
                });
        },
            dataType: "json"
        }); 

    }

    $('.pretrazi').click(function() {
                            //console.log("USO");
                            var l=$('.pretraga').val();
                            console.log(l);
                            pretraziKorisnikePremaKorisnickimImenima(l);

                    });


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

})(jQuery);