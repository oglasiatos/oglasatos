(function ($) {
    "use strict";
    
    /*==================================================================
    [ Validate ]*/
    var input = $('.form-control');
    
    //$(document).ready(function(){
    $("button").click(function(){
        test();
    });
    function test () {   
        var check = true;
        var stringArray = {};
        

        for(var i=0; i<(input.length); i++) {
            console.log($(input[i]).attr('id'));
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }else
            {
                stringArray[$(input[i]).attr('id') ] = $(input[i]).val();
                //console.log($(input[i]).attr('name'));
            }
        }
        if(check==true)
        {
            //console.log(stringArray);
            sendajx(stringArray);
        }    
        return check;
    };

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $('.form-control').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function sendajx(stringArray)
    {
       

        console.log(stringArray);
    
        var json_string = JSON.stringify(stringArray);
        console.log(json_string);
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            data: stringArray,
            success: function(data){
                //var array = JSON.parse(data)
                console.log(data);
                if(data['Status']==='Nevalidno korisnicko ime!')
                {
                    showValidate($('#korisnickoIme'));
                }
                else if(data['Status']==='Nevalidna email adresa')
                {
                    showValidate($('#email'));
                }
                else if(data['Status']==='Korisnik je trajno blokiran!')
                {
                
                    for(var i=0; i<(input.length)-1; i++) {
                             showValidate(input);
                         }
                }
                else
                {
                    if(data["Tip"]==="Admin")
                    {
                        console.log("Admin uso");
                        window.location = Settings.admin_url;
                    }
                    else
                    {
                        window.location = Settings.profil_url;
                    }
                     //setujkuki(data['Status']);
                     //
                     //$(".login100-form").attr("hidden",true);

                     //$(".uspesna").html("<span class='uspesno-txt'>Uspesno ste registrovani mozete pregledati svoj profil </span><a href='profil' class='uspesno-txt'>ovde</a>");
                }
            },
            dataType: "json"
            //traditional: true
        });

            
    };
    function validate (input) {

        /*if($(input).attr('id') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }*/
        if($(input).attr('id') == 'lozinka')
        {
             //console.log($(input).val());
            if($(input).val().length<8)
            {
                showValidate(input);
                //console.log("Kratka je sifra");
                return false;
            }
        }
        else if($(input).val().length<2)
        {
                return false;
        }


    }

    function showValidate(input) {

        var thisAlert = $(input).parent();;
        //console.log(thisAlert);
        //$(input).attr('id', 'inputWarning');
        $(thisAlert).addClass('has-error has-feedback');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('has-error has-feedback');
    }
    

    

})(jQuery);