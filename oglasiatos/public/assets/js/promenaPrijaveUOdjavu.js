(function ($){
	"use strict";

	$(function() { 
	 	console.log("Uso");
	    proveriDaLiJeKorisnikPrijavljen();
   	 });


	function proveriDaLiJeKorisnikPrijavljen()
	{
		console.log("Pocela provera");
		var s = "";

		$.ajax({
			method: "POST",
			url: Settings.proveriPrijavu_url,
			success: function(data)
			{
				console.log(data);

				if(data['ulogovan']!="Nije ulogovan!!!")
				{
					$('.prijavaRegistracija').empty();

					s = "<button class='navbar-btn nav-button wow bounceInRight login odjava'";
					s+="data-wow-delay='0.4s'>Odjavi se</button>";


					$('.prijavaRegistracija').append(s);

					$('.odjava').click(function(){
					$.ajax({
						method:"POST",
						url: Settings.odjaviSe_url,
						success: function(data)
						{
							window.location = Settings.pocetnaStranica_url;
						},
						dataType: "json"
						});
					});
				}
			},
			dataType: "json"
		});
	};

})(jQuery);

