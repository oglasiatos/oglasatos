(function ($) {
    "use strict";

    var trenutne=1;
    var brojstrana=0;

    $(function(){
        console.log("Usao");
        brojacstrana();
        izlistajOglase();
        //predlozeniOglasi();
        selectZaGradove();
    });

   

       function selectZaGradove()

    {
        $('.odabirGrada[name="grad"]').empty();

        var s="";

        $.ajax({
            method: "POST",
            url: Settings.selectZaGrad_url,
            success: function(data)
            {
                console.log(data);
                $.each(data, function(i, l){
                    console.log(l);

                    s="<option>"+l['grad']+"</option>";

                    $('.odabirGrada[name="grad"]').append(s);
                    $('.odabirGrada').selectpicker('refresh');
                })
            },
            dataType: "json"
        });
    };


    function izlistajOglase()
    {
        $('.oglasi').empty();
        var s="<div class='col-sm-6 col-md-4 p0'>";
        s+="<div class='box-two proerty-item'><div class='item-thumb'>";
        s+="<a href='property-1.html' ><img src='assets/img/demo/property-3.jpg'></a>";
        s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'>Naziv oglasa</a></h5>";
        s+="<div class='dot-hr'></div><span class='pull-left'><b>Naziv posla:</b></span>";
        s+="<span class='proerty-price pull-right'> $ 300,000</span><p style='display: none;''>Suspendisse ultricies Suspendisse ultricies Nulla quis dapibus nisl. Suspendisse ultricies commodo arcu nec pretium ...</p>";
        s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
        s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div>";

        console.log("CAO, KALE");
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            success: function(data)
            {
                console.log(data);
                $.each( data, function(i,l){

                    console.log(l);

                    var s="<a href = '"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+"'><div class='col-sm-6 col-md-4 p0'>";
                    s+="<div class='box-two proerty-item'><div class='item-thumb'>";
                    s+="<a href='posao.html' >";
                    if(l["slike"]!==null)
                    {
                        s+="<img src='"+l["slike"]+"'>";
                    }
                    else
                    {
                        s+="<img src='assets/img/demo/property-3.jpg'>";
                    }
                    s+="</a>";
                    s+="</div><div class='item-entry overflow'><h5 style='white-space: nowrap;width: 350px;overflow: hidden;text-overflow: clip;'>"+l['naslov_oglasa']+"</h5>";
                    s+="<div class='dot-hr'></div><span class='pull-left'><b>Naziv posla:</b> "+l['naziv_posla']+" </span>";
                    s+="<span class='proerty-price pull-right'>"+l['cena']+"€</span><p style='display: none;''>"+l['opis']+"</p>";
                    s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
                    s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div></a>";

                    $('.oglasi').append(s);

                });
            },
            dataType: "json"
        }); 
    }

     function brojacstrana()
    {
       
        
        $.ajax({
            method: "POST",
            url: Settings.base_url,
            success: function(data){
                    //var array = JSON.parse(data)
                    console.log(data);
                    brojstrana=data.length;
                    console.log(brojstrana);
                    $(".stranice").empty();
                    var s="<li><a class='strpre'>Prethodna</a></li>";
                    $(".stranice").append(s);

                     $(".strpre").click(function(d)
                        {
                            if(trenutne>1)
                            {
                                $("#"+trenutne).parent().removeClass("page-item active")
                                trenutne=trenutne-1;
                                $("#"+trenutne).parent().addClass("page-item active")
                                vratiOglase();
                            }
                            
                    });

                    for (var i = 0; i < brojstrana/9; i++) {
                        var indeks=i+1;
                        console.log(i);
                        s="<li><a class='str' id='"+indeks+"'>"+indeks+"</a></li>";
                        $(".stranice").append(s);
                        $("#"+indeks+".str").click(function(d)
                        {
                            console.log(d.target.id);
                            $("#"+trenutne).parent().removeClass("page-item active")
                            trenutne=d.target.id;
                            $("#"+trenutne).parent().addClass("page-item active")
                            vratiOglase();
                        });
                    }
                    s="<li><a class='sldstr'>Sledeca</a></li>";
                    $(".stranice").append(s);
                    $(".sldstr").click(function(d)
                        {

                            if(trenutne<(brojstrana/9))
                            {
                                console.log("aSad");
                                $("#"+trenutne).parent().removeClass("page-item active")
                                trenutne=trenutne+1;
                                $("#"+trenutne).parent().addClass("page-item active")
                                vratiOglase();
                            }
                            
                    });
                    $("#"+trenutne).parent().addClass("page-item active")

            },
            dataType: "json"
            //traditional: true
        });

            
    }

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    var inputiZaPretragu = $('.pretraga');
    var provera = false;

    var pocetnaVrednostSlajdera; 

    $('.slider').on('slide', function (ev) {
                               pocetnaVrednostSlajdera = ($('.slider1').val());
                               provera = true;
                           });

    $('.pretrazi_oglase.btn').click(function(){
        console.log('Pocela pretraga');
        inputiZaPretragu = $('.pretraga');

        var minimalnaCena = 0;
        var maksimalnaCena = 100000;
        var niz1 = "";
        var niz2 = "";

        if(provera)
        {
            for(var i = 0; i < pocetnaVrednostSlajdera.length; i++)
            {
                if(pocetnaVrednostSlajdera[i] == ",")
                {
                    break;
                }
                niz1 = niz1.concat(pocetnaVrednostSlajdera[i]);
            }
            for(var j = i + 1; j < pocetnaVrednostSlajdera.length; j++)
            {
                niz2 = niz2.concat(pocetnaVrednostSlajdera[j]);
            }
            minimalnaCena = parseInt(niz1);
            maksimalnaCena = parseInt(niz2);
        }

        console.log(niz1);
        console.log(niz2);
        console.log(minimalnaCena);
        console.log(maksimalnaCena);


        var stringArray = {};
        for(var i=0; i<(inputiZaPretragu.length); i++) {
            if($(inputiZaPretragu[i]).attr('type')==="checkbox")
                {
                    if($(inputiZaPretragu[i]).is(':checked'))
                    {
                        stringArray[$(inputiZaPretragu[i]).attr('name') ] = $(inputiZaPretragu[i]).is(':checked');
                    }
                    else
                    {
                        stringArray[$(inputiZaPretragu[i]).attr('name') ] = null;
                    }
                }
                else if($(inputiZaPretragu[i]).attr('type')==="radio")
                {
                     stringArray[$(inputiZaPretragu[i]).attr('name') ] =$("[name='"+$(inputiZaPretragu[i]).attr('name')+"']:checked").val();  
                }
                else 
                {
                    stringArray[$(inputiZaPretragu[i]).attr('name') ] = $(inputiZaPretragu[i]).val();
                }
                stringArray['minimalna_cena'] = minimalnaCena;
                stringArray['maksimalna_cena'] = maksimalnaCena;
        }
        console.log('stringArray');
        console.log(stringArray);
        filtrirajOglase(stringArray);
    });

    function filtrirajOglase(stringArray)
    {
        $('.oglasi').empty();
        var s="<div class='col-sm-6 col-md-4 p0'>";
        s+="<div class='box-two proerty-item'><div class='item-thumb'>";
        s+="<img src='assets/img/demo/property-3.jpg'>";
        s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'>Naziv oglasa</a></h5>";
        s+="<div class='dot-hr'></div><span class='pull-left'><b>Naziv posla:</b></span>";
        s+="<span class='proerty-price pull-right'> $ 300,000</span><p style='display: none;''>Suspendisse ultricies Suspendisse ultricies Nulla quis dapibus nisl. Suspendisse ultricies commodo arcu nec pretium ...</p>";
        s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
        s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div>";

        console.log("CAO, KALE");
        $.ajax({
            method: "POST",
            url: Settings.filtriranjeOglasa_url,
            data: stringArray,
            success: function(data)
            {
                console.log(data);
                $.each( data, function(i,l){

                    console.log(l);

                    var s="<a href = '"+Settings.prikazVelikogOglasa_url+l['id_oglasa']+"'><div class='col-sm-6 col-md-4 p0'>";
                    s+="<div class='box-two proerty-item'><div class='item-thumb'>";
                    s+="<a href='posao.html' >";
                    if(l["slike"]!==null)
                    {
                        s+="<img src='"+l["slike"]+"'>";
                    }
                    else
                    {
                        s+="<img src='assets/img/demo/property-3.jpg'>";
                    }
                    s+="</a>";
                    s+="</div><div class='item-entry overflow'><h5><a href='property-1.html'>"+l['naslov_oglasa']+"</a></h5>";
                    s+="<div class='dot-hr'></div><span class='pull-left'><b>Naziv posla:</b> "+l['naziv_posla']+" </span>";
                    s+="<span class='proerty-price pull-right'>"+l['cena']+"</span><p style='display: none;''>"+l['opis']+"</p>";
                    s+="<div class='property-icon'><img src='assets/img/icon/bed.png'>(5)|<img src='assets/img/icon/shawer.png'>(2)|";
                    s+="<img src='assets/img/icon/cars.png'>(1)</div></div></div></div></a>";

                    $('.oglasi').append(s);

                });
            },
            dataType: "json"
        });
    };

    

    

})(jQuery);